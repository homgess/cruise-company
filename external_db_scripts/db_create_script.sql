use cruisedb;

-- drop all tables if exists --
DROP TABLE IF EXISTS ticket;
DROP TABLE IF EXISTS app_user;
DROP TABLE IF EXISTS user_role_dict;
DROP TABLE IF EXISTS ticket_status_dict;

DROP TABLE IF EXISTS cruise_info;
DROP TABLE IF EXISTS cruise_status_dict;
DROP TABLE IF EXISTS cruise;
DROP TABLE IF EXISTS liner;
DROP TABLE IF EXISTS route_point;
DROP TABLE IF EXISTS route;

-- create available liner for cruise
CREATE TABLE liner (
  liner_id int UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE,
  liner_name varchar(45) NOT NULL UNIQUE,
  passenger_capacity int UNSIGNED DEFAULT 0,
  PRIMARY KEY (liner_id));

-- create available route for cruise
CREATE TABLE route (
  route_id int UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE,
  route_name varchar(45) NOT NULL UNIQUE,
  PRIMARY KEY (route_id));
  
  -- create points of route
  CREATE TABLE route_point (
  point_id int UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE,
  route_id int UNSIGNED NOT NULL,
  point_name varchar(45) DEFAULT NULL,
  PRIMARY KEY (point_id),
  FOREIGN KEY (route_id) REFERENCES route(route_id) ON DELETE CASCADE);
  
-- create new cruise with certain liner and route
CREATE TABLE cruise (
  cruise_id int UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE,
  cruise_name varchar(125) NOT NULL UNIQUE,
  liner_id int UNSIGNED NOT NULL,
  route_id int UNSIGNED NOT NULL,
  start_datetime datetime DEFAULT NULL,
  end_datetime datetime DEFAULT NULL,
  price decimal(13,2) DEFAULT 0.0,
  PRIMARY KEY (cruise_id),
  FOREIGN KEY (liner_id) REFERENCES liner (liner_id) ON DELETE RESTRICT,
  FOREIGN KEY (route_id) REFERENCES route (route_id) ON DELETE RESTRICT);
  
    CREATE TABLE cruise_status_dict(
  status_id int UNSIGNED NOT NULL UNIQUE,
  status_name varchar(45) DEFAULT NULL,
  PRIMARY KEY(status_id));
  
    -- additional cruise info 
    CREATE TABLE cruise_info(
  cruise_id int UNSIGNED NOT NULL UNIQUE,
  status_id int UNSIGNED DEFAULT 1,
  description text DEFAULT NULL,
  image_name varchar(60) DEFAULT NULL,
  FOREIGN KEY (status_id) REFERENCES cruise_status_dict(status_id) ON DELETE CASCADE,
  FOREIGN KEY (cruise_id) REFERENCES cruise(cruise_id) ON DELETE CASCADE);
  
  -- create status dictionary for orders
CREATE TABLE ticket_status_dict (
  status_id int  UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE,
  status_name varchar(45) DEFAULT NULL,
  PRIMARY KEY (status_id));
  
-- create role dictionary for users
  CREATE TABLE user_role_dict (
  role_id int UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE,
  role_name varchar(45),
  PRIMARY KEY (role_id));

-- create application user
CREATE TABLE app_user (
  user_id int UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE,
  user_login varchar(25) NOT NULL UNIQUE,
  user_password varchar(65) NOT NULL,
  role_id int UNSIGNED NOT NULL DEFAULT 2,
  last_online_date datetime DEFAULT NOW(),
  PRIMARY KEY (user_id),
  FOREIGN KEY (role_id) REFERENCES user_role_dict (role_id));
  
-- create user on certain cruise
  CREATE TABLE ticket (
  ticket_id int UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE,
  cruise_id int UNSIGNED NOT NULL,
  user_id int UNSIGNED NOT NULL,
  position int UNSIGNED NOT NULL,
  docs_image_name varchar(60) DEFAULT NULL,
  price decimal(13,2) DEFAULT 0.0,
  status_id int UNSIGNED DEFAULT 1,
  PRIMARY KEY (ticket_id),
  FOREIGN KEY (cruise_id) REFERENCES cruise (cruise_id),
  FOREIGN KEY (user_id) REFERENCES app_user (user_id),
  FOREIGN KEY (status_id) REFERENCES ticket_status_dict (status_id));
  
  -- create table content
INSERT INTO liner VALUES 
(1, 'Прованс сталь', 5), 
(2, 'Сильвас', 3),
(3, 'Принц Увельський', 7),
(4, 'Бадиста енерно', 20),
(5, 'Морський кришталевий наїздник', 32);

INSERT INTO route VALUES
(1, 'Азійське море'),
(2, 'Бермудський архіпелаг'),
(3, 'Червоне море'),
(4, 'Європейське море'),
(5, 'Кругосвітня подоріж Амстердам - Сідней');

INSERT INTO route_point VALUES
(DEFAULT, 1, 'Порт Сан-Марино (Іспанія)'),
(DEFAULT, 1, 'Порт Лепласка (Іспанія)'),
(DEFAULT, 1, 'Порт Чьордин (Іспанія)'),
(DEFAULT, 1, 'Порт Фінегал (Іспанія)'),
(DEFAULT, 1, 'Порт Безіменний-Принц (Іспанія)'),
(DEFAULT, 1, 'Порт Сан-Марино (Іспанія)'),
(DEFAULT, 2, 'Порт Сан-Марино (Іспанія)'),
(DEFAULT, 2, 'Порт Леплас (Португалія)'),
(DEFAULT, 2, 'Порт Чорджин (Іспанія)'),
(DEFAULT, 3, 'Порт Ліасполь'),
(DEFAULT, 3, 'Порт Сандрин (Англія)'),
(DEFAULT, 3, 'Порт Кадент (Англія)'),
(DEFAULT, 3, 'Порт Фьорна (Англія)'),
(DEFAULT, 3, 'Порт Сиппель-Трініті (Ірландія)'),
(DEFAULT, 3, 'Порт Леплас (Ірландія)'),
(DEFAULT, 3, 'Порт Чорджин (Ірландія)'),
(DEFAULT, 3, 'Порт Сандрин (Англія)'),
(DEFAULT, 4, 'Порт Леплас (Іспанія)'),
(DEFAULT, 4, 'Порт Кьорт (Португалія)'),
(DEFAULT, 4, 'Порт Льосас (Греція)'),
(DEFAULT, 4, 'Порт Абідел-сін (Італія)'),
(DEFAULT, 4, 'Порт Безіменний-Принц (Іспанія)'),
(DEFAULT, 5, 'Порт Креаполь (Англія)'),
(DEFAULT, 5, 'Порт Філіпс (Філіпіни)'),
(DEFAULT, 5, 'Порт Андерос (Австралія)'),
(DEFAULT, 5, 'Порт Сиппель-Трініті (Японія)'),
(DEFAULT, 5, 'Порт Колотокіто (Данія)'),
(DEFAULT, 5, 'Порт Сьоний (Швеція)'),
(DEFAULT, 5, 'Порт Зелена Миля (Бразилія)'),
(DEFAULT, 5, 'Порт Анкутрджин (Ванкувер)'),
(DEFAULT, 5, 'Порт Нью-Йорк (Нігерія)'),
(DEFAULT, 5, 'Порт Ась (Греція)'),
(DEFAULT, 5, 'Порт Абідел-сін (Італія)'),
(DEFAULT, 5, 'Порт Безіменний-Принц (Іспанія)');

INSERT INTO cruise_status_dict VALUES
(1, 'Створено новий круїз'),
(2, 'Ведеться набір пасажирів'),
(3, 'Круїз заповнений, очікується відправлення'),
(4, 'Круїз в процесі'),
(5, 'Круїз завершено'),
(6, 'Круїз відмінено');

INSERT INTO cruise VALUES 
(1, 'Подоріж навколо Азійського моря січень 2022', 1, 1, '2022-01-07 09:00:00', '2022-01-30 21:00:00', 1200.50),
(2, 'Круїз Бермундським архіпелагом березень 2022', 2, 2, '2022-03-25 11:25:00', '2022-05-01 12:00:00', 5000),
(3, 'Європейський туризм по морю', 5, 4, '2022-02-25 11:25:00', '2022-04-01 12:00:00', 14999.99),
(4, 'Світова подорож Амстердам - Сідней 2022', 3, 5, '2022-06-25 11:25:00', '2022-09-01 12:00:00', 50000),
(5, 'Круїз Бермундським архіпелагом вересень 2022', 2, 2, '2022-09-28 11:25:00', '2022-10-13 12:00:00', 5000),
(6, 'Світова подорож Амстердам - Сідней 2023', 3, 5, '2023-06-25 11:25:00', '2023-09-01 12:00:00', 50000),
(7, 'Подоріж навколо Азійського моря березень 2022', 1, 1, '2022-03-25 09:00:00', '2022-03-29 21:00:00', 1200.50),
(8, 'Подоріж навколо Азійського моря травень 2022', 1, 1, '2022-05-24 09:00:00', '2022-05-28 21:00:00', 1200.50),
(9, 'Подоріж навколо Азійського моря липень 2022', 1, 1, '2022-07-01 09:00:00', '2022-07-04 21:00:00', 1200.50),
(10, 'Подоріж навколо Азійського моря січень 2023', 1, 1, '2023-01-12 09:00:00', '2023-01-16 21:00:00', 1200.50),
(11, 'Круїз Бермундським архіпелагом березень 2023', 2, 2, '2023-03-25 11:25:00', '2023-05-01 12:00:00', 5000),
(12, 'Новий європейський туризм по морю', 5, 4, '2022-09-02 11:25:00', '2022-12-12 12:00:00', 14999.99),
(13, 'Світова подорож Амстердам - Сідней 2024', 3, 5, '2024-01-25 11:25:00', '2024-05-01 12:00:00', 50000),
(14, 'Круїз Бермундським архіпелагом вересень 2023', 2, 2, '2023-09-17 11:25:00', '2023-09-30 12:00:00', 5000),
(15, 'Світова подорож Амстердам - Сідней 2025', 3, 5, '2025-06-25 11:25:00', '2025-08-26 12:00:00', 50000),
(16, 'Подоріж навколо Азійського моря березень 2023', 1, 1, '2022-03-25 09:00:00', '2022-04-05 21:00:00', 1200.50),
(17, 'Подоріж навколо Азійського моря травень 2023', 1, 1, '2022-05-25 09:00:00', '2022-06-05 21:00:00', 1200.50),
(18, 'Подоріж навколо Азійського моря липень 2023', 1, 1, '2022-07-25 09:00:00', '2022-08-05 21:00:00', 1200.50);

INSERT INTO cruise_info VALUES
(1, 5, 'Найкращий круїз, що покаже вам незабутню подорож по всіх відомих місцях навколо Азійського моря', '1.jpg'),
(2, 5, 'Є такі речі, які обов`язково потрібно спробувати в житті. Наприклад, відправитися в подорож на круїзному лайнері по Бермудськии архіпелагам.', '2.jpg'),
(3, 2, 'Євро́па — частина світу в Північній півкулі. Омивається Північним Льодовитим океаном на півночі, Атлантичним океаном на заході та Середземним морем на півдні. На сході та південному сході відокремлена від Азії Уральськими та Кавказькими горами, річкою Урал, Каспійським та Чорним морями.', '3.jpg'),
(4, 2, 'Це проект про подорожі світом та Україною від українських мандрівників. Дуже любимо подорожувати і відкривати для себе нові місця, а ще більше нові країни.', '4.jpg'),
(5, 2, 'Є такі речі, які обов`язково потрібно спробувати в житті. Наприклад, відправитися в подорож на круїзному лайнері по Бермудськии архіпелагам.', '5.jpg'),
(6, 2, 'Це проект про подорожі світом та Україною від українських мандрівників. Дуже любимо подорожувати і відкривати для себе нові місця, а ще більше нові країни.', '6.jpg'),
(7, 2, 'Найкращий круїз, що покаже вам незабутню подорож по всіх відомих місцях навколо Азійського моря', '7.jpg'),
(8, 2, 'Найкращий круїз, що покаже вам незабутню подорож по всіх відомих місцях навколо Азійського моря', '8.jpg'),
(9, 2, 'Найкращий круїз, що покаже вам незабутню подорож по всіх відомих місцях навколо Азійського моря', '9.jpg'),
(10, 2, 'Найкращий круїз, що покаже вам незабутню подорож по всіх відомих місцях навколо Азійського моря', '10.jpg'),
(11, 2, 'Є такі речі, які обов`язково потрібно спробувати в житті. Наприклад, відправитися в подорож на круїзному лайнері по Бермудськии архіпелагам.', '11.jpg'),
(12, 2, 'Євро́па — частина світу в Північній півкулі. Омивається Північним Льодовитим океаном на півночі, Атлантичним океаном на заході та Середземним морем на півдні. На сході та південному сході відокремлена від Азії Уральськими та Кавказькими горами, річкою Урал, Каспійським та Чорним морями.', '12.jpg'),
(13, 2, 'Це проект про подорожі світом та Україною від українських мандрівників. Дуже любимо подорожувати і відкривати для себе нові місця, а ще більше нові країни.', '13.jpg'),
(14, 2, 'Є такі речі, які обов`язково потрібно спробувати в житті. Наприклад, відправитися в подорож на круїзному лайнері по Бермудськии архіпелагам.', '14.jpg'),
(15, 2, 'Це проект про подорожі світом та Україною від українських мандрівників. Дуже любимо подорожувати і відкривати для себе нові місця, а ще більше нові країни.', '15.jpg'),
(16, 2, 'Найкращий круїз, що покаже вам незабутню подорож по всіх відомих місцях навколо Азійського моря', '16.jpg'),
(17, 2, 'Найкращий круїз, що покаже вам незабутню подорож по всіх відомих місцях навколо Азійського моря', '17.jpg'),
(18, 2, 'Найкращий круїз, що покаже вам незабутню подорож по всіх відомих місцях навколо Азійського моря', '18.jpg');

INSERT INTO user_role_dict VALUES
(1, 'Заблокований'),
(2, 'Користувач'),
(3, 'Адміністратор');

INSERT INTO ticket_status_dict VALUES
(1, 'Створена заявка'),
(2, 'Заявка прийнята, очікується оплата'),
(3, 'Заявка оплачена'),
(4, 'Заявка неактуальна'),
(5, 'Заявка відхилена');


INSERT INTO app_user VALUES 
(1, 'admin', 'ffc96093f59e0d9c782dafcc5bac9ef6d5779777363b20158f915a7caad3cc13', 3, DEFAULT),
(2, 'new_admin', '7f81aaccd1c66c443c26bd0bdaea72c4735d31d44895aa40c62f5ba38ab37e56', 3, DEFAULT),
(3, 'user1', '454a3174b64f620b8a27a48e0e7dbf2cab05605ea15af9f2b67fed2ec28c5a60', 2, DEFAULT),
(4, 'user2', '4f9dd737d2cd22b291348fadb39196fd429f63323dd4c482d5637367b23cab81', 2, DEFAULT);

INSERT INTO ticket VALUES
(1, 1, 1, 1,'docs1.jpg', 355.55, 1),
(2, 1, 2, 3, 'docs2.png', 355.55, 1),
(3, 1, 3, 2,'docs3.jpg', 355.55, 1);