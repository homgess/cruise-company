Круїзна компанія

Компанія має декілька круїзних лайнерів. 

Лайнер має свою пасажиромісткість, маршрут, кількість відвідуваних портів, початок та кінець круїзу, персонал.

Кліент вибирає круїз із каталогу, залишає заявку на нього, за наявності вільних місць, завантажує скан копії документів та сплачує повну вартість круїзу, після того, як адміністратор круїзної компанії підтвердив можливість туру.

Реалізувати можливість вибору круїзу за датою та тривалістю.

Адміністратор круїзної компанії керує записами круїзних лайнерів та заявками клієнтів, змінюючи їх статус на "сплачена".

Заявка автоматично становиться "завершенною" по завершенні круїза. 

![alt text](https://gitlab.com/homgess/cruise-company/-/raw/master/external_db_scripts/db_diagram.png)
