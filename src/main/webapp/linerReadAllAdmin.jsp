<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="linerFindAll"/> | <fmt:message key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <h3><fmt:message key="linerFindAll"/></h3>
        </div>
    </div>
    <a class="btn btn-outline-info" href="${pageContext.request.contextPath}/pages/admin-panel"><fmt:message
            key="backToAdminPanel"/></a>
    <table class="table table-bordered">
        <caption><fmt:message key="listOfLinersAdmin"/></caption>
        <a class="btn btn-info"
           href="${pageContext.request.contextPath}/pages/admin-liner?action=form-insert"><fmt:message
                key="linerInsert"/></a>
        <tr>
            <th><fmt:message key="id"/></th>
            <th><fmt:message key="name"/></th>
            <th><fmt:message key="passengerCapacity"/></th>
        </tr>
        <c:forEach var="liner" items="${requestScope.linerList}">
            <tr>
                <td><c:out value="${liner.id}"/></td>
                <td><c:out value="${liner.name}"/></td>
                <td><c:out value="${liner.passengerCapacity}"/></td>
                <td>
                    <a class="btn btn-warning"
                       href="${pageContext.request.contextPath}/pages/admin-liner?action=form-update&id=<c:out value="${liner.id}"/>"><fmt:message
                            key="edit"/></a>
                </td>
                <td>
                    <a class="btn btn-danger"
                       href="${pageContext.request.contextPath}/pages/admin-liner?action=delete-by-id&id=<c:out value="${liner.id}"/>"><fmt:message
                            key="delete"/></a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>

<%@include file="footer.jspf" %>
</body>
</html>
