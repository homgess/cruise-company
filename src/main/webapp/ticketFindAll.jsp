<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="ct" uri="/WEB-INF/tld/customTagLibrary" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="ticketFindMy"/> | <fmt:message key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <h3><fmt:message key="ticketFindMy"/></h3>
        </div>
    </div>
    <table class="table table-bordered">
        <caption><fmt:message key="listOfTickets"/></caption>
        <tr>
            <th><fmt:message key="id"/></th>
            <th><fmt:message key="name"/></th>
            <th><fmt:message key="position"/></th>
            <th><fmt:message key="documents"/></th>
            <th><fmt:message key="price"/></th>
            <th><fmt:message key="status"/></th>
        </tr>
        <c:forEach var="ticket" items="${requestScope.ticketList}">
            <tr>
                <td><c:out value="${ticket.id}"/></td>
                <td>
                    <c:out value="${ticket.cruiseId}"/>
                    <a href="${pageContext.request.contextPath}/pages/cruise?action=find-by-id&id=<c:out value="${ticket.cruiseId}"/>">[<fmt:message
                            key="view"/>]</a>
                </td>
                <td><c:out value="${ticket.position}"/></td>
                <td><a href="${pageContext.request.contextPath}/data/docs/${ticket.docsImageName}"><fmt:message
                        key="view"/></a></td>
                <td><c:out value="${ticket.price}"/> <ct:currencyTag>UAH</ct:currencyTag></td>
                <td><c:out value="${requestScope.statusDict[ticket.statusId]}"/></td>
                <td>
                    <div class="row">
                        <c:if test="${ticket.statusId == 2}">
                            <div class="col">
                                <form action="${pageContext.request.contextPath}/pages/ticket">
                                    <input type="hidden" name="action" value="pay">
                                    <input type="hidden" name="id" value="${ticket.id}">
                                    <input class="btn btn-primary" type="submit" value="<fmt:message key="pay"/>">
                                </form>
                            </div>
                        </c:if>
                        <c:if test="${ticket.statusId <= 2}">
                            <div class="col">
                                <form action="${pageContext.request.contextPath}/pages/ticket">
                                    <input type="hidden" name="action" value="cancel">
                                    <input type="hidden" name="id" value="${ticket.id}">
                                    <input class="btn btn-secondary" type="submit" value="<fmt:message key="cancel"/>">
                                </form>
                            </div>
                        </c:if>
                    </div>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>

<%@include file="footer.jspf" %>
</body>
</html>
