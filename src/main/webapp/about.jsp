<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="about"/> | <fmt:message key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <h3><fmt:message key="about"/></h3>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-9">
            <p><fmt:message key="aboutText"/></p>
            <h4><fmt:message key="cruiseChoice"/></h4>
            <p><fmt:message key="cruiseChoiceText"/></p>
            <h4><fmt:message key="signUpToday"/></h4>
            <p>С<fmt:message key="signUpTodayText"/></p>
        </div>
    </div>
</div>
<%@include file="footer.jspf" %>
</body>
</html>