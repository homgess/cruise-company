<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="userLogin"/> | <fmt:message key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <h3><fmt:message key="userLogin"/></h3>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6">
            <form method="post" action="${pageContext.request.contextPath}/pages/auth">
                <input type="hidden" name="action" value="signIn">
                <div class="mb-3">
                    <label class="form-label" for="login"><fmt:message key="login"/></label>
                    <input class="form-control" type="text" name="login" id="login">
                </div>
                <div class="mb-3">
                    <label class="form-label" for="password"><fmt:message key="password"/></label>
                    <input class="form-control" type="password" name="password" id="password">
                </div>
                <div class="mb-3">
                    <input class="btn btn-primary" type="submit" value="<fmt:message key="signIn"/>">
                    <a class="btn btn-secondary"
                       href="${pageContext.request.contextPath}/pages/auth?action=form-signUp"><fmt:message
                            key="signUp"/></a>
                </div>
            </form>
        </div>
    </div>
</div>

<%@include file="footer.jspf" %>
</body>
</html>
