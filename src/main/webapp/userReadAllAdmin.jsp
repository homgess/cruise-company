<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="userFindAll"/> | <fmt:message key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <h3><fmt:message key="userFindAll"/></h3>
        </div>
    </div>
    <a class="btn btn-outline-info" href="${pageContext.request.contextPath}/pages/admin-panel"><fmt:message key="backToAdminPanel"/></a>
    <table class="table table-bordered">
        <caption><fmt:message key="listOfUsers"/></caption>
        <tr>
            <th><fmt:message key="id"/></th>
            <th><fmt:message key="login"/></th>
            <th><fmt:message key="role"/></th>
            <th><fmt:message key="lastSeenOnline"/></th>
        </tr>
        <c:forEach var="user" items="${requestScope.userList}">
            <tr>
                <td><c:out value="${user.id}"/></td>
                <td><c:out value="${user.login}"/></td>
                <td><c:out value="${requestScope.roleDict[user.roleId]}"/></td>
                <td><c:out value="${user.lastOnlineDate}"/></td>
                <td>
                    <a class="btn btn-outline-warning"
                       href="${pageContext.request.contextPath}/pages/admin-user?action=form-update-role&id=<c:out value="${user.id}"/>"><fmt:message
                            key="edit"/></a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>

<%@include file="footer.jspf" %>
</body>
</html>
