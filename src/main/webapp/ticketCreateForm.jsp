<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="/WEB-INF/tld/customTagLibrary" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="ticketInsert"/> | <fmt:message key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <h3><fmt:message key="ticketInsert"/></h3>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6">
            <form method="post" action="${pageContext.request.contextPath}/pages/ticket" enctype="multipart/form-data">
                <input type="hidden" name="action" value="create">
                <input type="hidden" name="user_id" value="${requestScope.user.id}">
                <input type="hidden" name="cruise_id" value="${requestScope.cruise.id}">
                <input type="hidden" name="price" value="${requestScope.cruise.price}">
                <p><fmt:message key="login"/>: ${requestScope.user.login}</p>
                <p><fmt:message key="name"/> ${requestScope.cruise.name}</p>
                <p><fmt:message key="price"/>: ${requestScope.cruise.price} <ct:currencyTag>UAH</ct:currencyTag></p>
                <div class="mb-3">
                    <label class="form-label" for="position"><fmt:message key="position"/>: </label>
                    <select class="form-select-sm" name="position" id="position">
                        <c:forEach begin="1" step="1" end="${requestScope.liner.passengerCapacity}" var="positionId">
                            <c:if test="${not ct:contains(requestScope.occupiedPositionsList,positionId)}">
                                <option value="${positionId}">${positionId}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="input-group-text" for="image"><fmt:message key="image"/>:</label>
                    <input class="form-control-sm" type="file" name="image_name" id="image">
                </div>
                <div class="mb-3">
                    <input class="btn btn-primary" type="submit" value="<fmt:message key="orderTicket"/>">
                </div>
            </form>
            <a class="btn btn-secondary"
               href="${pageContext.request.contextPath}/pages/cruise?action=find-by-id&id=${requestScope.cruise.id}"><fmt:message
                    key="backToCruise"/></a>
        </div>
    </div>
</div>

<%@include file="footer.jspf" %>
</body>
</html>
