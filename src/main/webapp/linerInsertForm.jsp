<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="linerInsert"/> | <fmt:message key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <fmt:message key="linerInsert"/>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6">
            <form method="post" action="${pageContext.request.contextPath}/pages/admin-liner">
                <input type="hidden" name="action" value="insert">
                <div class="mb-3">
                    <label class="form-label" for="name"><fmt:message key="name"/>:</label>
                    <input class="form-control" name="name" id="name">
                </div>
                <div class="mb-3">
                    <label class="form-label" for="passenger_capacity"><fmt:message key="passengerCapacity"/>:</label>
                    <input class="form-control" name="passenger_capacity" id="passenger_capacity">
                </div>
                <div class="mb-3">
                    <input class="btn btn-primary" type="submit" value="<fmt:message key="insert"/>">
                    <input class="btn btn-secondary" type="reset" value="<fmt:message key="reset"/>">
                </div>
            </form>
        </div>
    </div>
</div>

<%@include file="footer.jspf" %>
</body>
</html>
