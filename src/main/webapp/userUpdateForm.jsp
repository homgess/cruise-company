<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="userUpdate"/> | <fmt:message key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <h3><fmt:message key="userUpdate"/></h3>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6">
            <form method="post" action="${pageContext.request.contextPath}/pages/admin-user">
                <input type="hidden" name="action" value="update-role">
                <div class="mb-3">
                    <label class="form-label" for="id"><fmt:message key="id"/>: </label>
                    <input class="disabled" readonly name="id" id="id" value="${requestScope.user.id}">
                </div>
                <div class="mb-3">
                    <label for="login"><fmt:message key="login"/>: </label>
                    <input readonly name="login" id="login" value="${requestScope.user.login}">
                </div>
                <div class="mb-3">
                    <label class="form-label" for="roles"><fmt:message key="role"/>: </label>
                    <select class="form-select" name="roleId" id="roles">
                        <c:forEach var="role" items="${requestScope.roleDict}">
                            <option value="${role.key}"
                                    <c:if test="${requestScope.user.roleId == role.key}">selected</c:if>>${role.value}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="mb-3">
                    <input class="btn btn-primary" type="submit" value="<fmt:message key="save"/>">
                    <input class="btn btn-secondary" type="reset" value="<fmt:message key="reset"/>">
                </div>
            </form>
        </div>
    </div>
</div>

<%@include file="footer.jspf" %>
</body>
</html>