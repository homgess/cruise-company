<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/customTagLibrary" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="cruiseInsert"/> | <fmt:message key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <h3><fmt:message key="cruiseInsert"/></h3>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6">
            <form method="post" action="${pageContext.request.contextPath}/pages/admin-cruise" id="cruiseForm" enctype="multipart/form-data">
                <input type="hidden" name="action" value="insert">
                <div class="mb-3">
                    <label class="form-label" for="name"><fmt:message key="name"/>:</label>
                    <input class="form-control" name="name" id="name">
                </div>
                <div class="mb-3">
                    <label class="form-label" for="liner_id"><fmt:message key="liner"/>:</label>
                    <select class="form-select" name="liner_id" id="liner_id">
                        <c:forEach items="${requestScope.linerList}" var="liner">
                            <option value="${liner.id}">${liner.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="route_id"><fmt:message key="route"/>:</label>
                    <select class="form-select" name="route_id" id="route_id">
                        <c:forEach items="${requestScope.routeList}" var="route">
                            <option value="${route.id}">${route.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="start_datetime"><fmt:message key="startDatetime"/>:</label>
                    <input class="form-control" type="datetime-local" name="start_datetime" id="start_datetime">
                </div>
                <div class="mb-3">
                    <label class="form-label" for="end_datetime"><fmt:message key="endDatetime"/>:</label>
                    <input class="form-control" type="datetime-local" name="end_datetime" id="end_datetime">
                </div>
                <div class="mb-3">
                    <label class="form-label" for="price"><fmt:message key="price"/>:</label>
                    <div class="input-group">
                    <input class="form-control" type="number" step="0.01" name="price" id="price">
                        <span class="input-group-text"><ct:currencyTag>UAH</ct:currencyTag></span>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="description"><fmt:message key="description"/></label>
                    <textarea class="form-control" name="description" id="description" form="cruiseForm"></textarea>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="image_name"><fmt:message key="image"/></label>
                    <input class="form-control" type="file" name="image_name" id="image_name">
                </div>
                <div class="mb-3">
                    <input class="form btn btn-primary align-items-lg-center" type="submit" value="<fmt:message key="insert"/>">
                    <input class="btn btn-secondary" type="reset" value="<fmt:message key="reset"/>">
                </div>
            </form>
        </div>
    </div>
</div>
<%@include file="footer.jspf" %>
</body>
</html>
