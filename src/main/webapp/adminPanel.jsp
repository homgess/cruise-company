<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="adminPanel"/> | <fmt:message key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-4">
            <h3><fmt:message key="adminPanel"/></h3>
            <div class="row justify-content-center align-items-center mt-3">
                <a class="btn btn-info"
                   href="${pageContext.request.contextPath}/pages/admin-cruise?action=find-all"><fmt:message
                        key="editCruises"/></a>
            </div>
            <div class="row justify-content-center align-items-center mt-3">
                <a class="btn btn-info"
                   href="${pageContext.request.contextPath}/pages/admin-liner?action=find-all"><fmt:message
                        key="editLiners"/></a>
            </div>
            <div class="row justify-content-center align-items-center mt-3">
                <a class="btn btn-info"
                   href="${pageContext.request.contextPath}/pages/admin-route?action=find-all"><fmt:message
                        key="editRoutes"/></a>
            </div>
            <div class="row justify-content-center align-items-center mt-3">
                <a class="btn btn-info"
                   href="${pageContext.request.contextPath}/pages/admin-user?action=find-all"><fmt:message
                        key="editUsers"/></a>
            </div>
            <div class="row justify-content-center align-items-center mt-3">
                <a class="btn btn-info"
                   href="${pageContext.request.contextPath}/pages/admin-ticket?action=find-all"><fmt:message
                        key="editTickets"/></a>
            </div>
        </div>
        <div class="col-2"></div>
        <div class="col-4 ms-4">
            <fmt:message key="youModifying"/>:
            <c:forEach items="${requestScope.modifierList}" var="modifyingEntity">
                <p><fmt:message key="entityName"/> ${modifyingEntity.entityName}<br>
                    <fmt:message key="entityId"/> ${modifyingEntity.entityId}</p>
            </c:forEach>
        </div>
    </div>
</div>

<%@include file="footer.jspf" %>
</body>
</html>