<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/customTagLibrary" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="cruiseUpdate"/> - ${requestScope.cruise.name} | <fmt:message key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <h3><fmt:message key="cruiseUpdate"/></h3>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6">
            <form method="post" action="${pageContext.request.contextPath}/pages/admin-cruise" id="cruiseForm" enctype="multipart/form-data">
                <input type="hidden" name="action" value="update">
                <input type="hidden" name="old_image_name" value="${requestScope.cruise.imageName}">
                <div class="mb-3">
                    <label class="form-label" for="id"><fmt:message key="id"/>:</label>
                    <input readonly class="form-control disabled" name="id" value="${requestScope.cruise.id}" id="id">
                </div>
                <div class="mb-3">
                    <label class="form-label" for="name"><fmt:message key="name"/>:</label>
                    <input class="form-control" name="name" value="${requestScope.cruise.name}" id="name">
                </div>
                <div class="mb-3">
                    <label class="form-label" for="liner"><fmt:message key="liner"/>:</label>
                    <select class="form-select" name="liner_id" id="liner">
                        <c:forEach items="${requestScope.linerList}" var="linerFromList">
                            <option
                                    <c:if test="${requestScope.cruise.liner.id==linerFromList.id}"> selected </c:if>
                                    value="${linerFromList.id}">${linerFromList.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="route_id"><fmt:message key="route"/>:</label>
                    <select class="form-control" name="route_id" id="route_id">
                        <c:forEach items="${requestScope.routeList}" var="routeFromList">
                            <option
                                    <c:if test="${requestScope.cruise.route.id==routeFromList.id}"> selected </c:if>
                                    value="${routeFromList.id}">${routeFromList.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="start_datetime"><fmt:message key="startDatetime"/>:</label>
                    <input class="form-control" type="datetime-local" name="start_datetime"
                           value="${requestScope.cruise.startDateTime}"
                           id="start_datetime">
                </div>
                <div class="mb-3">
                    <label class="form-label" for="end_datetime"><fmt:message key="endDatetime"/>:</label>
                    <input class="form-control" type="datetime-local" name="end_datetime"
                           value="${requestScope.cruise.endDateTime}"
                           id="end_datetime">
                </div>
                <div class="mb-3">
                    <label class="form-label" for="price"><fmt:message key="price"/>:</label>
                    <div class="input-group">
                        <input class="form-control" type="number" step="0.01" name="price" id="price" value="${requestScope.cruise.price}">
                        <span class="input-group-text"><ct:currencyTag>UAH</ct:currencyTag></span>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="description"><fmt:message key="description"/></label>
                    <textarea class="form-control" name="description" id="description"
                              form="cruiseForm">${requestScope.cruise.description}</textarea>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="curr_image_name"><fmt:message key="currentImage"/></label>
                    <a id="curr_image_name" href="${pageContext.request.contextPath}/data/cruise/${requestScope.cruise.imageName}">view</a>
                </div>
                <div class="input-group mb-3">
                    <label class="input-group-text" for="image_name"><fmt:message key="uploadNewImage"/></label>
                    <input class="form-control" type="file" name="image_name"
                           id="image_name">
                </div>
                <div class="mb-3">
                    <label class="form-label" for="status"><fmt:message key="status"/></label>
                    <select class="form-select" name="status_id" id="status">
                        <c:forEach var="status" items="${requestScope.statusDict}">
                            <option
                                    <c:if test="${requestScope.cruise.statusId == status.key}">selected</c:if>
                                    value="${status.key}">${status.value}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="mb-3">
                    <input class="btn btn-primary" type="submit" value="<fmt:message key="save"/>">
                    <input class="btn btn-secondary" type="reset" value="<fmt:message key="reset"/>">
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
