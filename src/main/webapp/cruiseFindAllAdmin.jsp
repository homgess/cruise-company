<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/customTagLibrary" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="cruiseFindAllAdmin"/> | <fmt:message key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
<div class="row justify-content-center align-items-center">
    <div class="col-10">
        <h3><fmt:message key="cruiseFindAllAdmin"/></h3>
    </div>
</div>
<a class="btn btn-outline-info" href="${pageContext.request.contextPath}/pages/admin-panel"><fmt:message
        key="backToAdminPanel"/></a>
<table class="table table-bordered">
<caption><fmt:message key="listOfCruisesAdmin"/></caption>
<a class="btn btn-info" href="${pageContext.request.contextPath}/pages/admin-cruise?action=form-insert"><fmt:message
        key="cruiseInsert"/></a>
<tr>
<th><fmt:message key="id"/></th>
<th><fmt:message key="name"/></th>
<th><fmt:message key="liner"/></th>
<th><fmt:message key="route"/></th>
<th><fmt:message key="startDatetime"/></th>
<th><fmt:message key="endDatetime"/></th>
<th><fmt:message key="price"/></th>
<th><fmt:message key="status"/></th>
    </tr>
    <c:forEach var="cruise" items="${requestScope.cruiseList}">
        <tr>
            <td><c:out value="${cruise.id}"/></td>
            <td>${cruise.name} <a
                    href="${pageContext.request.contextPath}/pages/cruise?action=find-by-id&id=${cruise.id}">[view]</a>
            </td>
            <td><c:out value="${cruise.liner.name}"/>[<c:out value="${cruise.liner.passengerCapacity}"/>]</td>
            <td><c:out value="${cruise.route.name}"/>[<c:out value="${cruise.route.pointCount}"/>]</td>
            <td><c:out value="${cruise.startDateTime}"/></td>
            <td><c:out value="${cruise.endDateTime}"/></td>
            <td><c:out value="${cruise.price}"/> <ct:currencyTag>UAH</ct:currencyTag></td>
            <td><c:out value="${requestScope.statusDict[cruise.statusId]}"/></td>
            <td>
                <a class="btn btn-warning"
                   href="${pageContext.request.contextPath}/pages/admin-cruise?action=form-update&id=<c:out value="${cruise.id}"/>"><fmt:message key="edit"/></a>
            </td>
            <td>
                <a class="btn btn-danger"
                   href="${pageContext.request.contextPath}/pages/admin-cruise?action=delete-by-id&id=<c:out value="${cruise.id}"/>"><fmt:message key="delete"/></a>
            </td>
        </tr>
    </c:forEach>
    </table>
    </div>
    <%@include file="footer.jspf" %>
    </body>
    </html>
