<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="ticketFindAll"/> | <fmt:message key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <h3><fmt:message key="ticketFindAll"/></h3>
        </div>
    </div>
    <a class="btn btn-outline-info" href="${pageContext.request.contextPath}/pages/admin-panel"><fmt:message
            key="backToAdminPanel"/></a>
    <table class="table table-bordered">
        <caption><fmt:message key="listOfTickets"/></caption>
        <tr>
            <th><fmt:message key="id"/> (<fmt:message key="ticket"/>)</th>
            <th><fmt:message key="id"/> (<fmt:message key="cruise"/>)</th>
            <th><fmt:message key="id"/> (<fmt:message key="user"/>)</th>
            <th><fmt:message key="price"/></th>
            <th><fmt:message key="status"/></th>
        </tr>
        <c:forEach var="ticket" items="${requestScope.ticketList}">
            <tr>
                <td><c:out value="${ticket.id}"/></td>
                <td><c:out value="${ticket.cruiseId}"/></td>
                <td><c:out value="${ticket.userId}"/></td>
                <td><c:out value="${ticket.price}"/></td>
                <td><c:out value="${requestScope.statusDict[ticket.statusId]}"/></td>
                <td>
                    <a class="btn btn-warning"
                       href="${pageContext.request.contextPath}/pages/admin-ticket?action=form-update&id=<c:out value="${ticket.id}"/>"><fmt:message
                            key="edit"/></a>
                </td>
            </tr>
        </c:forEach>

    </table>
</div>

<%@include file="footer.jspf" %>
</body>
</html>
