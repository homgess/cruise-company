<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="ticketUpdate"/> | <fmt:message key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <h3><fmt:message key="ticketUpdate"/></h3>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6">
            <form method="POST" action="${pageContext.request.contextPath}/pages/admin-ticket">
                <input type="hidden" name="action" value="update">
                <div class="mb-3">
                    <label class="form-label" for="ticket_id"><fmt:message key="id"/> (<fmt:message
                            key="ticket"/>): </label>
                    <input class="disabled" readonly name="id" value="${requestScope.ticket.id}" id="ticket_id">
                </div>
                <p><fmt:message key="id"/> (<fmt:message key="cruise"/>): ${requestScope.ticket.cruiseId}</p>
                <p><fmt:message key="id"/> (<fmt:message key="user"/>): ${requestScope.ticket.userId}</p>
                <p><fmt:message key="price"/>: ${requestScope.ticket.price}</p>
                <p><fmt:message key="documents"/>: <a
                        href="${pageContext.request.contextPath}/data/docs/${requestScope.ticket.docsImageName}">${requestScope.ticket.docsImageName}</a>
                </p>
                <p><fmt:message key="status"/>: ${requestScope.statusDict[requestScope.ticket.statusId]}</p>
                <div class="mb-3">
                    <label class="form-label" for="status_id"><fmt:message key="updateStatus"/>: </label>
                    <select class="form-select" name="status_id" id="status_id">
                        <c:forEach var="status" items="${requestScope.statusDict}">
                            <option value="${status.key}"
                                    <c:if test="${requestScope.ticket.statusId == status.key}">selected</c:if>>${status.value}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="mb-3">
                    <input class="btn btn-primary" type="submit" value="<fmt:message key="save"/>">
                    <input class="btn btn-secondary" type="reset" value="<fmt:message key="reset"/>">
                </div>
            </form>
        </div>
    </div>
</div>

<%@include file="footer.jspf" %>
</body>
</html>
