<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/customTagLibrary" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="cruiseFindById"/> - ${requestScope.cruise.name} | <fmt:message
            key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <h3><fmt:message key="cruiseFindById"/></h3>
        </div>
    </div>
    <a class="btn btn-outline-info" href="${pageContext.request.contextPath}/pages/cruise?action=find-all"><fmt:message
            key="backToAllCruises"/></a>
    <div class="row mt-2">
        <div class="col">
            <div class="row mb-5">
                <p><fmt:message key="name"/>: ${requestScope.cruise.name}</p>
                <p><fmt:message key="description"/>: ${requestScope.cruise.description}</p>
            </div>
            <div class="row mb-5">
                <p><fmt:message key="startDatetime"/>: ${requestScope.cruise.startDateTime}</p>
                <p><fmt:message key="endDatetime"/>: ${requestScope.cruise.endDateTime}</p>
                <p><fmt:message key="liner"/>: ${requestScope.cruise.liner.name}</p>
                <p><fmt:message key="passengerCapacity"/>: ${requestScope.cruise.liner.passengerCapacity}</p>
                <p><fmt:message key="price"/>: ${requestScope.cruise.price} <ct:currencyTag>UAH</ct:currencyTag></p>
            </div>
            <div class="row mb-5">
                <p><fmt:message key="route"/>: ${requestScope.cruise.route.name}</p>
                <ol class="list-group list-group-numbered">
                    <c:forEach var="point" items="${requestScope.cruise.route.routePointList}">
                        <li class="list-group-item">${point.name}</li>
                    </c:forEach>
                </ol>
            </div>
            <c:if test="${requestScope.cruise.statusId == 2}">
                <form method="get" action="${pageContext.request.contextPath}/pages/ticket">
                    <input type="hidden" name="action" value="form-create">
                    <input type="hidden" name="cruise_id" value="${requestScope.cruise.id}">
                    <input class="btn btn-success" type="submit" value="<fmt:message key="order"/>">
                </form>
            </c:if>
        </div>
        <div class="col">
            <img src="${pageContext.request.contextPath}/data/cruise/<c:out value="${requestScope.cruise.imageName}"/>"
                 style="width:500px" alt="${requestScope.cruise.name}">
        </div>
    </div>
</div>
<%@include file="footer.jspf" %>
</body>
</html>
