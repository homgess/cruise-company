<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/customTagLibrary" prefix="ct" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <title><fmt:message key="cruiseFindAll"/> | <fmt:message key="cruiseCompany"/></title>
    <%@include file="header.jspf" %>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <h3><fmt:message key="cruiseFindAll"/></h3>
        </div>
    </div>
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <form method="GET" action="${pageContext.request.contextPath}/pages/cruise">
                <div class="input-group mb-3">
                    <label class="input-group-text" for="start_datetime"><fmt:message key="startDate"/></label>
                    <select class="form-select-sm" name="datetime_type">
                        <option
                                <c:if test="${fn:substring(requestScope.datetime_type, 0, 2) == 'le'}">selected</c:if>
                                value="le"><=
                        </option>
                        <option
                                <c:if test="${fn:substring(requestScope.datetime_type, 0, 2) == 'ge'}">selected</c:if>
                                value="ge">>=
                        </option>
                    </select>
                    <input class="form-control-sm" type="date" name="start_datetime" id="start_datetime"
                           value="${requestScope.startDateTime}"><br>
                </div>
                <div class="input-group mb-3">
                    <label class="input-group-text" for="duration"><fmt:message key="duration"/> </label>
                    <select class="form-select-sm" name="duration_type">
                        <option
                                <c:if test="${fn:substring(requestScope.durationType, 0, 2) == 'le'}">selected</c:if>
                                value="le"><=
                        </option>
                        <option
                                <c:if test="${fn:substring(requestScope.durationType, 0, 2) == 'ge'}">selected</c:if>
                                value="ge">>=
                        </option>
                    </select>
                    <input class="form-control-sm" type="number" name="duration" id="duration"
                           value="${requestScope.duration}">
                    <select class="form-select-sm" name="duration_time_type">
                        <option
                                <c:if test="${fn:substring(requestScope.durationTimeType, 0, 3) == 'day'}">selected</c:if>
                                value="day"><fmt:message key="days"/></option>
                        <option
                                <c:if test="${fn:substring(requestScope.durationTimeType, 0, 4) == 'week'}">selected</c:if>
                                value="week"><fmt:message key="weeks"/></option>
                        <option
                                <c:if test="${fn:substring(requestScope.durationTimeType, 0, 5) == 'month'}">selected</c:if>
                                value="month"><fmt:message key="month"/></option>
                    </select>
                    <input class="btn ms-5 btn-secondary" type="submit" value="<fmt:message key="search"/>">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container">
    <h5><fmt:message key="foundCruises"/>: ${requestScope.cruiseCount}</h5>
    <c:forEach var="cruise" items="${requestScope.cruiseList}">
        <div class="cruise-item row mb-2 border-bottom border-3 mt-2">
            <div class="col col-8 align-items-center justify-content-center px-4 mt-2">
                <div class="row">
                    <h4>
                        <a href="${pageContext.request.contextPath}/pages/cruise?action=find-by-id&id=<c:out value="${cruise.id}"/>">
                            <c:out value="${cruise.name}"/></a></h4>
                </div>
                <div class="row row-cols-auto mt-lg-3 mb-lg-1 justify-content-md-between">
                    <div class="col col-lg-auto"><fmt:message key="liner"/>: <c:out value="${cruise.liner.name}"/></div>
                    <div class="col col-lg-auto"><fmt:message key="route"/>: <c:out value="${cruise.route.name}"/></div>

                </div>
                <div class="row row-cols-auto justify-content-md-between">
                    <div class="col d-inline-block"><fmt:message key="startDatetime"/>: <c:out
                            value="${cruise.startDateTime}"/></div>
                    <div class="col d-inline"><fmt:message key="endDatetime"/>: <c:out
                            value="${cruise.endDateTime}"/></div>
                    <div class="col d-inline-flex"><fmt:message key="price"/>: <c:out value="${cruise.price}"/>
                        <ct:currencyTag>UAH</ct:currencyTag></div>
                </div>
            </div>
            <div class="cruise-img col col-3">
                <img class="img-thumbnail img-fluid"
                     src="${pageContext.request.contextPath}/data/cruise/<c:out value="${ cruise.imageName}"/>"
                     alt="${cruise.name}">
            </div>
        </div>
    </c:forEach>
</div>
<nav>
    <ul class="pagination justify-content-center">
        <c:if test="${requestScope.page > 1}">
            <li class="page-item"><a class="page-link"
                                     href="${pageContext.request.contextPath}/pages/cruise?page=${requestScope.page-1}&count=${requestScope.countPerPage}<c:if test="${not empty requestScope.startDateTime}">&datetime_type=${requestScope.datetime_type}&start_datetime=${requestScope.startDateTime}</c:if><c:if test="${requestScope.duration > 0}">&duration_type=${requestScope.durationType}&duration=${requestScope.duration}</c:if>">
                <span aria-hidden="true">&laquo;</span>
            </a>
            </li>
        </c:if>
        <c:forEach begin="1"
                   end="${(requestScope.cruiseCount/requestScope.countPerPage) + ((requestScope.cruiseCount/requestScope.countPerPage) % 1 == 0 ? 0 : 1)}"
                   var="number" step="1">
            <li class="page-item <c:if test="${requestScope.page == number}">active</c:if> "><a class="page-link"
                                                                                                href="${pageContext.request.contextPath}/pages/cruise?page=${number}&count=${requestScope.countPerPage}<c:if test="${not empty requestScope.startDateTime}">&datetime_type=${requestScope.datetime_type}&start_datetime=${requestScope.startDateTime}</c:if><c:if test="${requestScope.duration > 0}">&duration_type=${requestScope.durationType}&duration=${requestScope.duration}&duration_time_type=${requestScope.durationTimeType}</c:if>">${number}</a>
            </li>
        </c:forEach>
        <c:if test="${requestScope.page < (requestScope.cruiseCount/requestScope.countPerPage)}">
            <li class="page-item"><a class="page-link"
                                     href="${pageContext.request.contextPath}/pages/cruise?page=${requestScope.page+1}&count=${requestScope.countPerPage}<c:if test="${not empty requestScope.startDateTime}">&datetime_type=${requestScope.datetime_type}&start_datetime=${requestScope.startDateTime}</c:if><c:if test="${requestScope.duration > 0}">&duration_type=${requestScope.durationType}&duration=${requestScope.duration}</c:if>">
                <span aria-hidden="true">&raquo;</span>
            </a>
            </li>
        </c:if>
    </ul>
</nav>
<%@include file="footer.jspf" %>
</body>
</html>
