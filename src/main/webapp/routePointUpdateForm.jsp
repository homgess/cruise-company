<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="routePointUpdate"/> - ${requestScope.route.name} | <fmt:message
            key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <h3><fmt:message key="routePointUpdate"/></h3>
        </div>
    </div>
    <div class="mb-3">
        <label for="route_id"><fmt:message key="routeId"/>:</label>
        <input class="disabled" disabled name="route_id" value="${requestScope.route.id}" id="route_id">
    </div>
    <a class="btn btn-outline-info"
       href="${pageContext.request.contextPath}/pages/admin-route?action=find-all"><fmt:message
            key="backToRoutes"/> </a>
    <a class="btn btn-outline-primary"
       href="${pageContext.request.contextPath}/pages/admin-routepoint?action=insert&route_id=<c:out value="${requestScope.route.id}"/>"><fmt:message
            key="routePointInsert"/></a>
    <table class="table table-bordered">
        <caption><fmt:message key="listOfRoutePoints"/></caption>
        <tr>
            <th><fmt:message key="id"/></th>
            <th><fmt:message key="name"/></th>
        </tr>
        <c:forEach var="routePoint" items="${requestScope.route.routePointList}">
            <tr>
                <td>${routePoint.id}</td>
                <c:choose>
                    <c:when test="${requestScope.point_id == routePoint.id}">
                        <td>
                            <form action="${pageContext.request.contextPath}/pages/admin-routepoint">
                                <input type="hidden" name="action" value="update">
                                <input type="hidden" name="route_id" value="${requestScope.route.id}">
                                <input type="hidden" name="point_id" value="${routePoint.id}">
                                <input name="point_name" value="<c:out value="${routePoint.name}"/>">
                                <input class="btn btn-primary" type="submit" value="<fmt:message key="save"/>">
                            </form>
                        </td>
                    </c:when>
                    <c:otherwise>
                        <td>${routePoint.name}
                            <a class="btn btn-warning"
                               href="${pageContext.request.contextPath}/pages/admin-routepoint?action=form-update&route_id=<c:out value="${requestScope.route.id}"/>&point_id=<c:out value="${routePoint.id}"/>"><fmt:message
                                    key="edit"/></a>
                            <a class="btn btn-danger"
                               href="${pageContext.request.contextPath}/pages/admin-routepoint?action=delete-by-id&route_id=<c:out value="${requestScope.route.id}"/>&point_id=<c:out value="${routePoint.id}"/>"><fmt:message
                                    key="delete"/></a><br>
                        </td>
                    </c:otherwise>
                </c:choose>
            </tr>
        </c:forEach>
    </table>
</div>
<%@include file="footer.jspf" %>
</body>
</html>
