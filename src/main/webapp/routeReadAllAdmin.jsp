<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="routeFindAll"/> | <fmt:message key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <h3><fmt:message key="routeFindAll"/></h3>
        </div>
    </div>
    <a class="btn btn-outline-info" href="${pageContext.request.contextPath}/pages/admin-panel"><fmt:message
            key="backToAdminPanel"/></a>
    <a class="btn btn-info" href="${pageContext.request.contextPath}/pages/admin-route?action=form-insert"><fmt:message
            key="routeInsert"/></a>
    <table class="table table-bordered">
        <caption><fmt:message key="listOfRoutes"/></caption>
        <tr>
            <th><fmt:message key="id"/></th>
            <th><fmt:message key="name"/></th>
            <th><fmt:message key="routePoint"/></th>
        </tr>
        <c:forEach var="route" items="${requestScope.routeList}">
            <tr>
                <td><c:out value="${route.id}"/></td>
                <td><c:out value="${route.name}"/></td>
                <td>
                    <c:choose>
                        <c:when test="${(route.routePointList.size()) > 0}">
                            <ol class="list-group list-group-numbered">
                                <c:forEach var="routePoint" items="${route.routePointList}">
                                    <li class="list-group-item"><c:out value="${routePoint.name}"/></li>
                                </c:forEach>
                            </ol>
                        </c:when>
                        <c:otherwise>
                            <p><fmt:message key="empty"/></p>
                        </c:otherwise>
                    </c:choose>

                </td>
                <td>
                    <a class="btn btn-warning"
                       href="${pageContext.request.contextPath}/pages/admin-route?action=form-update&route_id=<c:out value="${route.id}"/>"><fmt:message
                            key="routeUpdate"/></a></td>
                <td>
                    <a class="btn btn-outline-warning"
                       href="${pageContext.request.contextPath}/pages/admin-routepoint?action=find-by-route-id&route_id=<c:out value="${route.id}"/>"><fmt:message
                            key="routePointUpdate"/></a></td>
                <td>
                    <a class="btn btn-danger"
                       href="${pageContext.request.contextPath}/pages/admin-route?action=delete-by-id&route_id=<c:out value="${route.id}"/>"><fmt:message
                            key="delete"/></a></td>
            </tr>
        </c:forEach>

    </table>
</div>

<%@include file="footer.jspf" %>
</body>
</html>
