<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="error"/> | <fmt:message key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <h3><fmt:message key="error"/></h3>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-9">
            <h5 class="text-danger">
                <c:choose>
                    <c:when test="${not empty requestScope.error}">
                        <fmt:message key="${requestScope.error}"/><br>
                    </c:when>
                    <c:otherwise>
                        <fmt:message key="somethingGoneWrong"/>
                    </c:otherwise>
                </c:choose>
            </h5>
            <a href="${pageContext.request.contextPath}/pages/index"><fmt:message key="backToIndex"/></a>
        </div>
    </div>
</div>
<%@include file="footer.jspf" %>
</body>
</html>