<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="locale"/>
<!DOCTYPE html>
<html lang="${sessionScope.lang}">
<head>
    <%@include file="header.jspf" %>
    <title><fmt:message key="routeUpdate"/> - ${requestScope.route.name} | <fmt:message key="cruiseCompany"/></title>
</head>
<body>
<%@include file="navbar.jspf" %>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <h3><fmt:message key="routeUpdate"/></h3>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6">
            <form method="post" action="${pageContext.request.contextPath}/pages/admin-route">
                <input type="hidden" name="action" value="update">
                <div class="mb-3">
                    <label class="form-label" for="id"><fmt:message key="id"/>:</label>
                    <input class="form-control disabled" readonly name="route_id" value="${requestScope.route.id}"
                           id="id">
                </div>
                <div class="mb-3">
                    <label class="form-label" for="name"><fmt:message key="name"/>:</label>
                    <input class="form-control" name="name" value="${requestScope.route.name}" id="name"><br>
                </div>
                <div class="mb-3">
                    <input class="btn btn-primary" type="submit" value="<fmt:message key="save"/>">
                    <input class="btn btn-secondary" type="reset" value="<fmt:message key="reset"/>">
                </div>
            </form>
        </div>
    </div>
</div>

<%@include file="footer.jspf" %>
</body>
</html>
