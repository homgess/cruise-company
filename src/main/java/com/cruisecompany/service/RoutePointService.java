package com.cruisecompany.service;


import com.cruisecompany.controller.util.EntityNotDeletedException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.dao.DAOFactory;
import com.cruisecompany.dao.RoutePointDAO;
import com.cruisecompany.entity.RoutePoint;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;


public class RoutePointService {
    private static final Logger logger = LogManager.getLogger(RoutePointService.class);
    final DAOFactory daoFactory;
    final RoutePointDAO routePointDAO;

    public RoutePointService() {
        daoFactory = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
        routePointDAO = daoFactory.getRoutePointDAO();
    }

    public RoutePointService(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
        routePointDAO = daoFactory.getRoutePointDAO();
    }

    public void saveRoutePoint(RoutePoint routePoint) throws SQLException, EntityNotInsertedException {
        logger.debug("Executing service operation: insert routepoint by route id [{}]", routePoint.getRouteId());
        routePointDAO.insert(routePoint);
    }

    public void modifyRoutePoint(RoutePoint routePoint) throws SQLException, EntityNotUpdatedException {
        logger.debug("Executing service operation: modify route-point [{}]", routePoint.getName());
        routePointDAO.update(routePoint);
    }

    public void deleteRoutePointById(int id) throws SQLException, EntityNotDeletedException {
        logger.debug("Executing service operation: delete route-point by id [{}]", id);
        routePointDAO.deleteById(id);
    }
}
