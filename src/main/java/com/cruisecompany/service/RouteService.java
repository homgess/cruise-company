package com.cruisecompany.service;


import com.cruisecompany.controller.util.EntityNotDeletedException;
import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.dao.DAOFactory;
import com.cruisecompany.dao.RouteDAO;
import com.cruisecompany.entity.Route;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;


public class RouteService {
    private static final Logger logger = LogManager.getLogger(RouteService.class);
    final DAOFactory daoFactory;
    final RouteDAO routeDAO;

    public RouteService() {
        daoFactory = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
        routeDAO = daoFactory.getRouteDAO();
    }

    public RouteService(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
        routeDAO = daoFactory.getRouteDAO();
    }

    public List<Route> findAllRoutes() throws SQLException, EntityNotFoundException {
        logger.debug("Executing service operation: find all routes");
        return routeDAO.findAll();
    }

    public Route findRouteById(int id) throws SQLException, EntityNotFoundException {
        logger.debug("Executing service operation: route by id [{}]", id);
        return routeDAO.findById(id);
    }

    public void saveRoute(Route route) throws SQLException, EntityNotInsertedException {
        logger.debug("Executing service operation: save route [{}]", route.getName());
        routeDAO.insert(route);
    }

    public void modifyRoute(Route route) throws SQLException, EntityNotUpdatedException {
        logger.debug("Executing service operation: modify route [{}]", route.getName());
        routeDAO.update(route);
    }

    public void deleteRouteById(int id) throws SQLException, EntityNotDeletedException {
        logger.debug("Executing service operation: delete route by id [{}]", id);
        routeDAO.deleteById(id);
    }
}
