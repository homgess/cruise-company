package com.cruisecompany.service;


import com.cruisecompany.controller.util.EntityNotDeletedException;
import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.dao.CruiseDAO;
import com.cruisecompany.dao.DAOFactory;
import com.cruisecompany.entity.Cruise;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;


public class CruiseService {
    private static final Logger logger = LogManager.getLogger(CruiseService.class);
    final DAOFactory daoFactory;
    final CruiseDAO cruiseDAO;

    public CruiseService() {
        this.daoFactory = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
        cruiseDAO = daoFactory.getCruiseDAO();
    }

    public CruiseService(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
        cruiseDAO = daoFactory.getCruiseDAO();
    }

    public void saveCruise(Cruise cruise) throws SQLException, EntityNotInsertedException, EntityNotFoundException {
        logger.debug("Executing service operation: save cruise [{}]", cruise.getName());
        cruiseDAO.insert(cruise);
        int cruiseId = cruiseDAO.findByName(cruise.getName()).getId();
        cruise.setId(cruiseId);
        cruiseDAO.insertCruiseInfo(cruise);
    }

    public List<Cruise> findAllCruises() throws SQLException, EntityNotFoundException {
        logger.debug("Executing service operation: find all cruises");
        return cruiseDAO.findAll();
    }

    public Cruise findCruiseById(int id) throws SQLException, EntityNotFoundException {
        logger.debug("Executing service operation: find cruise by id [{}]", id);
        return cruiseDAO.findById(id);
    }

    public List<Cruise> findAllCruisesByParameters(String dateTimeType, LocalDate startDateTime, String durationType, int duration, int pageId, int countPerPage) throws SQLException, EntityNotFoundException {
        logger.debug("Executing service operation: find all cruises by parameters");
        return cruiseDAO.findByParameters(dateTimeType, startDateTime, durationType, duration, pageId, countPerPage);
    }


    public void modifyCruise(Cruise cruise) throws SQLException, EntityNotUpdatedException {
        logger.debug("Executing service operation: modify cruise [{}]", cruise.getName());
        cruiseDAO.update(cruise);
        cruiseDAO.updateCruiseInfo(cruise);
    }

    public void deleteCruiseById(int id) throws SQLException, EntityNotDeletedException {
        logger.debug("Executing service operation: delete cruise by id [{}]", id);
        cruiseDAO.deleteById(id);
    }

    public Map<Integer, String> getStatusDict() throws SQLException, EntityNotFoundException {
        logger.debug("Executing service operation: get cruise status dict");
        return cruiseDAO.getStatusDict();
    }

    public int getCruiseWithParametersCount(String dateTimeType, LocalDate startDateTime, String durationType, int duration) throws SQLException {
        logger.debug("Executing service operation: get cruise with parameters dateTimeType:[{}}, startDateTime:[{}], durationType:[{}], duration:[{}] count", dateTimeType, startDateTime, durationType, duration);
        return cruiseDAO.getCruiseCount(dateTimeType, startDateTime, durationType, duration);
    }

    public void updateAllCruisesStatus() throws SQLException {
        logger.debug("Executing service operation: update all cruises status");
        cruiseDAO.updateAllEndedCruiseStatuses();
        cruiseDAO.updateAllStartedCruiseStatuses();
    }

    public void updateCruiseStatus(int id) throws SQLException {
        logger.debug("Executing service operation: cruise status if full");
        cruiseDAO.updateCruiseStatusIfFull(id);
        cruiseDAO.updateCruiseStatusIfNotFull(id);
    }
}
