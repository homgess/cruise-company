package com.cruisecompany.service;


import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.dao.DAOFactory;
import com.cruisecompany.dao.UserDAO;
import com.cruisecompany.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;


public class UserService {
    private static final Logger logger = LogManager.getLogger(UserService.class);
    final DAOFactory daoFactory;
    final UserDAO userDAO;

    public UserService() {
        daoFactory = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
        userDAO = daoFactory.getUserDAO();
    }

    public UserService(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
        userDAO = daoFactory.getUserDAO();
    }

    public List<User> findAllUsers() throws SQLException, EntityNotFoundException {
        logger.debug("Executing service operation: find all users");
        return userDAO.findAll();
    }

    public User findUserById(int id) throws SQLException, EntityNotFoundException {
        logger.debug("Executing service operation: find user by id [{}]", id);
        return userDAO.findById(id);
    }

    public User findUserByLogin(String login) throws SQLException, EntityNotFoundException {
        logger.debug("Executing service operation: find user by login [{}]", login);
        return userDAO.findByLogin(login);
    }

    public void saveUser(User user) throws SQLException, EntityNotInsertedException {
        logger.debug("Executing service operation: save user [{}]", user.getLogin());
        userDAO.insert(user);
    }

    public void modifyUser(User user) throws SQLException, EntityNotUpdatedException {
        logger.debug("Executing service operation: modify user [{}]", user.getLogin());
        userDAO.update(user);
    }

    public void updateLastOnlineDate(int id) throws SQLException, EntityNotUpdatedException {
        logger.debug("Executing service operation: modify last online date [{}]", id);
        userDAO.updateLastOnlineDateById(id);
    }


    public Map<Integer, String> findRoleDict() throws SQLException, EntityNotFoundException {
        logger.debug("Executing service operation: find role dictionary");
        return userDAO.getRoleDict();
    }
}
