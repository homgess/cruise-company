package com.cruisecompany.service;


import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.dao.DAOFactory;
import com.cruisecompany.dao.TicketDAO;
import com.cruisecompany.entity.Ticket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;


public class TicketService {
    private static final Logger logger = LogManager.getLogger(TicketService.class);
    final DAOFactory daoFactory;
    final TicketDAO ticketDAO;

    public TicketService() {
        daoFactory = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
        ticketDAO = daoFactory.getTicketDAO();
    }

    public TicketService(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
        ticketDAO = daoFactory.getTicketDAO();
    }

    public List<Ticket> findAllTickets() throws SQLException, EntityNotFoundException {
        logger.debug("Executing service operation: find all tickets");
        return ticketDAO.findAll();
    }

    public Ticket findTicketById(int id) throws SQLException, EntityNotFoundException {
        logger.debug("Executing service operation: find ticket by id [{}]", id);
        return ticketDAO.findById(id);
    }

    public List<Ticket> findAllTicketsByUserId(int id) throws SQLException, EntityNotFoundException {
        logger.debug("Executing service operation: find all tickets by user id [{}]", id);
        return ticketDAO.findTicketByUserId(id);
    }

    public void saveTicket(Ticket ticket) throws SQLException, EntityNotInsertedException {
        logger.debug("Executing service operation: save ticket [{}]", ticket.getId());
        ticketDAO.insert(ticket);
    }

    public void modifyTicketStatus(int ticketId, int statusId) throws SQLException, EntityNotUpdatedException {
        logger.debug("Executing service operation: modify ticket [{}]. Set status [{}]", ticketId, statusId);
        ticketDAO.updateTicketStatus(ticketId, statusId);
    }

    public Map<Integer, String> findStatusDict() throws SQLException, EntityNotFoundException {
        logger.debug("Executing service operation: find status dictionary");
        return ticketDAO.getStatusDict();
    }

    public List<Ticket> findAllTicketsByCruiseId(int id) throws SQLException, EntityNotFoundException {
        logger.debug("Executing service operation: find all tickets by cruise id");
        return ticketDAO.findTicketByCruiseId(id);
    }

    public void modifyAllTicketStatuses() throws SQLException, EntityNotUpdatedException {
        logger.debug("Executing service operation: update all ticket statuses");
        ticketDAO.updateAllTicketStatus();
    }
}
