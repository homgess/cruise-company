package com.cruisecompany.service;


import com.cruisecompany.controller.util.EntityNotDeletedException;
import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.dao.DAOFactory;
import com.cruisecompany.dao.LinerDAO;
import com.cruisecompany.entity.Liner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;


public class LinerService {
    private static final Logger logger = LogManager.getLogger(LinerService.class);
    final DAOFactory daoFactory;
    final LinerDAO linerDAO;

    public LinerService() {
        this.daoFactory = DAOFactory.getDAOFactory(DAOFactory.MYSQL);
        linerDAO = daoFactory.getLinerDAO();
    }

    public LinerService(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
        linerDAO = daoFactory.getLinerDAO();
    }

    public List<Liner> findAllLiners() throws SQLException, EntityNotFoundException {
        logger.debug("Executing service operation: find all liners");
        return linerDAO.findAll();
    }

    public Liner findById(int id) throws SQLException, EntityNotFoundException {
        logger.debug("Executing service operation: find liner by id [{}]", id);
        return linerDAO.findById(id);
    }

    public void saveLiner(Liner liner) throws SQLException, EntityNotInsertedException {
        logger.debug("Executing service operation: save liner [{}]", liner.getName());
        linerDAO.insert(liner);
    }

    public void modifyLiner(Liner liner) throws SQLException, EntityNotUpdatedException {
        logger.debug("Executing service operation: modify liner [{}]", liner.getName());
        linerDAO.update(liner);
    }

    public void deleteLinerById(int id) throws SQLException, EntityNotDeletedException {
        logger.debug("Executing service operation: delete liner by id [{}]", id);
        linerDAO.deleteById(id);
    }
}
