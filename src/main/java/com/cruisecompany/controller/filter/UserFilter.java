package com.cruisecompany.controller.filter;


import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebFilter(filterName = "UserAuthFilter", urlPatterns = {"/pages/ticket", "/pages/logout"})
public class UserFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        if (req.getSession() != null && req.getSession().getAttribute("loggedUser") != null)
            chain.doFilter(request, response);
        else
            resp.sendRedirect(req.getContextPath() + "/pages/auth");
    }
}
