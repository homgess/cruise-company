package com.cruisecompany.controller.filter;


import com.cruisecompany.entity.User;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebFilter(filterName = "AuthFilter", urlPatterns = {"/pages/admin-panel", "/pages/admin-cruise", "/pages/admin-liner", "/pages/admin-route", "/pages/admin-routepoint", "/pages/admin-ticket", "/pages/admin-user"})
public class AdminFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        if (req.getSession() != null && req.getSession().getAttribute("loggedUser") != null) {
            User user = (User) req.getSession().getAttribute("loggedUser");
            if (user.getRoleId() == 3)
                chain.doFilter(request, response);
            else if (user.getRoleId() < 3)
                resp.sendRedirect(req.getContextPath() + "/pages/index");
        } else resp.sendRedirect(req.getContextPath() + "/pages/auth");
    }
}
