package com.cruisecompany.controller.util;

public class EntityNotDeletedException extends Exception{
    public EntityNotDeletedException(String message) {
        super(message);
    }
}
