package com.cruisecompany.controller.util;


import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Currency;
import java.util.Locale;


public class CurrencyTag extends SimpleTagSupport {
    private String currencyCode;
    final StringWriter sw = new StringWriter();

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Override
    public void doTag() throws JspException, IOException {
        getJspBody().invoke(sw);
        currencyCode = sw.toString();
        Locale ua = new Locale("uk", "UA");
        Currency currency = Currency.getInstance(currencyCode);
        getJspContext().getOut().println(currency.getSymbol(ua));
    }
}
