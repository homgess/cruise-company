package com.cruisecompany.controller.util;

public class EntityNotUpdatedException extends Exception{
    public EntityNotUpdatedException(String message) {
        super(message);
    }
}
