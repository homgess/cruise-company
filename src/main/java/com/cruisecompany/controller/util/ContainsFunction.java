package com.cruisecompany.controller.util;

import java.util.Collection;

public class ContainsFunction {

    private ContainsFunction() {
    }

    public static boolean contains(Collection collection, Object object) {
        return collection.contains(object);
    }
}
