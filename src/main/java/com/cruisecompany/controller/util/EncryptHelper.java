package com.cruisecompany.controller.util;


import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class EncryptHelper {

    private EncryptHelper() {
    }

    public static String encrypt(String str) throws NoSuchAlgorithmException {
        String salt = "CRUISE-CYPHER22";
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(str.getBytes(StandardCharsets.UTF_8));
        messageDigest.update(salt.getBytes(StandardCharsets.UTF_8));
        byte[] hash = messageDigest.digest();
        BigInteger number = new BigInteger(1, hash);
        StringBuilder hexStr = new StringBuilder(number.toString(16));
        while (hexStr.length() < 32) {
            hexStr.insert(0, '0');
        }
        return hexStr.toString();
    }
}
