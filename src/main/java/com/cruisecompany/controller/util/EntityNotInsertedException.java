package com.cruisecompany.controller.util;

public class EntityNotInsertedException extends Exception{
    public EntityNotInsertedException(String message) {
        super(message);
    }
}
