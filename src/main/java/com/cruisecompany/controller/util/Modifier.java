package com.cruisecompany.controller.util;


import java.util.Objects;


public class Modifier {
        private final String sessionId;
        private final String entityName;
        private final int entityId;

    public Modifier(String sessionId, String entityName, int entityId) {
        this.sessionId = sessionId;
        this.entityName = entityName;
        this.entityId = entityId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getEntityName() {
        return entityName;
    }

    public int getEntityId() {
        return entityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Modifier modifier = (Modifier) o;
        return Objects.equals(entityName, modifier.entityName) && Objects.equals(entityId, modifier.entityId);
    }

    @Override
        public int hashCode() {
            return Objects.hash(entityName, entityId);
        }

        @Override
        public String toString() {
            return "EntityModifier{" +
                    "sessionId='" + sessionId + '\'' +
                    ", entityName='" + entityName + '\'' +
                    ", entityId='" + entityId + '\'' +
                    '}';
        }
}
