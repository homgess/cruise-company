package com.cruisecompany.controller.util;

import java.util.*;
import java.util.function.Predicate;

public class ModifierList extends AbstractList<Modifier>{
    private List<Modifier> modifiers;

    public ModifierList() {
        modifiers = new ArrayList<>();
    }

    public ModifierList(Object object) {
        if (object instanceof ModifierList){
            modifiers = new ArrayList<>();
            ModifierList objectAsModifierList = (ModifierList) object;
            modifiers.addAll(objectAsModifierList);
        }

}

    @Override
    public boolean add(Modifier modifier) {
        return modifiers.add(modifier);
    }

    @Override
    public boolean addAll(int index, Collection<? extends Modifier> c) {
        return modifiers.addAll(c);
    }

    @Override
    public Modifier remove(int index) {
        return modifiers.remove(index);
    }

    @Override
    public boolean removeIf(Predicate<? super Modifier> filter) {
        return modifiers.removeIf(filter);
    }

    @Override
    public Modifier get(int index) {
        return modifiers.get(index);
    }

    @Override
    public int size() {
        return modifiers.size();
    }

    public boolean isContainsAnotherSession(Modifier modifier){
        return modifiers.contains(modifier) && !modifiers.get(modifiers.indexOf(modifier)).getSessionId().equals(modifier.getSessionId());
    }

    public boolean isContainsThisSession(Modifier modifier){
        return modifiers.contains(modifier) && modifiers.get(modifiers.indexOf(modifier)).getSessionId().equals(modifier.getSessionId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ModifierList that = (ModifierList) o;
        return Objects.equals(modifiers, that.modifiers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), modifiers);
    }
}

