package com.cruisecompany.controller.util;


import com.cruisecompany.entity.*;

import java.math.BigDecimal;
import java.time.LocalDate;


public class Validator {
    private Validator() {
    }


    public static class Insert {
        private Insert() {
        }

        public static boolean isUserValid(User user) {
            return user.getLogin().matches("^\\w{3,20}$") &&
                    user.getPassword().matches("^\\w{3,20}$");
        }

        public static boolean isCruiseValid(Cruise cruise) {
            return cruise.getName().matches("^(\\S).{1,120}(\\S)$") &&
                    cruise.getLiner().getId() > 0 &&
                    cruise.getRoute().getId() > 0 &&
                    cruise.getStartDateTime().isBefore(cruise.getEndDateTime()) &&
                    cruise.getPrice().compareTo(BigDecimal.ZERO) > 0 &&
                    !cruise.getImageName().isBlank() &&
                    !cruise.getDescription().isBlank();
        }

        public static boolean isLinerValid(Liner liner) {
            return liner.getName().matches("^(\\S).{1,40}(\\S)$") &&
                    liner.getPassengerCapacity() >= 0;
        }

        public static boolean isRouteValid(Route route) {
            return route.getName().matches("^(\\S).{1,40}(\\S)$");
        }

        public static boolean isRoutePointValid(RoutePoint routePoint) {
            return routePoint.getRouteId() > 0;
        }

        public static boolean isTicketValid(Ticket ticket) {
            return ticket.getCruiseId() > 0 &&
                    ticket.getUserId() > 0 &&
                    ticket.getPosition() > 0 &&
                    ticket.getPrice().compareTo(BigDecimal.ZERO) > 0 &&
                    !ticket.getDocsImageName().isBlank();
        }
    }

    public static class Update {
        private Update() {
        }

        public static boolean isUserValid(User user) {
            return user.getId() > 0 &&
                    user.getLogin().matches("^\\w{3,20}$") &&
                    user.getPassword().matches("^\\w{3,20}$") &&
                    user.getRoleId() >= 1 && user.getRoleId() <= 3;
        }

        public static boolean isCruiseValid(Cruise cruise) {
            return cruise.getId() > 0 &&
                    cruise.getName().matches("^(\\S).{1,120}(\\S)$") &&
                    cruise.getLiner().getId() > 0 &&
                    cruise.getRoute().getId() > 0 &&
                    cruise.getStartDateTime().isBefore(cruise.getEndDateTime()) &&
                    cruise.getPrice().compareTo(BigDecimal.ZERO) > 0 &&
                    !cruise.getImageName().isBlank() &&
                    !cruise.getDescription().isBlank() && cruise.getStatusId() >= 1 && cruise.getStatusId() <= 6;
        }

        public static boolean isLinerValid(Liner liner) {
            return liner.getId() > 0 &&
                    liner.getName().matches("^(\\S).{1,40}(\\S)$") &&
                    liner.getPassengerCapacity() >= 0;
        }

        public static boolean isRouteValid(Route route) {
            return route.getId() > 0 &&
                    route.getName().matches("^(\\S).{1,40}(\\S)$") && route.getPointCount() >= 0;
        }

        public static boolean isRoutePointValid(RoutePoint routePoint) {
            return routePoint.getId() > 0 &&
                    routePoint.getRouteId() > 0 && routePoint.getName().matches("^(\\S).{1,40}(\\S)$");
        }
    }

    public static boolean isCruiseParametersValid(String dateTimeType, LocalDate startDateTime, String durationType, int duration) {
        return ((startDateTime == null) || (dateTimeType.equals("le") || dateTimeType.equals("ge")) && startDateTime != null) &&
                ((duration == 0) || (durationType.equals("le") || durationType.equals("ge")) && duration > 0);
    }

    public static boolean isPaginationValid(int pageId, int countPerPage) {
        return pageId >= 0 && countPerPage >= 0;
    }
}
