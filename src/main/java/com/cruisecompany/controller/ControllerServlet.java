package com.cruisecompany.controller;


import com.cruisecompany.controller.command.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;


@WebServlet(urlPatterns = {"/pages/*"}, initParams = {
        @WebInitParam(name = "DATA_PATH", value = "data\\"),
        @WebInitParam(name = "TMP_PATH", value = "D:\\tmp\\")})
@MultipartConfig(
        fileSizeThreshold = 1024 * 10,  // 10 KB
        maxFileSize = 1024 * 300,       // 300 KB
        maxRequestSize = 1024 * 1024)    // 1 MB
public class ControllerServlet extends HttpServlet {
    private final Map<String, Command> commandMap = new HashMap<>();
    private static final Logger logger = LogManager.getLogger(ControllerServlet.class);
    private static ServletConfig config;

    public static ServletConfig getConfig() {
        return config;
    }

    @Override
    public void init() {
        config = getServletConfig();
        logger.info("Initializing ControllerServlet#init");
        commandMap.put("/cruise", new CruiseCommand());
        commandMap.put("/admin-panel", new AdminPanelCommand());
        commandMap.put("/admin-cruise", new AdminCruiseCommand());
        commandMap.put("/admin-liner", new AdminLinerCommand());
        commandMap.put("/admin-route", new AdminRouteCommand());
        commandMap.put("/admin-routepoint", new AdminRoutePointCommand());
        commandMap.put("/admin-user", new AdminUserCommand());
        commandMap.put("/admin-ticket", new AdminTicketCommand());
        commandMap.put("/auth", new AuthCommand());
        commandMap.put("/sign-out", new LogoutCommand());
        commandMap.put("/ticket", new TicketCommand());
        commandMap.put("/error", new ErrorCommand());
        commandMap.put("/about", new AboutCommand());
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Initializing ControllerServlet#service");
        logger.debug("Received request path: [{}].", req.getPathInfo());
        Command command = commandMap.getOrDefault(req.getPathInfo(), new IndexPageCommand());
        logger.debug("[{}] command interpreted as [{}]", req.getPathInfo(), command);
        String result = command.execute(req, resp);
        logger.debug("Execute result: {}", result);
        if (result.matches("^re.+$")) {
            result = result.replaceAll("^re:", "");
            resp.sendRedirect(req.getContextPath() + result);
        } else req.getRequestDispatcher(result).forward(req, resp);
    }
}