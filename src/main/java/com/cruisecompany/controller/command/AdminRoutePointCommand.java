package com.cruisecompany.controller.command;


import com.cruisecompany.controller.util.*;
import com.cruisecompany.entity.Route;
import com.cruisecompany.entity.RoutePoint;
import com.cruisecompany.service.RoutePointService;
import com.cruisecompany.service.RouteService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;


public class AdminRoutePointCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminRoutePointCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Received request: [{}]", request.getRequestURI());
        String action = request.getParameter("action");
        RouteService routeService = new RouteService();
        RoutePointService routePointService = new RoutePointService();
        if (action == null) {
            logger.debug("Null action interpreted as find-all-routepoints-by-route-id");
            return findRoutepointsByRouteId(request, routeService);
        }
        switch (action) {
            case "find-by-route-id": {
                logger.debug("Action interpreted as find-routepoints-by-route-id");
                return findRoutepointsByRouteId(request, routeService);
            }
            case "insert": {
                logger.debug("Action interpreted as insert-routepoint");
                return insertRoutePoint(request, routePointService);
            }
            case "form-update": {
                logger.debug("Action interpreted as form-update-routepoint");
                return formUpdateRoutepoint(request, routeService);
            }
            case "update": {
                logger.debug("Action interpreted as update-routepoint");
                return updateRoutepoint(request, routePointService);
            }
            case "delete-by-id": {
                logger.debug("Action interpreted as delete-routepoint-by-id");
                return deleteRoutepointById(request, routePointService);
            }
            default:
                return generateErrorPage("errorWrongPage", logger, request);
        }
    }

    private String findRoutepointsByRouteId(HttpServletRequest request, RouteService routeService) {
        int id;
        Route route;
        try {
            id = Integer.parseInt(request.getParameter("route_id"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        try {
            route = routeService.findRouteById(id);
        } catch (SQLException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToFindData", logger, request);
        }
        request.setAttribute("route", route);
        return "/routePointUpdateForm.jsp";
    }

    private String insertRoutePoint(HttpServletRequest request, RoutePointService routePointService) {
        int routeId;
        try {
            routeId = Integer.parseInt(request.getParameter("route_id"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        RoutePoint routePoint = new RoutePoint(routeId);
        if (!Validator.Insert.isRoutePointValid(routePoint))
            return generateErrorPage("errorWrongInputData", logger, request);
        try {
            routePointService.saveRoutePoint(routePoint);
        } catch (SQLException | EntityNotInsertedException e) {
            return generateErrorPage("errorFailedToSaveData", logger, request);
        }
        return "re:/pages/admin-routepoint?action=form-update&route_id=" + routeId;
    }

    private String formUpdateRoutepoint(HttpServletRequest request, RouteService routeService) {
        int id;
        Route route;
        try {
            id = Integer.parseInt(request.getParameter("route_id"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        if (isDataBeingInUse(request, "route", id, false))
            return generateErrorPage("errorDataIsBeingModified", logger, request);
        try {
            route = routeService.findRouteById(id);
        } catch (SQLException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToUpdateData", logger, request);
        }
         if (request.getParameter("point_id") != null)
             request.setAttribute("point_id", request.getParameter("point_id"));
        request.setAttribute("route", route);
        return "/routePointUpdateForm.jsp";
    }

    private String updateRoutepoint(HttpServletRequest request, RoutePointService routePointService) {
        RoutePoint routePoint;
        int pointId;
        int routeId;
        try {
            pointId = Integer.parseInt(request.getParameter("point_id"));
            routeId = Integer.parseInt(request.getParameter("route_id"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        if (isDataBeingInUse(request, "route", routeId, true))
            return generateErrorPage("errorDataIsBeingModified", logger, request);
        String name = request.getParameter("point_name");
        routePoint = new RoutePoint(pointId, routeId, name);
        if (!Validator.Update.isRoutePointValid(routePoint))
            return generateErrorPage("errorWrongInputData", logger, request);
        try {
            routePointService.modifyRoutePoint(routePoint);
        } catch (SQLException | EntityNotUpdatedException e) {
            return generateErrorPage("errorFailedToUpdateData", logger, request);
        }
        return "re:/pages/admin-routepoint?action=find-by-route-id&route_id=" + routeId;
    }

    private String deleteRoutepointById(HttpServletRequest request, RoutePointService routePointService) {
        int id;
        int routeId;
        try {
            id = Integer.parseInt(request.getParameter("point_id"));
            routeId = Integer.parseInt(request.getParameter("route_id"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        if (isDataBeingInUse(request, "route", routeId, true))
            return generateErrorPage("errorDataIsBeingModified", logger, request);
        try {
            routePointService.deleteRoutePointById(id);
        } catch (SQLException | EntityNotDeletedException e) {
            return generateErrorPage("errorFailedToDeleteData", logger, request);
        }
        request.setAttribute("route_id", routeId);
        return "re:/pages/admin-routepoint?action=form-update&route_id=" + routeId;
    }
}
