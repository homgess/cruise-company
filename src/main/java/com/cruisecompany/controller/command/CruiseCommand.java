package com.cruisecompany.controller.command;


import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.Validator;
import com.cruisecompany.entity.Cruise;
import com.cruisecompany.service.CruiseService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;


public class CruiseCommand implements Command {
    private static final Logger logger = LogManager.getLogger(CruiseCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Received request: [{}]", request.getRequestURI());
        String action = request.getParameter("action");
        CruiseService cruiseService = new CruiseService();
        if (action == null) {
            logger.debug("Null action interpreted as find-all-cruises-by-parameters");
            return findAllCruisesByParameters(request, cruiseService);
        }

        if ("find-by-id".equals(action)) {
            logger.debug("Action interpreted as find-by-id");
            return findCruiseById(request, cruiseService);
        }
        return generateErrorPage("errorWrongPage", logger, request);
    }

    private String findAllCruisesByParameters(HttpServletRequest request, CruiseService cruiseService) {
        List<Cruise> cruiseList;
        int pageId;
        int countPerPage;
        int cruiseCount;
        String dateTimeType;
        LocalDate startDateTime;
        String durationType;
        int duration;
        int fixedDuration = 0;
        String durationTimeType;
        dateTimeType = request.getParameter("datetime_type");
        durationType = request.getParameter("duration_type");
        durationTimeType = request.getParameter("duration_time_type");
        try {
            pageId = Integer.parseInt(request.getParameter("page"));
            countPerPage = Integer.parseInt(request.getParameter("count"));
        } catch (NumberFormatException e) {
            pageId = 1;
            countPerPage = 5;
        }
        try {
            startDateTime = LocalDate.parse(request.getParameter("start_datetime"));
        } catch (NullPointerException | DateTimeParseException | NumberFormatException e) {
            startDateTime = null;
        }
        try {
            duration = Integer.parseInt(request.getParameter("duration"));
        } catch (NullPointerException | DateTimeParseException | NumberFormatException e) {
            duration = 0;
        }
        if (durationTimeType != null && durationTimeType.equals("day"))
            fixedDuration = duration;
        if (durationTimeType != null && durationTimeType.equals("week"))
            fixedDuration = duration * 7;
        else if (durationTimeType != null && durationTimeType.equals("month"))
            fixedDuration = duration * 30;
        if (!Validator.isCruiseParametersValid(dateTimeType, startDateTime, durationType, duration) || !Validator.isPaginationValid(pageId,countPerPage))
            return generateErrorPage("errorWrongInputData", logger, request);
        try {
            cruiseList = cruiseService.findAllCruisesByParameters(dateTimeType, startDateTime, durationType, fixedDuration, pageId, countPerPage);
            cruiseCount = cruiseService.getCruiseWithParametersCount(dateTimeType, startDateTime, durationType, fixedDuration);
        } catch (SQLException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToFindData", logger, request);
        }
        request.setAttribute("cruiseList", cruiseList);
        request.setAttribute("page", pageId);
        request.setAttribute("countPerPage", countPerPage);
        request.setAttribute("datetime_type", dateTimeType);
        request.setAttribute("startDateTime", startDateTime);
        request.setAttribute("durationType", durationType);
        request.setAttribute("duration", duration);
        request.setAttribute("durationTimeType", durationTimeType);
        request.setAttribute("cruiseCount", cruiseCount);
        return "/cruiseFindAll.jsp";
    }

    private String findCruiseById(HttpServletRequest request, CruiseService cruiseService) {
        int id;
        try {
            id = Integer.parseInt(request.getParameter("id"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        Cruise cruise;
        try {
            cruise = cruiseService.findCruiseById(id);
        } catch (SQLException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToFindData", logger, request);
        }
        request.setAttribute("cruise", cruise);
        return "/cruiseFindById.jsp";
    }

}
