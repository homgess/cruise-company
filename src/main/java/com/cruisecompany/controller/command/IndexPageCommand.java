package com.cruisecompany.controller.command;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;


public class IndexPageCommand implements Command {
    private static final Logger logger = LogManager.getLogger(IndexPageCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Received request: [{}]", request.getRequestURI());
        List<String> langList = new ArrayList<>();
        langList.add("uk");
        langList.add("en");
        String lang = request.getParameter("lang");
        if (lang != null && langList.contains(lang))
            request.getSession().setAttribute("lang", lang);
        return "/index.jsp";
    }
}
