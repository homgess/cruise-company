package com.cruisecompany.controller.command;


import com.cruisecompany.controller.util.*;
import com.cruisecompany.entity.Cruise;
import com.cruisecompany.entity.Liner;
import com.cruisecompany.entity.Route;
import com.cruisecompany.service.CruiseService;
import com.cruisecompany.service.LinerService;
import com.cruisecompany.service.RouteService;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.*;


public class AdminCruiseCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminCruiseCommand.class);

    private final CruiseService cruiseService;
    private final LinerService linerService;
    private final RouteService routeService;

    public AdminCruiseCommand() {
        this.cruiseService = new CruiseService();
        this.linerService = new LinerService();
        this.routeService = new RouteService();
    }

    public AdminCruiseCommand(CruiseService cruiseService, LinerService linerService, RouteService routeService) {
        this.cruiseService = cruiseService;
        this.linerService = linerService;
        this.routeService = routeService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Received request: [{}]", request.getRequestURI());
        String action;
        Map<String, String> parametersMap = new HashMap<>();
        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                parametersMap = processFileRequest(request, "/data/cruise/", "D://tmp/", logger);
                action = parametersMap.get("action");
            } catch (Exception e) {
                return generateErrorPage("errorFailedToLoadFile", logger, request);
            }
        } else
            action = request.getParameter("action");
        if (action == null) {
            logger.debug("Null action interpreted as find-all-cruises");
            return findAddCruises(request, cruiseService);
        }
        switch (action) {
            case "find-all": {
                logger.debug("Action interpreted as find-all-cruises");
                return findAddCruises(request, cruiseService);
            }
            case "form-insert": {
                logger.debug("Action interpreted as form-insert-cruise");
                return formInsertCruise(request, linerService, routeService);
            }
            case "insert": {
                logger.debug("Action interpreted as insert-cruise");
                return insertCruise(request, cruiseService, parametersMap);
            }
            case "form-update": {
                logger.debug("Action interpreted as form-update-cruise");
                return formUpdateCruise(request, cruiseService, linerService, routeService);
            }
            case "update": {
                logger.debug("Action interpreted as update-cruise");
                return updateCruise(request, cruiseService, parametersMap);
            }
            case "delete-by-id": {
                logger.debug("Action interpreted as delete-cruise-by-id");
                return deleteCruiseById(request, cruiseService);
            }
            default:
                return generateErrorPage("Wrong page", logger, request);
        }
    }

    private String findAddCruises(HttpServletRequest request, CruiseService cruiseService) {
        List<Cruise> cruiseList;
        Map<Integer, String> statusDict;
        try {
            cruiseList = cruiseService.findAllCruises();
            statusDict = cruiseService.getStatusDict();
        } catch (SQLException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToFindData", logger, request);
        }
        request.setAttribute("cruiseList", cruiseList);
        request.setAttribute("statusDict", statusDict);
        return "/cruiseFindAllAdmin.jsp";
    }

    private String formInsertCruise(HttpServletRequest request, LinerService linerService, RouteService routeService) {
        List<Liner> linerList;
        List<Route> routeList;
        try {
            linerList = linerService.findAllLiners();
            routeList = routeService.findAllRoutes();
        } catch (SQLException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToFindData", logger, request);
        }
        request.setAttribute("linerList", linerList);
        request.setAttribute("routeList", routeList);
        return "/cruiseInsertForm.jsp";
    }

    private String insertCruise(HttpServletRequest request, CruiseService cruiseService, Map<String, String> parametersMap) {
        int linerId;
        int routeId;
        LocalDateTime startDateTime;
        LocalDateTime endDatetime;
        BigDecimal price;
        try {
            linerId = Integer.parseInt(parametersMap.get("liner_id"));
            routeId = Integer.parseInt(parametersMap.get("route_id"));
            startDateTime = LocalDateTime.parse(parametersMap.get("start_datetime"));
            endDatetime = LocalDateTime.parse(parametersMap.get("end_datetime"));
            price = new BigDecimal(parametersMap.get("price"));

        } catch (NumberFormatException | DateTimeParseException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        String name = parametersMap.get("name");
        Liner liner = new Liner(linerId);
        Route route = new Route(routeId);
        String description = parametersMap.get("description");
        String imageName = parametersMap.get("image_name");
        Cruise cruise = new Cruise(name, liner, route, startDateTime, endDatetime, price);
        cruise.setDescription(description);
        cruise.setImageName(imageName);
        if (!Validator.Insert.isCruiseValid(cruise)) {
            File file = new File(parametersMap.get("image_path"));
            if (file.delete())
                logger.debug("{} file deleted", parametersMap.get("image_name"));
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        try {
            cruiseService.saveCruise(cruise);
        } catch (SQLException | EntityNotInsertedException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToSaveData", logger, request);
        }
        return "re:/pages/admin-cruise?action=find-all";
    }

    private String formUpdateCruise(HttpServletRequest request, CruiseService cruiseService, LinerService
            linerService, RouteService routeService) {
        int id;
        try {
            id = Integer.parseInt(request.getParameter("id"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorFailedToFindData", logger, request);
        }
        if (isDataBeingInUse(request, "cruise", id,false))
            return generateErrorPage("errorDataIsBeingModified", logger, request);
        Cruise cruise;
        List<Liner> linerList;
        List<Route> routeList;
        Map<Integer, String> statusDict;
        try {
            cruise = cruiseService.findCruiseById(id);
            linerList = linerService.findAllLiners();
            routeList = routeService.findAllRoutes();
            statusDict = cruiseService.getStatusDict();
        } catch (SQLException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToFindData", logger, request);
        }
        request.setAttribute("cruise", cruise);
        request.setAttribute("linerList", linerList);
        request.setAttribute("routeList", routeList);
        request.setAttribute("statusDict", statusDict);
        return "/cruiseUpdateForm.jsp";
    }

    private String updateCruise(HttpServletRequest request, CruiseService cruiseService, Map<String, String> parametersMap) {
        int cruiseId;
        int linerId;
        int routeId;
        int statusId;
        LocalDateTime startDateTime;
        LocalDateTime endDateTime;
        BigDecimal price;
        try {
            cruiseId = Integer.parseInt(parametersMap.get("id"));
            linerId = Integer.parseInt(parametersMap.get("liner_id"));
            routeId = Integer.parseInt(parametersMap.get("route_id"));
            statusId = Integer.parseInt(parametersMap.get("status_id"));
            startDateTime = LocalDateTime.parse(parametersMap.get("start_datetime"));
            endDateTime = LocalDateTime.parse(parametersMap.get("end_datetime"));
            price = new BigDecimal(parametersMap.get("price"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorFailedToFindData", logger, request);
        }
        if (isDataBeingInUse(request, "cruise", cruiseId, true))
            return generateErrorPage("errorDataIsBeingModified", logger, request);
        String name = parametersMap.get("name");
        Liner liner = new Liner(linerId);
        Route route = new Route(routeId);
        String description = parametersMap.get("description");
        String imageName;
        if (parametersMap.get("image_name") != null && !parametersMap.get("image_name").isBlank())
            imageName = parametersMap.get("image_name");
        else
            imageName = parametersMap.get("old_image_name");
        Cruise cruise = new Cruise(name, liner, route, startDateTime, endDateTime, price);
        cruise.setId(cruiseId);
        cruise.setDescription(description);
        cruise.setImageName(imageName);
        cruise.setStatusId(statusId);
        if (!Validator.Update.isCruiseValid(cruise)) {
            File file = new File(parametersMap.get("image_path"));
            if (file.delete())
                logger.debug("{} file deleted", parametersMap.get("image_name"));
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        try {
            cruiseService.modifyCruise(cruise);
        } catch (SQLException | EntityNotUpdatedException e) {
            return generateErrorPage("errorFailedToUpdateData", logger, request);
        }
        return "re:/pages/admin-cruise?action=find-all";
    }

    private String deleteCruiseById(HttpServletRequest request, CruiseService cruiseService) {
        int id;
        try {
            id = Integer.parseInt(request.getParameter("id"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        if (isDataBeingInUse(request, "cruise", id, true))
            return generateErrorPage("errorDataIsBeingModified", logger, request);
        try {
            cruiseService.deleteCruiseById(id);
        } catch (SQLException | EntityNotDeletedException e) {
            return generateErrorPage("errorFailedToDeleteData", logger, request);
        }
        return "re:/pages/admin-cruise?action=find-all";
    }
}
