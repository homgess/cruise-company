package com.cruisecompany.controller.command;


import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.entity.User;
import com.cruisecompany.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;


public class LogoutCommand implements Command {
    private static final Logger logger = LogManager.getLogger(LogoutCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Received request: [{}]", request.getRequestURI());
        UserService userService = new UserService();
        User user = (User) request.getSession().getAttribute("loggedUser");
        logger.debug("User [{}] logout", user.getLogin());
        try {
            userService.updateLastOnlineDate(user.getId());
        } catch (SQLException | EntityNotUpdatedException e) {
            return generateErrorPage("errorWhileLogout", logger, request);
        }
        request.getSession(false).invalidate();
        return "re:/pages/index?message";
    }

}
