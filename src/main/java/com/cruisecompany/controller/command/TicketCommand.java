package com.cruisecompany.controller.command;


import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.controller.util.Validator;
import com.cruisecompany.entity.Cruise;
import com.cruisecompany.entity.Liner;
import com.cruisecompany.entity.Ticket;
import com.cruisecompany.entity.User;
import com.cruisecompany.service.CruiseService;
import com.cruisecompany.service.LinerService;
import com.cruisecompany.service.TicketService;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TicketCommand implements Command {
    private static final Logger logger = LogManager.getLogger(TicketCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Received request: [{}]", request.getRequestURI());
        String action;
        Map<String, String> parametersMap = new HashMap<>();
        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                parametersMap = processFileRequest(request, "/data/docs/", "D://tmp/", logger);
                action = parametersMap.get("action");
            } catch (Exception e) {
                return generateErrorPage("errorFailedToLoadFile", logger, request);
            }
        } else
            action = request.getParameter("action");
        CruiseService cruiseService = new CruiseService();
        LinerService linerService = new LinerService();
        TicketService ticketService = new TicketService();
        if (action == null) {
            logger.debug("Null action interpeted as find-all-user-tickets");
            return findAllUserTickets(request, ticketService);
        }
        switch (action) {
            case "form-create":
                logger.debug("Action interpreted as form-create-ticket");
                return formCreateTicket(request, cruiseService, linerService, ticketService);
            case "create":
                logger.debug("Action interpreted as create-ticket");
                return createTicket(request, ticketService, cruiseService, parametersMap);
            case "find-my-tickets":
                logger.debug("Action interpreted as find-all-tickets-by-user-id");
                return findAllUserTickets(request, ticketService);
            case "pay":
                logger.debug("Action interpreted as user-pay-for-ticket");
                return userPayForTicket(request, ticketService);
            case "cancel":
                logger.debug("Action interpreted as user cancels the ticket");
                return userCancelTicket(request, ticketService);
            default:
                return generateErrorPage("errorWrongPage", logger, request);
        }
    }

    private String userCancelTicket(HttpServletRequest request, TicketService ticketService) {
        int ticketId;
        try {
            ticketId = Integer.parseInt(request.getParameter("id"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        try {
            ticketService.modifyTicketStatus(ticketId, 5);
        } catch (SQLException | EntityNotUpdatedException e) {
            return generateErrorPage("errorFailedToUpdateData", logger, request);
        }
        return "re:/pages/ticket";
    }

    private String userPayForTicket(HttpServletRequest request, TicketService ticketService) {
        int ticketId;
        try {
            ticketId = Integer.parseInt(request.getParameter("id"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorFailedToUpdateData", logger, request);
        }
        try {
            ticketService.modifyTicketStatus(ticketId, 3);
        } catch (SQLException | EntityNotUpdatedException e) {
            return generateErrorPage("errorFailedToUpdateData", logger, request);
        }
        return "re:/pages/ticket";
    }

    private String findAllUserTickets(HttpServletRequest request, TicketService ticketService) {
        int userId;
        User user = (User) request.getSession(false).getAttribute("loggedUser");
        if (user == null)
            return "/pages/auth";
        try {
            userId = user.getId();
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        List<Ticket> ticketList;
        Map<Integer, String> statusDict;
        try {
            ticketList = ticketService.findAllTicketsByUserId(userId);
            statusDict = ticketService.findStatusDict();
        } catch (SQLException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToSaveData", logger, request);
        }
        request.setAttribute("ticketList", ticketList);
        request.setAttribute("statusDict", statusDict);
        return "/ticketFindAll.jsp";
    }

    private String createTicket(HttpServletRequest request, TicketService ticketService, CruiseService cruiseService, Map<String, String> parametersMap) {
        int cruiseId;
        int userId;
        int position;
        BigDecimal price;
        try {
            cruiseId = Integer.parseInt(parametersMap.get("cruise_id"));
            User user = (User) request.getSession().getAttribute("loggedUser");
            userId = user.getId();
            position = Integer.parseInt(parametersMap.get("position"));
            price = new BigDecimal(parametersMap.get("price"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        String docsImageName = parametersMap.get("image_name");
        Ticket ticket = new Ticket(cruiseId, userId, position, docsImageName, price);
        if (!Validator.Insert.isTicketValid(ticket)) {
            File file = new File(parametersMap.get("image_path"));
            if (file.delete())
                logger.debug("{} file deleted", parametersMap.get("image_name"));
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        List<Ticket> ticketList = new ArrayList<>();
        try {
            ticketList = ticketService.findAllTicketsByCruiseId(cruiseId);
        } catch (EntityNotFoundException | SQLException ignored) {}
        try {
            for (Ticket existingTicket: ticketList) {
                if (existingTicket.getCruiseId() == ticket.getCruiseId() && existingTicket.getPosition() == ticket.getPosition())
                    return generateErrorPage("errorDataIsBeingModified", logger, request);
            }
                ticketService.saveTicket(ticket);
                cruiseService.updateCruiseStatus(cruiseId);
                return "re:/pages/index?message=successTicketCreate";
        } catch (SQLException | EntityNotInsertedException e) {
            return generateErrorPage("errorFailedToSaveData", logger, request);
        }
    }

    private String formCreateTicket(HttpServletRequest request, CruiseService cruiseService, LinerService linerService, TicketService ticketService) {
        int cruiseId;
        Liner liner;
        User user;
        try {
            cruiseId = Integer.parseInt(request.getParameter("cruise_id"));
            user = (User) request.getSession().getAttribute("loggedUser");
        } catch (NumberFormatException | ClassCastException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        Cruise cruise;
        List<Integer> occupiedPositionsList;
        try {
            cruise = cruiseService.findCruiseById(cruiseId);
            liner = linerService.findById(cruise.getLiner().getId());
            List<Ticket> ticketList = new ArrayList<>();
            try {
                ticketList = ticketService.findAllTicketsByCruiseId(cruiseId);
            } catch (EntityNotFoundException ignored) {}
            occupiedPositionsList = new ArrayList<>();
            for (Ticket ticket : ticketList) {
                if (ticket.getStatusId() <= 4)
                    occupiedPositionsList.add(ticket.getPosition());
            }
        } catch (SQLException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToFindData", logger, request);
        }
        request.setAttribute("cruise", cruise);
        request.setAttribute("liner", liner);
        request.setAttribute("occupiedPositionsList", occupiedPositionsList);
        request.setAttribute("user", user);
        return "/ticketCreateForm.jsp";
    }
}
