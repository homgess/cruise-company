package com.cruisecompany.controller.command;


import com.cruisecompany.controller.util.*;
import com.cruisecompany.entity.Liner;
import com.cruisecompany.service.LinerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.List;


public class AdminLinerCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminLinerCommand.class);
    private final LinerService linerService;

    public AdminLinerCommand() {
        linerService = new LinerService();
    }

    public AdminLinerCommand(LinerService linerService) {
        this.linerService = linerService;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Received request: [{}]", request.getRequestURI());
        String action = request.getParameter("action");
        if (action == null) {
            logger.debug("Null action interpreted as find-all-liners");
            return findAddLiners(request, linerService);
        }
        switch (action) {
            case "find-all": {
                logger.debug("Action interpreted as find-all-liners");
                return findAddLiners(request, linerService);
            }
            case "form-insert": {
                logger.debug("Action interpreted as form-insert-liner");
                return "/linerInsertForm.jsp";
            }
            case "insert": {
                logger.debug("Action interpreted as insert-liner");
                return insertLiner(request, linerService);
            }
            case "form-update": {
                logger.debug("Action interpreted as form-update-liner");
                return formUpdateLiner(request, linerService);
            }
            case "update": {
                logger.debug("Action interpreted as update-liner");
                return updateLiner(request, linerService);
            }
            case "delete-by-id": {
                logger.debug("Action interpreted as delete-liner-by-id");
                return deleteLinerById(request, linerService);
            }
            default:
                return generateErrorPage("errorWrongPage", logger, request);
        }
    }

    private String findAddLiners(HttpServletRequest request, LinerService linerService) {
        List<Liner> linerList;
        try {
            linerList = linerService.findAllLiners();
        } catch (SQLException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToFindData", logger, request);
        }
        request.setAttribute("linerList", linerList);
        return "/linerReadAllAdmin.jsp";
    }

    private String insertLiner(HttpServletRequest request, LinerService linerService) {
        int passengerCapacity;
        try {
            passengerCapacity = Integer.parseInt(request.getParameter("passenger_capacity"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        String name = request.getParameter("name");
        Liner liner = new Liner(1,name ,passengerCapacity);
        if (!Validator.Insert.isLinerValid(liner))
            return generateErrorPage("errorWrongInputData", logger, request);
        try {
            linerService.saveLiner(liner);
        } catch (SQLException | EntityNotInsertedException e) {
            return generateErrorPage("errorFailedToSaveData", logger, request);
        }
        return "re:/pages/admin-liner?action=find-all";
    }

    private String formUpdateLiner(HttpServletRequest request, LinerService linerService) {
        int id;
        try {
            id = Integer.parseInt(request.getParameter("id"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        if (isDataBeingInUse(request, "liner", id, false))
            return generateErrorPage("errorDataIsBeingModified", logger, request);
        Liner liner;
        try {
            liner = linerService.findById(id);
        } catch (SQLException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToFindData", logger, request);
        }
        request.setAttribute("liner", liner);
        return "/linerUpdateForm.jsp";
    }

    private String updateLiner(HttpServletRequest request, LinerService linerService) {
        int id;
        int passengerCapacity;
        try {
            id = Integer.parseInt(request.getParameter("id"));
            passengerCapacity = Integer.parseInt(request.getParameter("passenger_capacity"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        if (isDataBeingInUse(request, "liner", id, true))
            return generateErrorPage("errorDataIsBeingModified", logger, request);
        String name = request.getParameter("name");
        Liner liner = new Liner(id, name, passengerCapacity);
        if (!Validator.Update.isLinerValid(liner))
            return generateErrorPage("errorWrongInputData", logger, request);
        try {
            linerService.modifyLiner(liner);
        } catch (SQLException | EntityNotUpdatedException e) {
            return generateErrorPage("errorFailedToUpdateData", logger, request);
        }
        return "re:/pages/admin-liner?action=find-all";
    }

    private String deleteLinerById(HttpServletRequest request, LinerService linerService) {
        int id;
        try {
            id = Integer.parseInt(request.getParameter("id"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        if (isDataBeingInUse(request, "liner", id, true))
            return generateErrorPage("errorDataIsBeingModified", logger, request);
        try {
            linerService.deleteLinerById(id);
        } catch (SQLException | EntityNotDeletedException e) {
            return generateErrorPage("errorFailedToDeleteData", logger, request);
        }
        return "re:/pages/admin-liner?action=find-all";
    }
}
