package com.cruisecompany.controller.command;


import com.cruisecompany.controller.ControllerServlet;
import com.cruisecompany.controller.util.Modifier;
import com.cruisecompany.controller.util.ModifierList;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;


public interface Command {
    String execute(HttpServletRequest request, HttpServletResponse response);

    default String generateErrorPage(String error, Logger logger, HttpServletRequest request) {
        logger.error("Error {}", error);
        HttpSession session = request.getSession(false);
        if (session != null)
            session.setAttribute("error", error);
        return "re:/pages/error";
    }

    default boolean isDataBeingInUse(HttpServletRequest request, String entityName, int entityId, boolean isAction) throws IllegalStateException{
        ModifierList modifierList = new ModifierList(ControllerServlet.getConfig().getServletContext().getAttribute("modifierList"));
        Modifier modifier = new Modifier(request.getSession().getId(), entityName, entityId);
        if (isAction) {
            if (modifierList.isContainsAnotherSession(modifier))
                return true;
            else if (modifierList.isContainsThisSession(modifier)) {
                modifierList.remove(modifier);
                ControllerServlet.getConfig().getServletContext().setAttribute("modifierList", modifierList);
                return false;
            }
        } else {
            if (!modifierList.isContainsAnotherSession(modifier)) {
                modifierList.add(modifier);
                ControllerServlet.getConfig().getServletContext().setAttribute("modifierList", modifierList);
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    default Map<String, String> processFileRequest(HttpServletRequest request, String relativePath, String tmpPath, Logger logger) throws Exception {
        final String FILE_PATH = ControllerServlet.getConfig().getServletContext().getRealPath(relativePath);
        final String TMP_FILE_PATH = tmpPath;
        File file;
        String fileName;
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setRepository(new File(TMP_FILE_PATH));
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setHeaderEncoding("UTF-8");
        List<FileItem> fileItems = upload.parseRequest(request);
        Map<String, String> parametersMap = new HashMap<>();
        for (FileItem fileItem : fileItems) {
            if (!fileItem.isFormField() && !fileItem.getName().isBlank()) {
                fileName = fileItem.getName();
                logger.debug("fileName: {}", fileName);
                String contentType = fileItem.getContentType();
                logger.debug("contentType: {}", contentType);
                boolean isInMemory = fileItem.isInMemory();
                logger.debug("isInMemory: {}", isInMemory);
                long sizeInBytes = fileItem.getSize();
                logger.debug("size: {}", sizeInBytes);
                String ext = getFileExt(fileName);
                do {
                    fileName = UUID.randomUUID() + ext;
                } while (Files.exists(Path.of(FILE_PATH + fileName)));
                file = new File(FILE_PATH + fileName);
                fileItem.write(file);
                parametersMap.put("image_name", fileName);
                parametersMap.put("image_path", FILE_PATH + fileName);
                logger.debug("Uploaded file [{}]. Size: {} bytes ", fileName, sizeInBytes);
            } else
                parametersMap.put(fileItem.getFieldName(), fileItem.getString("UTF-8"));
        }
        return parametersMap;
    }

    private String getFileExt(String path) {
        if (path != null && path.lastIndexOf(".") != -1)
            return path.substring(path.lastIndexOf("."));
        else
            return null;
    }
}
