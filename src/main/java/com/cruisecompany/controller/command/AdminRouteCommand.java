package com.cruisecompany.controller.command;


import com.cruisecompany.controller.util.*;
import com.cruisecompany.entity.Route;
import com.cruisecompany.service.RouteService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.List;


public class AdminRouteCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminRouteCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Received request: [{}]", request.getRequestURI());
        String action = request.getParameter("action");
        RouteService routeService = new RouteService();
        if (action == null) {
            logger.debug("Null action interpreted as find-all-routes");
            return findAllRoutes(request, routeService);
        }
        switch (action) {
            case "find-all": {
                logger.debug("Action interpreted as find-all-routes");
                return findAllRoutes(request, routeService);
            }
            case "form-insert": {
                logger.debug("Action interpreted as form-insert-cruise");
                return "/routeCreate.jsp";
            }
            case "insert": {
                logger.debug("Action interpreted as insert-cruise");
                return insertRoute(request, routeService);
            }
            case "form-update": {
                logger.debug("Action interpreted as form-update-route");
                return formUpdateRoute(request, routeService);
            }
            case "update": {
                logger.debug("Action interpreted as update-route");
                return updateRoute(request, routeService);
            }
            case "delete-by-id": {
                logger.debug("Action interpreted as delete-route-by-id");
                return deleteRouteById(request, routeService);
            }
            default:
                return generateErrorPage("errorWrongPage", logger, request);
        }
    }

    private String findAllRoutes(HttpServletRequest request, RouteService routeService) {
        List<Route> routeList;
        try {
            routeList = routeService.findAllRoutes();
        } catch (SQLException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToFindData", logger, request);
        }
        request.setAttribute("routeList", routeList);
        return "/routeReadAllAdmin.jsp";
    }

    private String insertRoute(HttpServletRequest request, RouteService routeService) {
        String name = request.getParameter("name");
        Route route = new Route(name);
        if (!Validator.Insert.isRouteValid(route))
            return generateErrorPage("errorWrongInputData", logger, request);
        try {
            routeService.saveRoute(route);
        } catch (SQLException | EntityNotInsertedException e) {
            return generateErrorPage("errorFailedToSaveData", logger, request);
        }
        return "re:/pages/admin-route?action=find-all";
    }

    private String formUpdateRoute(HttpServletRequest request, RouteService routeService) {
        int id;
        try {
            id = Integer.parseInt(request.getParameter("route_id"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        if (isDataBeingInUse(request, "route", id, false))
            return generateErrorPage("errorDataIsBeingModified", logger, request);
        Route route;
        try {
            route = routeService.findRouteById(id);
        } catch (SQLException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToFindData", logger, request);
        }
        request.setAttribute("route", route);
        return "/routeUpdateForm.jsp";
    }


    private String updateRoute(HttpServletRequest request, RouteService routeService) {
        int id;
        Route route;
        try {
            id = Integer.parseInt(request.getParameter("route_id"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        if (isDataBeingInUse(request, "route", id, true))
            return generateErrorPage("errorDataIsBeingModified", logger, request);
        String name = request.getParameter("name");
        route = new Route(id, name);
        if (!Validator.Update.isRouteValid(route))
            return generateErrorPage("errorWrongInputData", logger, request);
        try {
            routeService.modifyRoute(route);
        } catch (SQLException | EntityNotUpdatedException e) {
            return generateErrorPage("errorFailedToUpdateData", logger, request);
        }
        return "re:/pages/admin-route?action=find-all";
    }

    private String deleteRouteById(HttpServletRequest request, RouteService routeService) {
        int id;
        try {
            id = Integer.parseInt(request.getParameter("route_id"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        if (isDataBeingInUse(request, "route", id, true))
            return generateErrorPage("errorDataIsBeingModified", logger, request);
        try {
            routeService.deleteRouteById(id);
        } catch (SQLException | EntityNotDeletedException e) {
            return generateErrorPage("errorFailedToDeleteData", logger, request);
        }
        return "re:/pages/admin-route?action=find-all";
    }
}
