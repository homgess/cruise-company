package com.cruisecompany.controller.command;


import com.cruisecompany.controller.ControllerServlet;
import com.cruisecompany.controller.util.ModifierList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class AdminPanelCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminPanelCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Received request: [{}]", request.getRequestURI());
        ModifierList modifierList = new ModifierList(ControllerServlet.getConfig().getServletContext().getAttribute("modifierList"));
        modifierList.removeIf(modifier -> !modifier.getSessionId().equals(request.getSession().getId()));
        request.setAttribute("modifierList", modifierList);
        return "/adminPanel.jsp";
    }
}
