package com.cruisecompany.controller.command;


import com.cruisecompany.controller.util.*;
import com.cruisecompany.entity.User;
import com.cruisecompany.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;


public class AuthCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AuthCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Received request: [{}]", request.getRequestURI());
        String action = request.getParameter("action");
        UserService userService = new UserService();
        if (action == null) {
            logger.debug("Null action interpreted as form-login");
            return "/userLoginForm.jsp";
        }
        switch (action) {
            case "form-signUp": {
                logger.debug("Action interpreted as form-insert-user");
                return "/userCreateForm.jsp";
            }
            case "signUp": {
                logger.debug("Action interpreted as insert-user");
                return insertUser(request, userService);
            }
            case "form-signIn": {
                logger.debug("Action interpreted as form-login");
                return "/userLoginForm.jsp";
            }
            case "signIn": {
                logger.debug("Action interpreted as login-user");
                return loginUser(request, userService);
            }
            default:
                return generateErrorPage("errorWrongPage", logger, request);
        }
    }

    private String loginUser(HttpServletRequest request, UserService userService) {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        User user = new User(login, password);
        if (!Validator.Insert.isUserValid(user))
            return generateErrorPage("errorWrongInputData", logger, request);
        User foundUser;
        try {
            foundUser = userService.findUserByLogin(user.getLogin());
        } catch (SQLException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToFindData", logger, request);
        }
        try {
            user.setPassword(EncryptHelper.encrypt(user.getPassword()));
        } catch (NoSuchAlgorithmException e) {
            return generateErrorPage("errorWhileLogin", logger, request);
        }
        if (user.getPassword().equals(foundUser.getPassword())) {
            if (foundUser.getRoleId() == 1) {
                logger.debug("Blocked user [{}] tried to login. Denied", user.getLogin());
                return generateErrorPage("errorUserBlocked", logger, request);
            } else {
                try {
                    userService.updateLastOnlineDate(foundUser.getId());
                } catch (SQLException | EntityNotUpdatedException e) {
                    return generateErrorPage("errorWhileLogin", logger, request);
                }
                logger.debug("User [{}] successfully login", user.getLogin());
                HttpSession session = request.getSession();
                session.setAttribute("loggedUser", foundUser);
                return "re:/pages/index";
            }
        }
        logger.debug("User [{}] failed to login", request.getParameter("login"));
        return generateErrorPage("errorWhileLogin", logger, request);
    }

    private String insertUser(HttpServletRequest request, UserService userService) {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        User user = new User(login, password);
        if (!Validator.Insert.isUserValid(user))
            return generateErrorPage("errorWrongInputData", logger, request);
        try {
            userService.findUserByLogin(login);
            return generateErrorPage("errorAlreadyExists", logger, request);
        } catch (EntityNotFoundException | SQLException ignored) {
        }
        try {
            user.setPassword(EncryptHelper.encrypt(user.getPassword()));
        } catch (NoSuchAlgorithmException e) {
            return generateErrorPage("errorWhileLogin", logger, request);
        }
        try {
            userService.saveUser(user);
        } catch (SQLException | EntityNotInsertedException e) {
            return generateErrorPage("errorFailedToSaveData", logger, request);
        }
        return "re:/pages/index";
    }
}
