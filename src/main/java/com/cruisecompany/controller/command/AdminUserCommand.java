package com.cruisecompany.controller.command;


import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.entity.User;
import com.cruisecompany.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;


public class AdminUserCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminUserCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Received request: [{}]", request.getRequestURI());
        String action = request.getParameter("action");
        UserService userService = new UserService();
        if (action == null) {
            logger.debug("Null action interpreted as find-all-users");
            return findAllUsers(request, userService);
        }
        switch (action) {
            case "find-all": {
                logger.debug("Action interpreted as find-all-users");
                return findAllUsers(request, userService);
            }
            case "form-insert": {
                logger.debug("Action interpreted as form-create-user");
                return "/userCreateForm.jsp";
            }
            case "form-update-role": {
                logger.debug("Action interpreted as form-update-user");
                return formUpdateUserRole(request, userService);
            }
            case "update-role": {
                logger.debug("Action interpreted as update-user");
                return updateUserRole(request, userService);
            }
            default:
                return generateErrorPage("errorWrongPage", logger, request);
        }
    }

    private String findAllUsers(HttpServletRequest request, UserService userService) {
        List<User> userList;
        Map<Integer, String> roleDict;
        try {
            userList = userService.findAllUsers();
            roleDict = userService.findRoleDict();
        } catch (SQLException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToFindData", logger, request);
        }
        request.setAttribute("userList", userList);
        request.setAttribute("roleDict", roleDict);
        return "/userReadAllAdmin.jsp";
    }

    private String formUpdateUserRole(HttpServletRequest request, UserService userService) {
        int id;
        User user;
        Map<Integer, String> roleDict;
        try {
            id = Integer.parseInt(request.getParameter("id"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        if (isDataBeingInUse(request, "user", id, false))
            return generateErrorPage("errorDataIsBeingModified", logger, request);
        try {
            user = userService.findUserById(id);
            roleDict = userService.findRoleDict();
        } catch (SQLException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToFindData", logger, request);
        }
        request.setAttribute("user", user);
        request.setAttribute("roleDict", roleDict);
        return "/userUpdateForm.jsp";
    }

    private String updateUserRole(HttpServletRequest request, UserService userService) {
        int userId;
        int roleId;
        User user;
        try {
            userId = Integer.parseInt(request.getParameter("id"));
            roleId = Integer.parseInt(request.getParameter("roleId"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        if (isDataBeingInUse(request, "user", userId, true))
            return generateErrorPage("errorDataIsBeingModified", logger, request);
        try {
            user = userService.findUserById(userId);
            user.setRoleId(roleId);
            userService.modifyUser(user);
        } catch (SQLException | EntityNotFoundException | EntityNotUpdatedException e) {
            return generateErrorPage("errorFailedToUpdateData", logger, request);
        }
        return "re:/pages/admin-user?action=find-all";
    }
}
