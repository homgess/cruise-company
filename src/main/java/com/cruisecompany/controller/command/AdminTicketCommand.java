package com.cruisecompany.controller.command;


import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.entity.Ticket;
import com.cruisecompany.service.CruiseService;
import com.cruisecompany.service.TicketService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;


public class AdminTicketCommand implements Command {
    private static final Logger logger = LogManager.getLogger(AdminTicketCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Received request: [{}]", request.getRequestURI());
        String action = request.getParameter("action");
        TicketService ticketService = new TicketService();
        CruiseService cruiseService = new CruiseService();
        if (action == null) {
            logger.debug("Null action interpreted as find-all-tickets");
            return findAllTickets(request, ticketService);
        }
        switch (action) {
            case "find-all": {
                logger.debug("Action interpreted as find-all-tickets");
                return findAllTickets(request, ticketService);
            }
            case "form-update": {
                logger.debug("Action interpreted as form-update-ticket-status");
                return formUpdateTicketStatus(request, ticketService);
            }
            case "update": {
                logger.debug("Action interpreted as update-ticket-status");
                return updateTicketStatus(request, ticketService, cruiseService);
            }
            default:
                return generateErrorPage("errorWrongPage", logger, request);
        }
    }

    private String findAllTickets(HttpServletRequest request, TicketService ticketService) {
        List<Ticket> ticketList;
        Map<Integer, String> statusDict;
        try {
            ticketList = ticketService.findAllTickets();
            statusDict = ticketService.findStatusDict();
        } catch (SQLException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToFindData", logger, request);
        }
        request.setAttribute("ticketList", ticketList);
        request.setAttribute("statusDict", statusDict);
        return "/ticketReadAllAdmin.jsp";
    }

    private String formUpdateTicketStatus(HttpServletRequest request, TicketService ticketService) {
        int ticketId;
        Ticket ticket;
        Map<Integer, String> statusDict;
        try {
            ticketId = Integer.parseInt(request.getParameter("id"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        if (isDataBeingInUse(request, "ticket", ticketId, false))
            return generateErrorPage("errorDataIsBeingModified", logger, request);
        try {
            ticket = ticketService.findTicketById(ticketId);
            statusDict = ticketService.findStatusDict();
        } catch (SQLException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToFindData", logger, request);
        }
        request.setAttribute("ticket", ticket);
        request.setAttribute("statusDict", statusDict);
        return "/ticketUpdateForm.jsp";
    }

    private String updateTicketStatus(HttpServletRequest request, TicketService ticketService, CruiseService cruiseService) {
        int ticketId;
        int statusId;
        try {
            ticketId = Integer.parseInt(request.getParameter("id"));
            statusId = Integer.parseInt(request.getParameter("status_id"));
        } catch (NumberFormatException e) {
            return generateErrorPage("errorWrongInputData", logger, request);
        }
        if (isDataBeingInUse(request, "ticket", ticketId, true))
            return generateErrorPage("errorDataIsBeingModified", logger, request);
        try {
            ticketService.modifyTicketStatus(ticketId, statusId);
            int id = ticketService.findTicketById(ticketId).getCruiseId();
            cruiseService.updateCruiseStatus(id);
        } catch (SQLException | EntityNotUpdatedException | EntityNotFoundException e) {
            return generateErrorPage("errorFailedToUpdateData", logger, request);
        }
        return "re:/pages/admin-ticket?action=find-all";
    }
}
