package com.cruisecompany.controller.command;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class ErrorCommand implements Command {
    private static final Logger logger = LogManager.getLogger(ErrorCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Received request: [{}]", request.getRequestURI());
        HttpSession session = request.getSession(false);
        try {
            String error = (String) session.getAttribute("error");
            session.removeAttribute("error");
            request.setAttribute("error", error);
        } catch (NullPointerException e) {
            logger.warn("Undefined error", e);
        }
        return "/error.jsp";
    }
}
