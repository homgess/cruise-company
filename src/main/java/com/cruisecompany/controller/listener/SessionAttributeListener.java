package com.cruisecompany.controller.listener;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.*;
import javax.servlet.annotation.*;


@WebListener
public class SessionAttributeListener implements HttpSessionAttributeListener {
    private static final Logger logger = LogManager.getLogger(SessionAttributeListener.class);

    @Override
    public void attributeAdded(HttpSessionBindingEvent sbe) {
        logger.debug("[SESSION {}] Added attribute. Name: {}, Value: {}", sbe.getSession().getId(), sbe.getName(), sbe.getValue());
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent sbe) {
        logger.debug("[SESSION {}] Removed attribute. Name: {}, Value: {}", sbe.getSession().getId(), sbe.getName(), sbe.getValue());
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent sbe) {
        logger.debug("[SESSION {}] Replaced attribute. Name: {}, Value: {}", sbe.getSession().getId(), sbe.getName(), sbe.getValue());
    }
}
