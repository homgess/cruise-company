package com.cruisecompany.controller.listener;


import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.controller.util.ModifierList;
import com.cruisecompany.service.CruiseService;
import com.cruisecompany.service.TicketService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.sql.SQLException;


@WebListener
public class SessionListener implements HttpSessionListener {
    private static final Logger logger = LogManager.getLogger(SessionListener.class);

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        logger.debug("Session created. ID = [{}]", se.getSession().getId());
        se.getSession().setAttribute("lang", "uk");
        TicketService ticketService = new TicketService();
        CruiseService cruiseService = new CruiseService();
        try {
            cruiseService.updateAllCruisesStatus();
            ticketService.modifyAllTicketStatuses();
        } catch (SQLException | EntityNotUpdatedException e) {
            logger.error(e);
        }
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        ModifierList modifierList = new ModifierList(se.getSession().getServletContext().getAttribute("modifierList"));
        modifierList.removeIf(modifier -> modifier.getSessionId().equals(se.getSession().getId()));
        logger.debug("Session destroyed. ID = [{}]", se.getSession().getId());
    }

}
