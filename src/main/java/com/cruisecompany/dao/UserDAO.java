package com.cruisecompany.dao;


import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.entity.User;

import java.sql.SQLException;
import java.util.Map;


public interface UserDAO extends GenericDAO<User> {
    User findByLogin(String login) throws SQLException, EntityNotFoundException;

    Map<Integer, String> getRoleDict() throws SQLException, EntityNotFoundException;

    void updateLastOnlineDateById(int id) throws SQLException, EntityNotUpdatedException;
}
