package com.cruisecompany.dao;

import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.entity.RoutePoint;

import java.sql.SQLException;
import java.util.List;

public interface RoutePointDAO extends GenericDAO<RoutePoint> {
    List<RoutePoint> findByRouteId(int id) throws SQLException, EntityNotFoundException;
}
