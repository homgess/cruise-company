package com.cruisecompany.dao;

import com.cruisecompany.entity.Route;

public interface RouteDAO extends GenericDAO<Route> {
}
