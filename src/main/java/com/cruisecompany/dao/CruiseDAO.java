package com.cruisecompany.dao;


import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.entity.Cruise;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;


public interface CruiseDAO extends GenericDAO<Cruise> {
    Cruise findByName(String name) throws SQLException, EntityNotFoundException;

    List<Cruise> findByParameters(String dateTimeType, LocalDate startDateTime, String durationType, int duration, int pageId, int countPerPage) throws SQLException, EntityNotFoundException;

    void insertCruiseInfo(Cruise cruise) throws SQLException, EntityNotInsertedException;

    void updateCruiseInfo(Cruise cruise) throws SQLException, EntityNotUpdatedException;

    int getCruiseCount() throws SQLException;

    int getCruiseCount(String dateTimeType, LocalDate startDateTime, String durationType, int duration) throws SQLException;

    Map<Integer, String> getStatusDict() throws SQLException, EntityNotFoundException;

    void updateCruiseStatusIfFull(int id) throws SQLException;

    void updateCruiseStatusIfNotFull(int id) throws SQLException;

    void updateAllEndedCruiseStatuses() throws SQLException;

    void updateAllStartedCruiseStatuses() throws SQLException;
}
