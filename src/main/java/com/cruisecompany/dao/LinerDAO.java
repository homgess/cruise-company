package com.cruisecompany.dao;

import com.cruisecompany.entity.Liner;

public interface LinerDAO extends GenericDAO<Liner> {
}
