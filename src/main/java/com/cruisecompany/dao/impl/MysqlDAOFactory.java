package com.cruisecompany.dao.impl;


import com.cruisecompany.dao.*;


public class MysqlDAOFactory extends DAOFactory {
    final ConnectionPoolHandler connPool = ConnectionPoolHandler.getInstance();

    @Override
    public UserDAO getUserDAO() {
        return new MysqlUserDAO(connPool.getConnection());
    }

    @Override
    public CruiseDAO getCruiseDAO() {
        return new MysqlCruiseDAO(connPool.getConnection());
    }

    @Override
    public LinerDAO getLinerDAO() {
        return new MysqlLinerDAO(connPool.getConnection());
    }

    @Override
    public RouteDAO getRouteDAO() {
        return new MysqlRouteDAO(connPool.getConnection());
    }

    @Override
    public RoutePointDAO getRoutePointDAO() {
        return new MysqlRoutePointDAO(connPool.getConnection());
    }

    @Override
    public TicketDAO getTicketDAO() {
        return new MysqlTicketDAO(connPool.getConnection());
    }
}
