package com.cruisecompany.dao.impl;


import com.cruisecompany.controller.util.EntityNotDeletedException;
import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.dao.LinerDAO;
import com.cruisecompany.entity.Liner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class MysqlLinerDAO extends MysqlBaseDAO<Liner> implements LinerDAO {
    private static final Logger logger = LogManager.getLogger(MysqlLinerDAO.class);
    private final Connection conn;

    public static final String CREATE_LINER = "INSERT INTO liner VALUES (DEFAULT, ?, ?)";
    public static final String FIND_LINER_BY_ID = "SELECT * FROM liner WHERE liner_id = ?";
    public static final String FIND_ALL_LINERS = "SELECT * FROM liner";
    public static final String UPDATE_LINER = "UPDATE liner SET liner_name = ?, passenger_capacity = ? WHERE liner_id = ?";
    public static final String DELETE_LINER_BY_ID = "DELETE FROM liner WHERE liner_id = ?";

    public MysqlLinerDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void insert(Liner liner) throws SQLException, EntityNotInsertedException {
        logger.debug("Generating an insert liner query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(CREATE_LINER);
            pStmt.setString(1, liner.getName());
            pStmt.setInt(2, liner.getPassengerCapacity());
            int executeCount = pStmt.executeUpdate();
            if (executeCount < 1) throw new EntityNotInsertedException("Insert query was not affected database");
            logger.debug("Inserted liner [{}]", liner);
        } catch (SQLException e) {
            throw new SQLException("Error while inserting new liner", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    public void update(Liner liner) throws SQLException, EntityNotUpdatedException {
        logger.debug("Generating an update liner query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(UPDATE_LINER);
            pStmt.setString(1, liner.getName());
            pStmt.setInt(2, liner.getPassengerCapacity());
            pStmt.setInt(3, liner.getId());
            int executeCount = pStmt.executeUpdate();
            if (executeCount < 1) throw new EntityNotUpdatedException("Update query was not affected database");
            logger.debug("Updated liner [{}]", liner);
        } catch (SQLException e) {
            throw new SQLException("Error while updating existing liner", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    public Liner findById(int id) throws SQLException, EntityNotFoundException {
        logger.debug("Generating a find liner by id query");
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        Liner liner;
        try {
            pStmt = conn.prepareStatement(FIND_LINER_BY_ID);
            pStmt.setInt(1, id);
            rs = pStmt.executeQuery();
            if (isResultSetEmpty(rs)) throw new EntityNotFoundException("None liner was found");
            liner = extractEntities(rs).get(0);
            logger.debug("Found liner [{}]", liner);
        } catch (SQLException e) {
            throw new SQLException("Error while searching liner by id", e);
        } finally {
            close(rs);
            close(pStmt);
        }
        return liner;
    }

    @Override
    public List<Liner> findAll() throws SQLException, EntityNotFoundException {
        logger.debug("Generating a find all liners query");
        Statement stmt = null;
        ResultSet rs = null;
        List<Liner> linerList;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(FIND_ALL_LINERS);
            if (isResultSetEmpty(rs)) throw new EntityNotFoundException("None liner was found");
            linerList = extractEntities(rs);
            logger.debug("Found [{}] liners", linerList.size());
        } catch (SQLException e) {
            throw new SQLException("Error while searching all liners", e);
        } finally {
            close(rs);
            close(stmt);
        }
        return linerList;
    }

    @Override
    public void deleteById(int id) throws SQLException, EntityNotDeletedException {
        logger.debug("Generating a delete liner by id query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(DELETE_LINER_BY_ID);
            pStmt.setInt(1, id);
            int executeCount = pStmt.executeUpdate();
            if (executeCount < 1) throw new EntityNotDeletedException("None liner was deleted");
            logger.debug("Deleted [{}] liner(s)", executeCount);
        } catch (SQLException e) {
            throw new SQLException("Error while deleting liner by id", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    List<Liner> extractEntities(ResultSet rs) throws SQLException {
        logger.debug("Extracting liners from resultSet");
        List<Liner> linerList = new ArrayList<>();
        try {
            while (rs.next()) {
                Liner liner = new Liner();
                liner.setId(rs.getInt("liner_id"));
                liner.setName(rs.getString("liner_name"));
                liner.setPassengerCapacity(rs.getInt("passenger_capacity"));
                linerList.add(liner);
            }
        } catch (SQLException e) {
            throw new SQLException("Error while extracting liners from resultSet", e);
        }
        return linerList;
    }
}
