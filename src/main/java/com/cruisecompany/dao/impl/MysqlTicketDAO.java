package com.cruisecompany.dao.impl;


import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.dao.TicketDAO;
import com.cruisecompany.entity.Ticket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MysqlTicketDAO extends MysqlBaseDAO<Ticket> implements TicketDAO {
    private static final Logger logger = LogManager.getLogger(MysqlTicketDAO.class);
    private final Connection conn;

    public static final String CREATE_TICKET = "INSERT INTO ticket VALUES (DEFAULT, ?, ?, ?, ?, ?, DEFAULT)";
    public static final String READ_ALL_TICKETS = "SELECT * from ticket";
    public static final String READ_TICKET_BY_ID = "SELECT * from ticket WHERE ticket_id = ?";
    public static final String READ_ALL_TICKETS_BY_CRUISE_ID = "SELECT * from ticket WHERE cruise_id = ?";
    public static final String READ_ALL_TICKETS_BY_USER_ID = "SELECT ticket.ticket_id, ticket.user_id, ticket.position, ticket.docs_image_name, ticket.cruise_id, cruise.cruise_name, cruise.start_datetime, cruise.end_datetime, ticket.price, ticket.status_id from ticket INNER JOIN cruise on ticket.cruise_id = cruise.cruise_id WHERE user_id = ?";
    public static final String UPDATE_TICKET = "UPDATE ticket SET cruise_id = ?, user_id = ?, position = ?, docs_image_name = ?, price = ?, status_id = ? WHERE ticket_id = ?";
    public static final String UPDATE_TICKET_STATUS = "UPDATE ticket SET status_id = ? WHERE ticket_id = ?";
    public static final String READ_STATUS_DICT = "SELECT * from ticket_status_dict";
    public static final String UPDATE_ALL_TICKET_STATUS = "UPDATE ticket JOIN cruise on ticket.cruise_id = cruise.cruise_id SET status_id = 4 WHERE status_id != 4 AND DATEDIFF(end_datetime, now()) < 0";

    public MysqlTicketDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void insert(Ticket ticket) throws SQLException, EntityNotInsertedException {
        logger.debug("Generating an insert ticket query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(CREATE_TICKET);
            pStmt.setInt(1, ticket.getCruiseId());
            pStmt.setInt(2, ticket.getUserId());
            pStmt.setInt(3, ticket.getPosition());
            pStmt.setString(4, ticket.getDocsImageName());
            pStmt.setBigDecimal(5, ticket.getPrice());
            int executeCount = pStmt.executeUpdate();
            if (executeCount < 1) throw new EntityNotInsertedException("Update query was not affected database");
            logger.debug("Updated ticket [{}]", ticket);
        } catch (SQLException e) {
            throw new SQLException("Error while updating existing ticket", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    public void update(Ticket ticket) throws SQLException, EntityNotUpdatedException {
        logger.debug("Generating an update ticket query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(UPDATE_TICKET);
            pStmt.setInt(1, ticket.getCruiseId());
            pStmt.setInt(2, ticket.getUserId());
            pStmt.setInt(3, ticket.getPosition());
            pStmt.setString(4, ticket.getDocsImageName());
            pStmt.setBigDecimal(5, ticket.getPrice());
            pStmt.setInt(6, ticket.getStatusId());
            pStmt.setInt(7, ticket.getId());
            int executeCount = pStmt.executeUpdate();
            if (executeCount < 1) throw new EntityNotUpdatedException("Update query was not affected database");
            logger.debug("Updated ticket [{}]", ticket);
        } catch (SQLException e) {
            throw new SQLException("Error while updating existing ticket", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    public Ticket findById(int id) throws SQLException, EntityNotFoundException {
        logger.debug("Generating a find ticket by id query");
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        Ticket ticket;
        try {
            pStmt = conn.prepareStatement(READ_TICKET_BY_ID);
            pStmt.setInt(1, id);
            rs = pStmt.executeQuery();
            if (isResultSetEmpty(rs)) throw new EntityNotFoundException("None ticket was found");
            ticket = extractEntities(rs).get(0);
            logger.debug("Found ticket [{}]", ticket);
        } catch (SQLException e) {
            throw new SQLException("Error while searching ticket by id", e);
        } finally {
            close(rs);
            close(pStmt);
        }
        return ticket;
    }

    @Override
    public List<Ticket> findAll() throws SQLException, EntityNotFoundException {
        logger.debug("Generating a find all tickets query");
        Statement stmt = null;
        ResultSet rs = null;
        List<Ticket> ticketList;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(READ_ALL_TICKETS);
            if (isResultSetEmpty(rs)) throw new EntityNotFoundException("None ticket was found");
            ticketList = extractEntities(rs);
            logger.debug("Found tickets [{}]", ticketList);
        } catch (SQLException e) {
            throw new SQLException("Error while searching all tickets", e);
        } finally {
            close(rs);
            close(stmt);
        }
        return ticketList;
    }

    @Override
    public void deleteById(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void updateTicketStatus(int ticketId, int statusId) throws SQLException, EntityNotUpdatedException {
        logger.debug("Executing update ticket status query [{}]. Parameters: [{},{}]", UPDATE_TICKET_STATUS, ticketId, statusId);
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(UPDATE_TICKET_STATUS);
            pStmt.setInt(1, statusId);
            pStmt.setInt(2, ticketId);
            int executeCount = pStmt.executeUpdate();
            if (executeCount < 1)
                throw new EntityNotUpdatedException("Update query was not affected database");
            logger.debug("Updated ticket [{}]", ticketId);
        } catch (SQLException e) {
            throw new SQLException("Error while updating existing ticket", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    public List<Ticket> findTicketByUserId(int id) throws SQLException, EntityNotFoundException {
        logger.debug("Generating a find all tickets by user id query");
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        List<Ticket> ticketList;
        try {
            pStmt = conn.prepareStatement(READ_ALL_TICKETS_BY_USER_ID);
            pStmt.setInt(1, id);
            rs = pStmt.executeQuery();
            if (isResultSetEmpty(rs)) throw new EntityNotFoundException("None ticket was found");
            ticketList = extractEntities(rs);
            logger.debug("Found [{}] ticket(s)", ticketList.size());
        } catch (SQLException e) {
            throw new SQLException("Error while searching all tickets by user id", e);
        } finally {
            close(rs);
            close(pStmt);
        }
        return ticketList;
    }

    @Override
    public List<Ticket> findTicketByCruiseId(int id) throws SQLException, EntityNotFoundException {
        logger.debug("Generating a find all tickets by cruise id query");
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        List<Ticket> ticketList;
        try {
            pStmt = conn.prepareStatement(READ_ALL_TICKETS_BY_CRUISE_ID);
            pStmt.setInt(1, id);
            rs = pStmt.executeQuery();
            if (isResultSetEmpty(rs)) throw new EntityNotFoundException("None ticket was found");
            ticketList = extractEntities(rs);
            logger.debug("Found [{}] ticket(s)", ticketList.size());
        } catch (SQLException e) {
            throw new SQLException("Error while searching all tickets by cruise id", e);
        } finally {
            close(rs);
            close(pStmt);
        }
        return ticketList;
    }

    @Override
    public Map<Integer, String> getStatusDict() throws SQLException, EntityNotFoundException {
        logger.debug("Generating a find status dictionary query");
        Statement stmt = null;
        ResultSet rs = null;
        Map<Integer, String> statusDict;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(READ_STATUS_DICT);
            if (isResultSetEmpty(rs)) throw new EntityNotFoundException("None status dictionary was found");
            statusDict = extractEntitiesToMap(rs);
            logger.debug("Found [{}] statuses", statusDict.size());
        } catch (SQLException e) {
            throw new SQLException("Error while searching status dictionary", e);
        } finally {
            close(rs);
            close(stmt);
        }
        return statusDict;
    }

    @Override
    public void updateAllTicketStatus() throws SQLException {
        logger.debug("Updating all ticket status");
        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate("SET SQL_SAFE_UPDATES = 0");
            int executeCount = stmt.executeUpdate(UPDATE_ALL_TICKET_STATUS);
            stmt.executeUpdate("SET SQL_SAFE_UPDATES = 1");
            logger.debug("Updates [{}] statuses of tickets", executeCount);
        } catch (SQLException e) {
            throw new SQLException("Error updating statuses of all tickets", e);
        } finally {
            close(stmt);
        }
    }


    @Override
    List<Ticket> extractEntities(ResultSet rs) throws SQLException {
        logger.debug("Extracting tickets from resultSet");
        List<Ticket> ticketList = new ArrayList<>();
        try {
            while (rs.next()) {
                Ticket ticket = new Ticket();
                ticket.setId(rs.getInt("ticket_id"));
                ticket.setCruiseId(rs.getInt("cruise_id"));
                ticket.setUserId(rs.getInt("user_id"));
                ticket.setPosition(rs.getInt("position"));
                ticket.setDocsImageName(rs.getString("docs_image_name"));
                ticket.setPrice(rs.getBigDecimal("price"));
                ticket.setStatusId(rs.getInt("status_id"));
                ticketList.add(ticket);
            }
        } catch (SQLException e) {
            throw new SQLException("Error while extracting tickets from resultSet", e);
        }
        return ticketList;
    }

    Map<Integer, String> extractEntitiesToMap(ResultSet rs) throws SQLException {
        logger.debug("Extracting tickets from resultSet to map");
        Map<Integer, String> statusDict = new HashMap<>();
        try {
            while (rs.next()) {
                statusDict.put(rs.getInt("status_id"), rs.getString("status_name"));
            }
        } catch (SQLException e) {
            throw new SQLException("Error while extracting tickets from resultSet to map", e);
        }
        return statusDict;
    }
}
