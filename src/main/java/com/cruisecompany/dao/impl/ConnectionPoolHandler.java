package com.cruisecompany.dao.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;


public class ConnectionPoolHandler {
    private static final Logger logger = LogManager.getLogger(ConnectionPoolHandler.class);
    private static ConnectionPoolHandler instance;
    DataSource ds;
    Connection conn;

    private ConnectionPoolHandler() {
        logger.debug("Opening connection pool");
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            ds = (DataSource) envContext.lookup("jdbc/cruisedb");
            conn = ds.getConnection();
        } catch (SQLException | NamingException e) {
            logger.fatal("Error while opening connection pool");
            throw new IllegalStateException("Error while opening connection pool");
        }
    }

    public static ConnectionPoolHandler getInstance() {
        if (instance == null)
            instance = new ConnectionPoolHandler();
        return instance;
    }

    public Connection getConnection() {
        return conn;
    }
}
