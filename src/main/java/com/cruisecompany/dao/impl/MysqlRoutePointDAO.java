package com.cruisecompany.dao.impl;


import com.cruisecompany.controller.util.EntityNotDeletedException;
import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.dao.RoutePointDAO;
import com.cruisecompany.entity.RoutePoint;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class MysqlRoutePointDAO extends MysqlBaseDAO<RoutePoint> implements RoutePointDAO {
    private static final Logger logger = LogManager.getLogger(MysqlRoutePointDAO.class);
    private final Connection conn;

    public static final String INSERT_ROUTEPOINT = "INSERT INTO route_point VALUES (DEFAULT, ?, DEFAULT)";
    public static final String UPDATE_ROUTEPOINT = "UPDATE route_point SET route_id = ?, point_name = ? WHERE point_id = ?";
    public static final String FIND_ROUTEPOINT_BY_ROUTE_ID = "SELECT * FROM route_point WHERE route_id = ?";
    public static final String DELETE_ROUTEPOINT_BY_POINT_ID = "DELETE FROM route_point WHERE point_id = ?";

    public MysqlRoutePointDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void insert(RoutePoint routePoint) throws SQLException, EntityNotInsertedException {
        logger.debug("Generating an insert route-point query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(INSERT_ROUTEPOINT);
            pStmt.setInt(1, routePoint.getRouteId());
            int executeCount = pStmt.executeUpdate();
            if (executeCount < 1) throw new EntityNotInsertedException("Insert query was not affected database");
            logger.debug("Inserted route-point [{}]", routePoint);
        } catch (SQLException e) {
            throw new SQLException("Error while inserting new route-point", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    public void update(RoutePoint routePoint) throws SQLException, EntityNotUpdatedException {
        logger.debug("Generating an update route-point query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(UPDATE_ROUTEPOINT);
            pStmt.setInt(1, routePoint.getRouteId());
            pStmt.setString(2, routePoint.getName());
            pStmt.setInt(3, routePoint.getId());
            int executeCount = pStmt.executeUpdate();
            if (executeCount < 1) throw new EntityNotUpdatedException("Update query was not affected database");
            logger.debug("Updated route-point [{}]", routePoint);
        } catch (SQLException e) {
            throw new SQLException("Error while updating existing route-point", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    public RoutePoint findById(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<RoutePoint> findByRouteId(int id) throws SQLException, EntityNotFoundException {
        logger.debug("Generating a find route-point by route id query");
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        List<RoutePoint> routePointList;
        try {
            pStmt = conn.prepareStatement(FIND_ROUTEPOINT_BY_ROUTE_ID);
            pStmt.setInt(1, id);
            rs = pStmt.executeQuery();
            if (isResultSetEmpty(rs)) throw new EntityNotFoundException("None route-point was found");
            routePointList = extractEntities(rs);
            logger.debug("Found [{}] route-point by route id", routePointList.size());
        } catch (SQLException e) {
            throw new SQLException("Error while searching route-point by route id", e);
        } finally {
            close(rs);
            close(pStmt);
        }
        return routePointList;
    }

    @Override
    public List<RoutePoint> findAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteById(int id) throws SQLException, EntityNotDeletedException {
        logger.debug("Generating a delete route-point by id query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(DELETE_ROUTEPOINT_BY_POINT_ID);
            pStmt.setInt(1, id);
            int executedCount = pStmt.executeUpdate();
            if (executedCount < 1) throw new EntityNotDeletedException("None route-point was deleted");
            logger.debug("Delete [{}] route-point(s)", executedCount);
        } catch (SQLException e) {
            throw new SQLException("Error while deleting route-points by id", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    List<RoutePoint> extractEntities(ResultSet rs) throws SQLException {
        logger.debug("Extracting route-points from resultSet");
        List<RoutePoint> routePointList = new ArrayList<>();
        try {
            while (rs.next()) {
                RoutePoint routePoint = new RoutePoint();
                routePoint.setRouteId(rs.getInt("route_id"));
                routePoint.setId(rs.getInt("point_id"));
                routePoint.setName(rs.getString("point_name"));
                routePointList.add(routePoint);
            }
        } catch (SQLException e) {
            throw new SQLException("Error while extracting route-points from resultSet", e);
        }
        return routePointList;
    }

}
