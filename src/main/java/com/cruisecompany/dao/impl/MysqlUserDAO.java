package com.cruisecompany.dao.impl;


import com.cruisecompany.controller.util.EntityNotDeletedException;
import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.dao.UserDAO;
import com.cruisecompany.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MysqlUserDAO extends MysqlBaseDAO<User> implements UserDAO {
    private static final Logger logger = LogManager.getLogger(MysqlUserDAO.class);
    private final Connection conn;

    public static final String CREATE_USER = "INSERT INTO app_user VALUES (DEFAULT, ?, ?, DEFAULT, DEFAULT)";
    public static final String FIND_USER_BY_ID = "SELECT * from app_user WHERE user_id = ?";
    public static final String FIND_USER_BY_LOGIN = "SELECT * from app_user WHERE user_login = ?";
    public static final String FIND_ALL_USERS = "SELECT * from app_user";
    public static final String UPDATE_USER_BY_ID = "UPDATE app_user SET user_login = ?, user_password = ?, role_id = ? WHERE user_id = ?";
    public static final String UPDATE_USER_LAST_ONLINE_BY_ID = "UPDATE app_user SET last_online_date = ? WHERE user_id = ?";
    public static final String DELETE_USER = "DELETE from app_user WHERE user_id = ?";
    public static final String FIND_ROLE_DICT = "SELECT * FROM user_role_dict";

    public MysqlUserDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void insert(User user) throws SQLException, EntityNotInsertedException {
        logger.debug("Generating an insert user query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(CREATE_USER);
            pStmt.setString(1, user.getLogin());
            pStmt.setString(2, user.getPassword());
            int executeCount = pStmt.executeUpdate();
            if (executeCount < 1) throw new EntityNotInsertedException("insert query was not affected database");
            logger.debug("Inserted user [{}]", user);
        } catch (SQLException e) {
            throw new SQLException("Error while inserting new user", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    public void update(User user) throws SQLException, EntityNotUpdatedException {
        logger.debug("Generating an update user query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(UPDATE_USER_BY_ID);
            pStmt.setString(1, user.getLogin());
            pStmt.setString(2, user.getPassword());
            pStmt.setInt(3, user.getRoleId());
            pStmt.setInt(4, user.getId());
            int executeCount = pStmt.executeUpdate();
            if (executeCount < 1) throw new EntityNotUpdatedException("update query was not affected database");
            logger.debug("updated user [{}]", user);
        } catch (SQLException e) {
            throw new SQLException("Error while updating existing user", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    public User findById(int id) throws SQLException, EntityNotFoundException {
        logger.debug("Generating a find user by id query");
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        User user;
        try {
            pStmt = conn.prepareStatement(FIND_USER_BY_ID);
            pStmt.setInt(1, id);
            rs = pStmt.executeQuery();
            if (isResultSetEmpty(rs)) throw new EntityNotFoundException("None user was found");
            user = extractEntities(rs).get(0);
            logger.debug("Found user [{}]", user);
        } catch (SQLException e) {
            throw new SQLException("Error while searching user by id", e);
        } finally {
            close(rs);
            close(pStmt);
        }
        return user;
    }

    @Override
    public User findByLogin(String login) throws SQLException, EntityNotFoundException {
        logger.debug("Generating a find user by login query");
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        User user;
        try {
            pStmt = conn.prepareStatement(FIND_USER_BY_LOGIN);
            pStmt.setString(1, login);
            rs = pStmt.executeQuery();
            if (isResultSetEmpty(rs)) throw new EntityNotFoundException("None user was found");
            user = extractEntities(rs).get(0);
            logger.debug("Found user [{}]", user);
        } catch (SQLException e) {
            throw new SQLException("Error while searching user by login");
        } finally {
            close(rs);
            close(pStmt);
        }
        return user;
    }

    @Override
    public List<User> findAll() throws SQLException, EntityNotFoundException {
        logger.debug("Generating a find all users query");
        Statement stmt = null;
        ResultSet rs = null;
        List<User> userList;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(FIND_ALL_USERS);
            if (isResultSetEmpty(rs)) throw new EntityNotFoundException("None user was found");
            userList = extractEntities(rs);
            logger.debug("Found [{}] user(s)", userList.size());
        } catch (SQLException e) {
            throw new SQLException("Error while search for all users");
        } finally {
            close(rs);
            close(stmt);
        }
        return userList;
    }

    @Override
    public void deleteById(int id) throws SQLException, EntityNotDeletedException {
        logger.debug("Gene delete user by id query [{}]. Parameters: [{}]", DELETE_USER, id);
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(DELETE_USER);
            pStmt.setInt(1, id);
            int executedCount = pStmt.executeUpdate();
            if (executedCount < 1)
                throw new EntityNotDeletedException("None user was deleted");
            logger.debug("Delete [{}] user(s)", executedCount);
        } catch (SQLException e) {
            throw new SQLException("Error while deleting user by id", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    public Map<Integer, String> getRoleDict() throws SQLException, EntityNotFoundException {
        logger.debug("Generating a find role dictionary query");
        Statement stmt = null;
        ResultSet rs = null;
        Map<Integer, String> statusDict;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(FIND_ROLE_DICT);
            if (isResultSetEmpty(rs)) throw new EntityNotFoundException("None role dictionary was found");
            statusDict = extractEntitiesToMap(rs);
            logger.debug("Found [{}] roles", statusDict.size());
        } catch (SQLException e) {
            throw new SQLException("Error while searching roles dictionary", e);
        } finally {
            close(rs);
            close(stmt);
        }
        return statusDict;
    }

    @Override
    public void updateLastOnlineDateById(int id) throws SQLException, EntityNotUpdatedException {
        logger.debug("Generating an update last online date by id query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(UPDATE_USER_LAST_ONLINE_BY_ID);
            pStmt.setString(1, String.valueOf(LocalDateTime.now()));
            pStmt.setInt(2, id);
            int executeCount = pStmt.executeUpdate();
            if (executeCount < 1) throw new EntityNotUpdatedException("update query was not affected database");
            logger.debug("updated last online date user [{}]", id);
        } catch (SQLException e) {
            throw new SQLException("Error while updating last online date to existing user", e);
        } finally {
            close(pStmt);
        }
    }


    @Override
    List<User> extractEntities(ResultSet rs) throws SQLException {
        logger.debug("Extracting users from resultSet");
        List<User> userList = new ArrayList<>();
        try {
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("user_id"));
                user.setLogin(rs.getString("user_login"));
                user.setPassword(rs.getString("user_password"));
                user.setRoleId(rs.getInt("role_id"));
                user.setLastOnlineDate((LocalDateTime) rs.getObject("last_online_date"));
                userList.add(user);
            }
        } catch (SQLException e) {
            throw new SQLException("Error while extracting users from resultSet", e);
        }
        return userList;
    }

    Map<Integer, String> extractEntitiesToMap(ResultSet rs) throws SQLException {
        logger.debug("Extracting users from resultSet to map");
        Map<Integer, String> statusDict = new HashMap<>();
        try {
            while (rs.next()) {
                statusDict.put(rs.getInt("role_id"), rs.getString("role_name"));
            }
        } catch (SQLException e) {
            throw new SQLException("Error while extracting users from resultSet to map", e);
        }
        return statusDict;
    }

}
