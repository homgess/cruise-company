package com.cruisecompany.dao.impl;


import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.dao.CruiseDAO;
import com.cruisecompany.entity.Cruise;
import com.cruisecompany.entity.Liner;
import com.cruisecompany.entity.Route;
import com.cruisecompany.entity.RoutePoint;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MysqlCruiseDAO extends MysqlBaseDAO<Cruise> implements CruiseDAO {
    private static final Logger logger = LogManager.getLogger(MysqlCruiseDAO.class);
    private final Connection conn;

    public static final String CREATE_CRUISE = "INSERT INTO cruise VALUES (DEFAULT, ?, ?, ?, ?, ?, ?)";
    public static final String CREATE_CRUISE_INFO = "INSERT INTO cruise_info VALUES (?, DEFAULT, ?, ?)";
    public static final String FIND_CRUISE_BY_ID_FULL = "SELECT cruise.cruise_id, cruise_name, cruise.liner_id, liner_name, passenger_capacity, cruise.route_id, route_name, route_point.point_id, route_point.point_name, start_datetime, end_datetime, price, cruise_info.status_id, cruise_info.description, cruise_info.image_name FROM cruise INNER JOIN liner on cruise.liner_id=liner.liner_id INNER JOIN route on cruise.route_id=route.route_id INNER JOIN cruise_info on cruise.cruise_id = cruise_info.cruise_id LEFT OUTER JOIN route_point on route.route_id = route_point.route_id WHERE cruise.cruise_id = ?";
    public static final String FIND_CRUISE_BY_NAME_FULL = "SELECT cruise.cruise_id, cruise_name, cruise.liner_id, liner_name, passenger_capacity, cruise.route_id, route_name, route_point.point_id, route_point.point_name, start_datetime, end_datetime, price, cruise_info.status_id, cruise_info.description, cruise_info.image_name FROM cruise LEFT OUTER JOIN liner on cruise.liner_id=liner.liner_id LEFT OUTER JOIN route on cruise.route_id=route.route_id LEFT OUTER JOIN cruise_info on cruise.cruise_id = cruise_info.cruise_id LEFT OUTER JOIN route_point on route.route_id = route_point.route_id WHERE cruise.cruise_name = ?";
    public static final String FIND_ALL_CRUISES = "SELECT cruise.cruise_id, cruise_name, liner_name, passenger_capacity ,route_name, (SELECT COUNT(point_id) FROM route_point WHERE route_point.route_id = cruise.route_id) as 'point_count', start_datetime, end_datetime, price, cruise_info.status_id, cruise_info.image_name FROM cruise INNER JOIN liner on cruise.liner_id=liner.liner_id INNER JOIN route on cruise.route_id=route.route_id INNER JOIN cruise_info on cruise.cruise_id = cruise_info.cruise_id";
    public static final String FIND_ALL_CRUISES_PAGINATION = "SELECT cruise.cruise_id, cruise_name, liner_name, passenger_capacity ,route_name, (SELECT COUNT(point_id) FROM route_point WHERE route_point.route_id = cruise.route_id) as 'point_count', start_datetime, end_datetime, price, cruise_info.status_id, cruise_info.image_name FROM cruise INNER JOIN liner on cruise.liner_id=liner.liner_id INNER JOIN route on cruise.route_id=route.route_id INNER JOIN cruise_info on cruise.cruise_id = cruise_info.cruise_id WHERE cruise_info.status_id = 2";
    public static final String UPDATE_CRUISE = "UPDATE cruise SET cruise_name = ?, liner_id = ?, route_id = ?, start_datetime = ?, end_datetime = ?, price = ? WHERE cruise_id = ?";
    public static final String UPDATE_CRUISE_INFO = "UPDATE cruise_info SET status_id = ?, description = ?, image_name = ? WHERE cruise_id = ?";
    public static final String DELETE_CRUISE_BY_ID = "DELETE FROM cruise WHERE cruise_id = ?";
    public static final String READ_STATUS_DICT = "SELECT * FROM cruise_status_dict";
    public static final String GET_AVAILABLE_CRUISE_COUNT = "SELECT COUNT(cruise_id) FROM cruise_info WHERE status_id = 2";
    public static final String GET_AVAILABLE_CRUISE_BY_PARAMETERS_COUNT = "SELECT COUNT(cruise.cruise_id) FROM cruise INNER JOIN cruise_info on cruise.cruise_id = cruise_info.cruise_id WHERE status_id = 2";
    public static final String UPDATE_ALL_ENDED_CRUISE_STATUSES = "UPDATE cruise_info JOIN cruise on cruise_info.cruise_id = cruise.cruise_id SET status_id = 5 WHERE status_id BETWEEN 2 AND 4 AND DATEDIFF(end_datetime, now()) < 0";
    public static final String UPDATE_ALL_STARTED_CRUISE_STATUSES = "UPDATE cruise_info JOIN cruise on cruise_info.cruise_id = cruise.cruise_id SET status_id = 4 WHERE status_id BETWEEN 2 AND 3 AND DATEDIFF(start_datetime, now()) < 0";
    public static final String UPDATE_CRUISE_STATUS_IF_FULL = "UPDATE cruise_info INNER JOIN cruise on cruise_info.cruise_id = cruise.cruise_id INNER JOIN liner on cruise.liner_id = liner.liner_id INNER JOIN ticket on cruise.cruise_id = ticket.cruise_id SET cruise_info.status_id = 3 WHERE cruise.cruise_id = ? AND cruise_info.status_id = 2 AND liner.passenger_capacity <= (SELECT COUNT(ticket_id) FROM ticket WHERE ticket.cruise_id = ? AND ticket.status_id BETWEEN 1 AND 3)";
    public static final String UPDATE_CRUISE_STATUS_IF_NOT_FULL = "UPDATE cruise_info INNER JOIN cruise on cruise_info.cruise_id = cruise.cruise_id INNER JOIN liner on cruise.liner_id = liner.liner_id INNER JOIN ticket on cruise.cruise_id = ticket.cruise_id SET cruise_info.status_id = 2 WHERE cruise.cruise_id = ? AND cruise_info.status_id = 3 AND liner.passenger_capacity > (SELECT COUNT(ticket_id) FROM ticket WHERE ticket.cruise_id = ? AND ticket.status_id BETWEEN 1 AND 3)";

    public MysqlCruiseDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void insert(Cruise cruise) throws SQLException, EntityNotInsertedException {
        logger.debug("Generating an insert cruise query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(CREATE_CRUISE);
            setCruise(cruise, pStmt);
            int executeCount = pStmt.executeUpdate();
            if (executeCount < 1) throw new EntityNotInsertedException("Insert query was not affected database");
            logger.debug("Inserted cruise [{}]", cruise);
        }
        catch (SQLException e) {
            throw new SQLException("Failed to insert new cruise", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    public void insertCruiseInfo(Cruise cruise) throws SQLException, EntityNotInsertedException {
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(CREATE_CRUISE_INFO);
            pStmt.setInt(1, cruise.getId());
            pStmt.setString(2, cruise.getDescription());
            pStmt.setString(3, cruise.getImageName());
            int executeCount = pStmt.executeUpdate();
            if (executeCount < 1) throw new EntityNotInsertedException("Insert query was not affected database");
        } catch (SQLException e) {
            throw new SQLException("Failed to insert new cruise", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    public void update(Cruise cruise) throws SQLException, EntityNotUpdatedException {
        logger.debug("Generating an update cruise query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(UPDATE_CRUISE);
            setCruise(cruise, pStmt);
            pStmt.setInt(7, cruise.getId());
            int executeCount = pStmt.executeUpdate();
            if (executeCount < 1) throw new EntityNotUpdatedException("Update query was not affected database");
            logger.debug("Updated cruise [{}]", cruise);
        } catch (SQLException e) {
            throw new SQLException("Error while updating existing cruise", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    public void updateCruiseInfo(Cruise cruise) throws SQLException, EntityNotUpdatedException {
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(UPDATE_CRUISE_INFO);
            pStmt.setInt(1, cruise.getStatusId());
            pStmt.setString(2, cruise.getDescription());
            pStmt.setString(3, cruise.getImageName());
            pStmt.setInt(4, cruise.getId());
            int executeCount = pStmt.executeUpdate();
            if (executeCount < 1) throw new EntityNotUpdatedException("Insert query was not affected database");
        } catch (SQLException e) {
            throw new SQLException("Error while updating existing cruise", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    public int getCruiseCount() throws SQLException {
        logger.debug("Generating a find all available cruises query");
        Statement stmt = null;
        ResultSet rs = null;
        int cruiseCount = 0;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(GET_AVAILABLE_CRUISE_COUNT);
            if (isResultSetEmpty(rs)) throw new SQLException("Failed to count cruises");
            if (rs.next())
                cruiseCount = rs.getInt("COUNT(cruise_id)");
            logger.debug("Found [{}] available cruises ", cruiseCount);
        } catch (SQLException e) {
            throw new SQLException("Error while searching available cruise", e);
        } finally {
            close(rs);
            close(stmt);
        }
        return cruiseCount;
    }

    @Override
    public int getCruiseCount(String dateTimeType, LocalDate startDateTime, String durationType, int duration) throws SQLException {
        logger.debug("Generating a count cruise by parameters query");
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        int cruiseCount = 0;
        StringBuilder stringBuilder = new StringBuilder(GET_AVAILABLE_CRUISE_BY_PARAMETERS_COUNT);
        if (startDateTime != null) {
            stringBuilder.append(" AND start_datetime ");
            if (dateTimeType.equals("ge"))
                stringBuilder.append(">= ? ");
            else if (dateTimeType.equals("le"))
                stringBuilder.append("<= ? ");
        }
        if (duration != 0) {
            stringBuilder.append(" AND DATEDIFF(end_datetime,start_datetime) ");
            if (durationType.equals("ge"))
                stringBuilder.append(">= ?");
            else if (durationType.equals("le"))
                stringBuilder.append(" <= ?");
        }

        try {
            int i = 1;
            pStmt = conn.prepareStatement(stringBuilder.toString());
            if (startDateTime != null)
                pStmt.setString(i++, String.valueOf(startDateTime));
            if (duration != 0)
                pStmt.setInt(i, duration);
            rs = pStmt.executeQuery();
            if (isResultSetEmpty(rs)) throw new SQLException("Failed to count cruises");
            if (rs.next())
                cruiseCount = rs.getInt("COUNT(cruise.cruise_id)");
            logger.debug("Found [{}] cruises by parameters", cruiseCount);
        } catch (SQLException e) {
            throw new SQLException("Error while searching count of cruises with parameters", e);
        } finally {
            close(rs);
            close(pStmt);
        }
        return cruiseCount;
    }

    @Override
    public Cruise findById(int id) throws SQLException, EntityNotFoundException {
        logger.debug("Generating a find cruise by id query");
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        Cruise cruise;
        try {
            pStmt = conn.prepareStatement(FIND_CRUISE_BY_ID_FULL);
            pStmt.setInt(1, id);
            rs = pStmt.executeQuery();
            if (isResultSetEmpty(rs)) throw new EntityNotFoundException("None cruiser was found");
            cruise = extractEntity(rs);
            logger.debug("Found cruise [{}]", cruise);
        } catch (SQLException e) {
            throw new SQLException("Error while searching cruise by id", e);
        } finally {
            close(rs);
            close(pStmt);
        }
        return cruise;
    }

    @Override
    public Cruise findByName(String name) throws SQLException, EntityNotFoundException {
        logger.debug("Generating a find cruise by name query");
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        Cruise cruise;
        try {
            pStmt = conn.prepareStatement(FIND_CRUISE_BY_NAME_FULL);
            pStmt.setString(1, name);
            rs = pStmt.executeQuery();
            if (isResultSetEmpty(rs)) throw new EntityNotFoundException("None cruiser was found");
            cruise = extractEntity(rs);
            logger.debug("Found cruise [{}]", cruise);
        } catch (SQLException e) {
            throw new SQLException("Error while searching cruise by id", e);
        } finally {
            close(rs);
            close(pStmt);
        }
        return cruise;
    }

    @Override
    public List<Cruise> findByParameters(String dateTimeType, LocalDate startDateTime, String durationType, int duration, int pageId, int countPerPage) throws SQLException, EntityNotFoundException {
        logger.debug("Generating a find cruise by parameters query");
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        List<Cruise> cruiseList;
        StringBuilder stringBuilder = new StringBuilder(FIND_ALL_CRUISES_PAGINATION);
        if (startDateTime != null) {
            stringBuilder.append(" AND start_datetime ");
            if (dateTimeType.equals("ge"))
                stringBuilder.append(">= ? ");
            else if (dateTimeType.equals("le"))
                stringBuilder.append("<= ? ");
        }
        if (duration != 0) {
            stringBuilder.append(" AND DATEDIFF(end_datetime,start_datetime) ");
            if (durationType.equals("ge"))
                stringBuilder.append(">= ?");
            else if (durationType.equals("le"))
                stringBuilder.append(" <= ?");
        }
        stringBuilder.append(" LIMIT ?, ?");
        try {
            int i = 1;
            pStmt = conn.prepareStatement(stringBuilder.toString());
            if (startDateTime != null)
                pStmt.setString(i++, String.valueOf(startDateTime));
            if (duration != 0)
                pStmt.setInt(i++, duration);
            pStmt.setInt(i++, (pageId - 1) * countPerPage);
            pStmt.setInt(i, countPerPage);
            rs = pStmt.executeQuery();
            if (isResultSetEmpty(rs)) throw new EntityNotFoundException("None cruiser was found");
            cruiseList = extractEntities(rs);
            logger.debug("Found [{}] cruises by parameters", cruiseList.size());
        } catch (SQLException e) {
            throw new SQLException("Error while searching route-point by route id", e);
        } finally {
            close(rs);
            close(pStmt);
        }
        return cruiseList;
    }


    @Override
    public Map<Integer, String> getStatusDict() throws SQLException, EntityNotFoundException {
        logger.debug("Generating a find status dictionary query");
        Statement stmt = null;
        ResultSet rs = null;
        Map<Integer, String> statusDict;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(READ_STATUS_DICT);
            if (isResultSetEmpty(rs)) throw new EntityNotFoundException("None status dictionary was found");
            statusDict = extractEntitiesToMap(rs);
            logger.debug("Found [{}] statuses", statusDict.size());
        } catch (SQLException e) {
            throw new SQLException("Error while searching status dictionary", e);
        } finally {
            close(rs);
            close(stmt);
        }
        return statusDict;
    }

    @Override
    public void updateCruiseStatusIfFull(int id) throws SQLException {
        logger.debug("Generating a update cruise status if full query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(UPDATE_CRUISE_STATUS_IF_FULL);
            pStmt.setInt(1, id);
            pStmt.setInt(2, id);
            int executeCount = pStmt.executeUpdate();
            logger.debug("Updates status of [{}] full cruise(s)", executeCount);
        } catch (SQLException e) {
            throw new SQLException("Error while updating status for full cruise by id", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    public void updateCruiseStatusIfNotFull(int id) throws SQLException {
        logger.debug("Generating a update cruise status if not full query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(UPDATE_CRUISE_STATUS_IF_NOT_FULL);
            pStmt.setInt(1, id);
            pStmt.setInt(2, id);
            int executeCount = pStmt.executeUpdate();
            logger.debug("Updates status of [{}] not full cruise(s)", executeCount);
        } catch (SQLException e) {
            throw new SQLException("Error while updating status for not full cruise by id", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    public void updateAllEndedCruiseStatuses() throws SQLException {
        logger.debug("Generating a update all ended cruises status query");
        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            int executeCount = stmt.executeUpdate(UPDATE_ALL_ENDED_CRUISE_STATUSES);
            logger.debug("Updated status for [{}] ended cruises", executeCount);
        } catch (SQLException e) {
            throw new SQLException("Error while updating status of all ended cruises", e);
        } finally {
            close(stmt);
        }
    }

    @Override
    public void updateAllStartedCruiseStatuses() throws SQLException {
        logger.debug("Generating a update all started cruises status query");
        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate("SET SQL_SAFE_UPDATES = 0");
            int executeCount = stmt.executeUpdate(UPDATE_ALL_STARTED_CRUISE_STATUSES);
            stmt.executeUpdate("SET SQL_SAFE_UPDATES = 1");
            logger.debug("Updated status for [{}] started cruises", executeCount);
        } catch (SQLException e) {
            throw new SQLException("Error while updating status of all started cruises", e);
        } finally {
            close(stmt);
        }
    }

    @Override
    public List<Cruise> findAll() throws SQLException {
        logger.debug("Generating a find all cruises query");
        Statement stmt = null;
        ResultSet rs = null;
        List<Cruise> cruiseList;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(FIND_ALL_CRUISES);
            if (isResultSetEmpty(rs)) throw new SQLException("None cruiser was found");
            cruiseList = extractEntities(rs);
            logger.debug("Found [{}] cruises ", cruiseList.size());
        } catch (SQLException e) {
            throw new SQLException("Error while searching cruise by id", e);
        } finally {
            close(rs);
            close(stmt);
        }
        return cruiseList;
    }

    @Override
    public void deleteById(int id) throws SQLException {
        logger.debug("Generating a delete cruise by id query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(DELETE_CRUISE_BY_ID);
            pStmt.setInt(1, id);
            int executeCount = pStmt.executeUpdate();
            if (executeCount < 1) throw new SQLException("None cruiser was deleted");
            logger.debug("Delete [{}] cruise(s)", executeCount);
        } catch (SQLException e) {
            throw new SQLException("Error while deleting cruise by id", e);
        } finally {
            close(pStmt);
        }
    }


    @Override
    List<Cruise> extractEntities(ResultSet rs) throws SQLException {
        logger.debug("Extracting cruises from resultSet");
        List<Cruise> cruiseList = new ArrayList<>();
        try {
            while (rs.next()) {
                Cruise cruise = new Cruise();
                cruise.setId(rs.getInt("cruise_id"));
                cruise.setName(rs.getString("cruise_name"));
                Liner liner = new Liner();
                liner.setName(rs.getString("liner_name"));
                liner.setPassengerCapacity(rs.getInt("passenger_capacity"));
                cruise.setLiner(liner);
                Route route = new Route();
                route.setName(rs.getString("route_name"));
                route.setPointCount(rs.getInt("point_count"));
                cruise.setRoute(route);
                cruise.setStartDateTime((LocalDateTime) rs.getObject("start_datetime"));
                cruise.setEndDateTime((LocalDateTime) rs.getObject("end_datetime"));
                cruise.setPrice(rs.getBigDecimal("price"));
                cruise.setStatusId(rs.getInt("status_id"));
                cruise.setImageName(rs.getString("image_name"));
                cruiseList.add(cruise);
            }
        } catch (SQLException e) {
            throw new SQLException("Error while extracting cruises from resultSet", e);
        }
        return cruiseList;
    }

    Cruise extractEntity(ResultSet rs) throws SQLException {
        logger.debug("Extracting one cruise from resultSet");
        Cruise cruise = new Cruise();
        try {
            Route route = new Route();
            if (rs.next()) {
                cruise.setId(rs.getInt("cruise_id"));
                cruise.setName(rs.getString("cruise_name"));
                Liner liner = new Liner();
                liner.setId(rs.getInt("liner_id"));
                liner.setName(rs.getString("liner_name"));
                liner.setPassengerCapacity(rs.getInt("passenger_capacity"));
                cruise.setLiner(liner);
                route.setId(rs.getInt("route_id"));
                route.setName(rs.getString("route_name"));
                route.addRoutePointtoList(new RoutePoint(rs.getInt("route_id"), rs.getInt("point_id"), rs.getString("point_name")));
                cruise.setStartDateTime((LocalDateTime) rs.getObject("start_datetime"));
                cruise.setEndDateTime((LocalDateTime) rs.getObject("end_datetime"));
                cruise.setPrice(rs.getBigDecimal("price"));
                cruise.setStatusId(rs.getInt("status_id"));
                cruise.setDescription(rs.getString("description"));
                cruise.setImageName(rs.getString("image_name"));
            }
            while (rs.next()) {
                route.addRoutePointtoList(new RoutePoint(rs.getInt("route_id"), rs.getInt("point_id"), rs.getString("point_name")));
            }
            cruise.setRoute(route);
        } catch (SQLException e) {
            throw new SQLException("Error while extracting full cruises from resultSet", e);
        }
        return cruise;
    }

    Map<Integer, String> extractEntitiesToMap(ResultSet rs) throws SQLException {
        logger.debug("Extracting tickets from resultSet to map");
        Map<Integer, String> statusDict = new HashMap<>();
        try {
            while (rs.next()) {
                statusDict.put(rs.getInt("status_id"), rs.getString("status_name"));
            }
        } catch (SQLException e) {
            throw new SQLException("Error while extracting tickets from resultSet to map", e);
        }
        return statusDict;
    }

    private void setCruise(Cruise cruise, PreparedStatement pStmt1) throws SQLException {
        pStmt1.setString(1, cruise.getName());
        pStmt1.setInt(2, cruise.getLiner().getId());
        pStmt1.setInt(3, cruise.getRoute().getId());
        pStmt1.setString(4, String.valueOf(cruise.getStartDateTime()));
        pStmt1.setString(5, String.valueOf(cruise.getEndDateTime()));
        pStmt1.setBigDecimal(6, cruise.getPrice());
    }

}
