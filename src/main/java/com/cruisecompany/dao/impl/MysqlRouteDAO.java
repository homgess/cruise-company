package com.cruisecompany.dao.impl;


import com.cruisecompany.controller.util.EntityNotDeletedException;
import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.dao.RouteDAO;
import com.cruisecompany.entity.Route;
import com.cruisecompany.entity.RoutePoint;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class MysqlRouteDAO extends MysqlBaseDAO<Route> implements RouteDAO {
    private static final Logger logger = LogManager.getLogger(MysqlRouteDAO.class);
    private final Connection conn;

    public static final String CREATE_ROUTE = "INSERT INTO route VALUES (DEFAULT, ?)";
    public static final String FIND_ROUTE_BY_ID = "SELECT route.route_id, route_name, point_id, point_name FROM route LEFT OUTER JOIN route_point ON route.route_id = route_point.route_id WHERE route.route_id = ?";
    public static final String FIND_ALL_ROUTES = "SELECT route.route_id, route_name, point_id, point_name FROM route LEFT OUTER JOIN route_point ON route.route_id = route_point.route_id";
    public static final String UPDATE_ROUTE = "UPDATE route SET route_name = ? WHERE route_id = ?";
    public static final String DELETE_ROUTE_BY_ID = "DELETE FROM route WHERE route_id = ?";

    public MysqlRouteDAO(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void insert(Route route) throws SQLException, EntityNotInsertedException {
        logger.debug("Generating an insert route query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(CREATE_ROUTE);
            pStmt.setString(1, route.getName());
            int executeCount = pStmt.executeUpdate();
            if (executeCount < 1) throw new EntityNotInsertedException("Insert query was not affected database");
            logger.debug("Inserted route [{}]", route);
        } catch (SQLException e) {
            throw new SQLException("Error while inserting new route", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    public void update(Route route) throws SQLException, EntityNotUpdatedException {
        logger.debug("Generating an update route query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(UPDATE_ROUTE);
            pStmt.setString(1, route.getName());
            pStmt.setInt(2, route.getId());
            int executeCount = pStmt.executeUpdate();
            if (executeCount < 1) throw new EntityNotUpdatedException("Update query was not affected database");
            logger.debug("Updated route [{}]", route);
        } catch (SQLException e) {
            throw new SQLException("Error while updating existing route", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    public Route findById(int id) throws SQLException, EntityNotFoundException {
        logger.debug("Generating a find route by id query");
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        Route route;
        try {
            pStmt = conn.prepareStatement(FIND_ROUTE_BY_ID);
            pStmt.setInt(1, id);
            rs = pStmt.executeQuery();
            if (isResultSetEmpty(rs)) throw new EntityNotFoundException("None route was found");
            route = extractEntities(rs).get(0);
            logger.debug("Found route [{}]", route);
        } catch (SQLException e) {
            throw new SQLException("Error while searching route by id", e);
        } finally {
            close(rs);
            close(pStmt);
        }
        return route;
    }

    @Override
    public List<Route> findAll() throws SQLException, EntityNotFoundException {
        logger.debug("Generating a find all routes query");
        Statement stmt = null;
        ResultSet rs = null;
        List<Route> routeList;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(FIND_ALL_ROUTES);
            if (isResultSetEmpty(rs)) throw new EntityNotFoundException("None route was found");
            routeList = extractEntities(rs);
            logger.debug("Found [{}] routes", routeList.size());
        } catch (SQLException e) {
            throw new SQLException("Error while searching all routes", e);
        } finally {
            close(rs);
            close(stmt);
        }
        return routeList;
    }

    @Override
    public void deleteById(int id) throws SQLException, EntityNotDeletedException {
        logger.debug("Generating a delete route by id query");
        PreparedStatement pStmt = null;
        try {
            pStmt = conn.prepareStatement(DELETE_ROUTE_BY_ID);
            pStmt.setInt(1, id);
            int executedCount = pStmt.executeUpdate();
            if (executedCount < 1) throw new EntityNotDeletedException("None route was deleted");
            logger.debug("Delete [{}] route(s)", executedCount);
        } catch (SQLException e) {
            throw new SQLException("Error while deleting route by id", e);
        } finally {
            close(pStmt);
        }
    }

    @Override
    List<Route> extractEntities(ResultSet rs) throws SQLException {
        logger.debug("Extracting routes from resultSet");
        List<Route> routeList = new ArrayList<>();
        try {
            while (rs.next()) {
                Route route = new Route();
                route.setId(rs.getInt("route_id"));
                route.setName(rs.getString("route_name"));
                RoutePoint routePoint = new RoutePoint();
                routePoint.setRouteId(rs.getInt("route_id"));
                routePoint.setId(rs.getInt("point_id"));
                routePoint.setName(rs.getString("point_name"));
                int id = routeList.indexOf(route);
                if (id > -1) {
                    route = routeList.get(id);
                    if (routePoint.getId() != 0)
                        route.addRoutePointtoList(routePoint);
                    routeList.set(id, route);
                } else {
                    if (routePoint.getId() != 0)
                        route.addRoutePointtoList(routePoint);
                    routeList.add(route);
                }
            }
        } catch (SQLException e) {
            throw new SQLException("Error while extracting routes from resultSet", e);
        }
        return routeList;
    }
}
