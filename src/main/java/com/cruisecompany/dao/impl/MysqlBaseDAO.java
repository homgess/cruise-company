package com.cruisecompany.dao.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


public abstract class MysqlBaseDAO<T> {
    private static final Logger logger = LogManager.getLogger(MysqlBaseDAO.class);

    static void close(AutoCloseable obj) throws IllegalStateException {
        if (obj != null) {
            logger.debug("Closing resource [{}]", obj.getClass().getSimpleName());
            try {
                obj.close();
            } catch (Exception e) {
                logger.warn("Error while closing db connection", e);
            }
        }
    }

    public static boolean isResultSetEmpty(ResultSet rs) throws SQLException {
        return (!rs.isBeforeFirst() && rs.getRow() == 0);
    }

    abstract List<T> extractEntities(ResultSet rs) throws SQLException;

}
