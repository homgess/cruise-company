package com.cruisecompany.dao;


import com.cruisecompany.dao.impl.MysqlDAOFactory;


public abstract class DAOFactory {
    public static final int MYSQL = 1;

    public abstract UserDAO getUserDAO();

    public abstract CruiseDAO getCruiseDAO();

    public abstract LinerDAO getLinerDAO();

    public abstract RouteDAO getRouteDAO();

    public abstract RoutePointDAO getRoutePointDAO();

    public abstract TicketDAO getTicketDAO();

    public static DAOFactory getDAOFactory(int factoryId) {
        if (factoryId == 1) {
            return new MysqlDAOFactory();
        }
        throw new IllegalArgumentException("Failed to get specific DAO factory");
    }
}
