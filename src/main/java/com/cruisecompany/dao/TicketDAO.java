package com.cruisecompany.dao;


import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.entity.Ticket;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;


public interface TicketDAO extends GenericDAO<Ticket> {
    void updateTicketStatus(int ticketId, int statusId) throws SQLException, EntityNotUpdatedException;

    List<Ticket> findTicketByUserId(int id) throws SQLException, EntityNotFoundException;

    List<Ticket> findTicketByCruiseId(int id) throws SQLException, EntityNotFoundException;

    Map<Integer, String> getStatusDict() throws SQLException, EntityNotFoundException;

    void updateAllTicketStatus() throws SQLException;
}
