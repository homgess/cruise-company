package com.cruisecompany.dao;

import com.cruisecompany.controller.util.EntityNotDeletedException;
import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;

import java.sql.SQLException;
import java.util.List;

public interface GenericDAO<T> {
    void insert(T obj) throws SQLException, EntityNotInsertedException;

    void update(T obj) throws SQLException, EntityNotUpdatedException;

    T findById(int id) throws SQLException, EntityNotFoundException;

    List<T> findAll() throws SQLException, EntityNotFoundException;

    void deleteById(int id) throws SQLException, EntityNotDeletedException;
}
