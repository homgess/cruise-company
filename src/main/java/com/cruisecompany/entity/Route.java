package com.cruisecompany.entity;


import java.util.*;


public class Route {
    private int id;
    private String name;
    private int pointCount;
    private final List<RoutePoint> routePointList = new ArrayList<>();

    public Route() {
    }

    public Route(int id) {
        this.id = id;
    }

    public Route(String name) {
        this.name = name;
    }

    public Route(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPointCount() {
        return pointCount;
    }

    public void setPointCount(int pointCount) {
        this.pointCount = pointCount;
    }

    public List<RoutePoint> getRoutePointList() {
        return routePointList;
    }

    public void addRoutePointtoList(RoutePoint routePoint) {
        this.routePointList.add(routePoint);
    }

    @Override
    public String toString() {
        return "Route{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", routePointList=" + routePointList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Route route = (Route) o;
        return id == route.id && Objects.equals(name, route.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
