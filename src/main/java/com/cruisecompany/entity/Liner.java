package com.cruisecompany.entity;


import java.util.Objects;


public class Liner {
    private int id;
    private String name;
    private int passengerCapacity;

    public Liner() {
    }

    public Liner(int id) {
        this.id = id;
    }

    public Liner(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Liner(int id, String name, int passengerCapacity) {
        this.id = id;
        this.name = name;
        this.passengerCapacity = passengerCapacity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    public void setPassengerCapacity(int passengerCapacity) {
        this.passengerCapacity = passengerCapacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Liner liner = (Liner) o;
        return id == liner.id && passengerCapacity == liner.passengerCapacity && Objects.equals(name, liner.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, passengerCapacity);
    }

    @Override
    public String toString() {
        return "Liner{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", passengerCapacity=" + passengerCapacity +
                '}';
    }
}
