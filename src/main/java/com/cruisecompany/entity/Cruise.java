package com.cruisecompany.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

public class Cruise {
    private int id;
    private String name;
    private Liner liner;
    private Route route;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private BigDecimal price;
    private String description;
    private String imageName;
    private int statusId;

    public Cruise() {
    }

    public Cruise(int id) {
        this.id = id;
    }

    public Cruise(String name, Liner liner, Route route, LocalDateTime startDateTime, LocalDateTime endDateTime, BigDecimal price) {
        this.name = name;
        this.liner = liner;
        this.route = route;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Liner getLiner() {
        return liner;
    }

    public void setLiner(Liner liner) {
        this.liner = liner;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public LocalDateTime getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(LocalDateTime endDateTime) {
        this.endDateTime = endDateTime;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cruise cruise = (Cruise) o;
        return id == cruise.id && Objects.equals(name, cruise.name) && Objects.equals(liner, cruise.liner) && Objects.equals(route, cruise.route) && Objects.equals(startDateTime, cruise.startDateTime) && Objects.equals(endDateTime, cruise.endDateTime) && Objects.equals(price, cruise.price);
    }

    @Override
    public String toString() {
        return "Cruise{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", liner=" + liner +
                ", route=" + route +
                ", startDateTime=" + startDateTime +
                ", endDateTime=" + endDateTime +
                ", price=" + price +
                '}';
    }

}
