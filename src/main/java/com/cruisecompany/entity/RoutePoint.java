package com.cruisecompany.entity;


import java.util.Objects;


public class RoutePoint {
    private int routeId;
    private int id;
    private String name;

    public RoutePoint() {
    }

    public RoutePoint(int routeId) {
        this.routeId = routeId;
    }

    public RoutePoint(int id, int routeId, String name) {
        this.id = id;
        this.routeId = routeId;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "RoutePoint{" +
                "route_id=" + routeId +
                ", point_id=" + id +
                ", point_name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoutePoint that = (RoutePoint) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
