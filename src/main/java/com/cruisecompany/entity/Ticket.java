package com.cruisecompany.entity;


import java.math.BigDecimal;
import java.util.Objects;


public class Ticket {
    private int id;
    private int cruiseId;
    private int userId;
    private int position;
    private String docsImageName;
    private BigDecimal price;
    private int statusId;

    public Ticket() {
    }

    public Ticket(int id) {
        this.id = id;
    }

    public Ticket(int cruiseId, int userId, int position, String docsImageName, BigDecimal price) {
        this.cruiseId = cruiseId;
        this.userId = userId;
        this.position = position;
        this.docsImageName = docsImageName;
        this.price = price;
    }

    public Ticket(int id, int cruiseId, int userId, int position, String docsImageName, BigDecimal price) {
        this.id = id;
        this.cruiseId = cruiseId;
        this.userId = userId;
        this.position = position;
        this.docsImageName = docsImageName;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCruiseId() {
        return cruiseId;
    }

    public void setCruiseId(int cruiseId) {
        this.cruiseId = cruiseId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getDocsImageName() {
        return docsImageName;
    }

    public void setDocsImageName(String docsImageName) {
        this.docsImageName = docsImageName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return id == ticket.id && cruiseId == ticket.cruiseId && userId == ticket.userId && position == ticket.position && statusId == ticket.statusId && Objects.equals(docsImageName, ticket.docsImageName) && Objects.equals(price, ticket.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cruiseId, userId, position, docsImageName, price);
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", cruiseId=" + cruiseId +
                ", userId=" + userId +
                ", position=" + position +
                ", docsImageName='" + docsImageName + '\'' +
                ", price=" + price +
                ", statusId=" + statusId +
                '}';
    }
}
