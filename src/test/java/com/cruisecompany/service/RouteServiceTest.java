package com.cruisecompany.service;


import com.cruisecompany.controller.util.EntityNotDeletedException;
import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.dao.DAOFactory;
import com.cruisecompany.dao.impl.MysqlRouteDAO;
import com.cruisecompany.entity.Route;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@ExtendWith(MockitoExtension.class)
class RouteServiceTest {

    @Mock
    DAOFactory mockDaoFactory;

    @Mock
    MysqlRouteDAO mockMysqlRouteDAO;

    RouteService routeService;

    @BeforeEach
    void setUp(){
        Mockito.when(mockDaoFactory.getRouteDAO()).thenReturn(mockMysqlRouteDAO);
        routeService = new RouteService(mockDaoFactory);
    }

    @Test
    void findAllSuccess() throws SQLException, EntityNotFoundException {
        List<Route> routeList = new ArrayList<>();
        Route route1 = new Route(1, "Costra-lanka");
        Route route2 = new Route(2, "Puerto-rikko 3 days freedom");
        Route route3 = new Route(3, "Atlantis");
        routeList.add(route1);
        routeList.add(route2);
        routeList.add(route3);
        Mockito.when(mockMysqlRouteDAO.findAll()).thenReturn(routeList);
        Assertions.assertEquals(routeList,  routeService.findAllRoutes());
        Mockito.verify(mockMysqlRouteDAO, Mockito.times(1)).findAll();
    }

    @Test
    void findAllFail() throws SQLException, EntityNotFoundException {
        Mockito.doThrow(new SQLException()).when(mockMysqlRouteDAO).findAll();
        Assertions.assertThrows(SQLException.class, () ->  routeService.findAllRoutes());
        Mockito.verify(mockMysqlRouteDAO, Mockito.times(1)).findAll();
    }

    @Test
    void findRouteByIdSuccess() throws SQLException, EntityNotFoundException {
        Route route = new Route(2, "Puerto-rikko 3 days freedom");
        Mockito.when(mockMysqlRouteDAO.findById(Mockito.anyInt())).thenReturn(route);
        Assertions.assertEquals(route, routeService.findRouteById(route.getId()));
        Mockito.verify(mockMysqlRouteDAO, Mockito.times(1)).findById(Mockito.anyInt());
    }

    @Test
    void findRouteByIdFail() throws SQLException, EntityNotFoundException {
        Route route = new Route(2, "Puerto-rikko 3 days freedom");
        Mockito.doThrow(new SQLException()).when(mockMysqlRouteDAO).findById(Mockito.anyInt());
        Assertions.assertThrows(SQLException.class, () ->  routeService.findRouteById(route.getId()));
        Mockito.verify(mockMysqlRouteDAO, Mockito.times(1)).findById(Mockito.anyInt());
    }

    @Test
    void saveRouteSuccess() throws SQLException, EntityNotInsertedException {
        Route route = new Route(2, "Puerto-rikko 3 days freedom");
        routeService.saveRoute(route);
        Mockito.verify(mockMysqlRouteDAO,Mockito.times(1)).insert(Mockito.any(Route.class));
    }

    @Test
    void saveRouteFail() throws SQLException, EntityNotInsertedException {
        Route route = new Route(2, "Puerto-rikko 3 days freedom");
        Mockito.doThrow(new SQLException()).when(mockMysqlRouteDAO).insert(Mockito.any(Route.class));
        Assertions.assertThrows(SQLException.class, () ->  routeService.saveRoute(route));
        Mockito.verify(mockMysqlRouteDAO,Mockito.times(1)).insert(Mockito.any(Route.class));
    }

    @Test
    void modifyRouteSuccess() throws SQLException, EntityNotUpdatedException {
        Route route = new Route(2, "Puerto-rikko 3 days freedom");
        routeService.modifyRoute(route);
        Mockito.verify(mockMysqlRouteDAO,Mockito.times(1)).update(Mockito.any(Route.class));
    }

    @Test
    void modifyRouteFail() throws SQLException, EntityNotUpdatedException {
        Route route = new Route(2, "Puerto-rikko 3 days freedom");
        Mockito.doThrow(new SQLException()).when(mockMysqlRouteDAO).update(Mockito.any(Route.class));
        Assertions.assertThrows(SQLException.class, () -> routeService.modifyRoute(route));
        Mockito.verify(mockMysqlRouteDAO,Mockito.times(1)).update(Mockito.any(Route.class));
    }

    @Test
    void deleteRouteByIdSuccess() throws SQLException, EntityNotDeletedException {
        routeService.deleteRouteById(131);
        Mockito.verify(mockMysqlRouteDAO,Mockito.times(1)).deleteById(Mockito.anyInt());
    }

    @Test
    void deleteRouteByIdFail() throws SQLException, EntityNotDeletedException {
        Mockito.doThrow(new SQLException()).when(mockMysqlRouteDAO).deleteById(Mockito.anyInt());
        Assertions.assertThrows(SQLException.class, () -> routeService.deleteRouteById(131));
        Mockito.verify(mockMysqlRouteDAO,Mockito.times(1)).deleteById(Mockito.anyInt());
    }
}