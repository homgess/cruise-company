package com.cruisecompany.service;

import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.dao.DAOFactory;
import com.cruisecompany.dao.impl.MysqlTicketDAO;
import com.cruisecompany.entity.Ticket;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@ExtendWith(MockitoExtension.class)
class TicketServiceTest {

    @Mock
    DAOFactory mockDaoFactory;

    @Mock
    MysqlTicketDAO mockMysqlTicketDAO;

    TicketService ticketService;

    @BeforeEach
    void setUp(){
        Mockito.when(mockDaoFactory.getTicketDAO()).thenReturn(mockMysqlTicketDAO);
        ticketService = new TicketService(mockDaoFactory);
    }

    @Test
    void findAllSuccess() throws SQLException, EntityNotFoundException {
        List<Ticket> ticketList = new ArrayList<>();
        Ticket ticket1 = new Ticket(211, 13, 4,2, "img.jpg", new BigDecimal("100"));
        Ticket ticket2 = new Ticket(212, 12, 3,4, "img2.jpg", new BigDecimal("100"));
        Ticket ticket3 = new Ticket(213, 11, 2,7, "img3.jpg", new BigDecimal("100"));
        ticketList.add(ticket1);
        ticketList.add(ticket2);
        ticketList.add(ticket3);
        Mockito.when(mockMysqlTicketDAO.findAll()).thenReturn(ticketList);
        Assertions.assertEquals(ticketList,  ticketService.findAllTickets());
        Mockito.verify(mockMysqlTicketDAO, Mockito.times(1)).findAll();
    }

    @Test
    void findAllFail() throws SQLException, EntityNotFoundException {
        Mockito.doThrow(new SQLException()).when(mockMysqlTicketDAO).findAll();
        Assertions.assertThrows(SQLException.class, () ->  ticketService.findAllTickets());
        Mockito.verify(mockMysqlTicketDAO, Mockito.times(1)).findAll();
    }

    @Test
    void findTicketByIdSuccess() throws SQLException, EntityNotFoundException {
        Ticket ticket = new Ticket(213, 11, 2,8, "img3.jpg", new BigDecimal("100"));
        Mockito.when(mockMysqlTicketDAO.findById(Mockito.anyInt())).thenReturn(ticket);
        Assertions.assertEquals(ticket, ticketService.findTicketById(ticket.getId()));
        Mockito.verify(mockMysqlTicketDAO, Mockito.times(1)).findById(Mockito.anyInt());
    }

    @Test
    void findTicketByIdFail() throws SQLException, EntityNotFoundException {
        Ticket ticket = new Ticket(213, 11, 2,9, "img3.jpg", new BigDecimal("100"));
        Mockito.doThrow(new SQLException()).when(mockMysqlTicketDAO).findById(Mockito.anyInt());
        Assertions.assertThrows(SQLException.class, () ->  ticketService.findTicketById(ticket.getId()));
        Mockito.verify(mockMysqlTicketDAO, Mockito.times(1)).findById(Mockito.anyInt());
    }

    @Test
    void findAllTicketsByUserIdSuccess() throws SQLException, EntityNotFoundException {
        List<Ticket> ticketList = new ArrayList<>();
        Ticket ticket1 = new Ticket(211, 13, 4,3, "img.jpg", new BigDecimal("100"));
        Ticket ticket2 = new Ticket(212, 12, 3,2, "img2.jpg", new BigDecimal("100"));
        Ticket ticket3 = new Ticket(213, 11, 2,1, "img3.jpg", new BigDecimal("100"));
        ticketList.add(ticket1);
        ticketList.add(ticket2);
        ticketList.add(ticket3);
        Mockito.when(mockMysqlTicketDAO.findTicketByUserId(Mockito.anyInt())).thenReturn(ticketList);
        Assertions.assertEquals(ticketList, ticketService.findAllTicketsByUserId(1337));
        Mockito.verify(mockMysqlTicketDAO, Mockito.times(1)).findTicketByUserId(Mockito.anyInt());
    }

    @Test
    void findAllTicketsByUserIdFail() throws SQLException, EntityNotFoundException {
        Mockito.doThrow(new SQLException()).when(mockMysqlTicketDAO).findTicketByUserId(Mockito.anyInt());
        Assertions.assertThrows(SQLException.class, () -> ticketService.findAllTicketsByUserId(1337));
        Mockito.verify(mockMysqlTicketDAO, Mockito.times(1)).findTicketByUserId(Mockito.anyInt());
    }

    @Test
    void saveTicketSuccess() throws SQLException, EntityNotInsertedException {
        Ticket ticket = new Ticket(213, 11, 2,1, "img3.jpg", new BigDecimal("100"));
        ticketService.saveTicket(ticket);
        Mockito.verify(mockMysqlTicketDAO,Mockito.times(1)).insert(Mockito.any(Ticket.class));
    }

    @Test
    void saveTicketFail() throws SQLException, EntityNotInsertedException {
        Ticket ticket = new Ticket(213, 11, 2,5, "img3.jpg", new BigDecimal("100"));
        Mockito.doThrow(new SQLException()).when(mockMysqlTicketDAO).insert(Mockito.any(Ticket.class));
        Assertions.assertThrows(SQLException.class, () ->  ticketService.saveTicket(ticket));
        Mockito.verify(mockMysqlTicketDAO,Mockito.times(1)).insert(Mockito.any(Ticket.class));
    }

    @Test
    void modifyTicketStatusSuccess() throws SQLException, EntityNotUpdatedException {
        ticketService.modifyTicketStatus(123, 4);
        Mockito.verify(mockMysqlTicketDAO,Mockito.times(1)).updateTicketStatus(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void modifyTicketStatusFail() throws SQLException, EntityNotUpdatedException {
        Mockito.doThrow(new SQLException()).when(mockMysqlTicketDAO).updateTicketStatus(Mockito.anyInt(), Mockito.anyInt());
        Assertions.assertThrows(SQLException.class, () ->  ticketService.modifyTicketStatus(123, 4));
        Mockito.verify(mockMysqlTicketDAO,Mockito.times(1)).updateTicketStatus(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void getStatusDictSuccess() throws SQLException, EntityNotFoundException {
        Map<Integer, String> statusDict = new HashMap<>();
        statusDict.put(1,"qwerty");
        Mockito.when(mockMysqlTicketDAO.getStatusDict()).thenReturn(statusDict);
        Assertions.assertEquals(statusDict, ticketService.findStatusDict());
        Mockito.verify(mockMysqlTicketDAO, Mockito.times(1)).getStatusDict();
    }

    @Test
    void getStatusDictFail() throws SQLException, EntityNotFoundException {
        Mockito.doThrow(new SQLException()).when(mockMysqlTicketDAO).getStatusDict();
        Assertions.assertThrows(SQLException.class,() -> ticketService.findStatusDict());
        Mockito.verify(mockMysqlTicketDAO, Mockito.times(1)).getStatusDict();
    }
}