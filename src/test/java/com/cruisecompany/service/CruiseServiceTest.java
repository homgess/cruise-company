package com.cruisecompany.service;


import com.cruisecompany.controller.util.EntityNotDeletedException;
import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.dao.DAOFactory;
import com.cruisecompany.dao.impl.MysqlCruiseDAO;
import com.cruisecompany.entity.Cruise;
import com.cruisecompany.entity.Liner;
import com.cruisecompany.entity.Route;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@ExtendWith(MockitoExtension.class)
class CruiseServiceTest {

    @Mock
    DAOFactory mockDaoFactory;

    @Mock
    MysqlCruiseDAO mockMySqlCruiseDAO;

    CruiseService cruiseService;

    @BeforeEach
    void setUp(){
        Mockito.when(mockDaoFactory.getCruiseDAO()).thenReturn(mockMySqlCruiseDAO);
        cruiseService = new CruiseService(mockDaoFactory);
    }

    @Test
    void saveCruiseSuccess() throws SQLException, EntityNotFoundException, EntityNotInsertedException {
        Cruise cruise = new Cruise("Test cruise", new Liner(1), new Route(2), LocalDateTime.MIN, LocalDateTime.MAX, new BigDecimal("123"));
        cruise.setId(11);
        Mockito.when(mockMySqlCruiseDAO.findByName(Mockito.anyString())).thenReturn(cruise);
        cruiseService.saveCruise(cruise);
        Mockito.verify(mockMySqlCruiseDAO,Mockito.times(1)).insert(Mockito.any(Cruise.class));
        Mockito.verify(mockMySqlCruiseDAO,Mockito.times(1)).findByName(Mockito.anyString());
        Mockito.verify(mockMySqlCruiseDAO,Mockito.times(1)).insertCruiseInfo(Mockito.any(Cruise.class));
    }

    @Test
    void saveCruiseFail() throws SQLException, EntityNotInsertedException, EntityNotFoundException {
        Cruise cruise = new Cruise( "Test cruise", new Liner(1), new Route(2), LocalDateTime.MIN, LocalDateTime.MAX, new BigDecimal("123"));
        cruise.setId(11);
        Mockito.doThrow(new SQLException()).when(mockMySqlCruiseDAO).insert(Mockito.any(Cruise.class));
        Assertions.assertThrows(SQLException.class, () ->  cruiseService.saveCruise(cruise));
        Mockito.verify(mockMySqlCruiseDAO,Mockito.times(1)).insert(Mockito.any(Cruise.class));
        Mockito.verify(mockMySqlCruiseDAO,Mockito.times(0)).findByName(Mockito.anyString());
        Mockito.verify(mockMySqlCruiseDAO,Mockito.times(0)).insertCruiseInfo(Mockito.any(Cruise.class));
    }

    @Test
    void findAllSuccess() throws SQLException, EntityNotFoundException {
        List<Cruise> cruiseList = new ArrayList<>();
        Cruise cruise1 = new Cruise( "Test cruise", new Liner(1), new Route(4), LocalDateTime.MIN, LocalDateTime.MAX, new BigDecimal("123"));
        cruise1.setId(11);
        Cruise cruise2 = new Cruise("Test cruise 2", new Liner(3), new Route(3), LocalDateTime.MIN.plusDays(1), LocalDateTime.MAX.minusDays(1), new BigDecimal("124"));
        cruise2.setId(12);
        Cruise cruise3 = new Cruise("Test cruise 3", new Liner(2), new Route(2), LocalDateTime.MIN.plusWeeks(2), LocalDateTime.MIN.plusWeeks(7), new BigDecimal("125"));
        cruise3.setId(13);
        cruiseList.add(cruise1);
        cruiseList.add(cruise2);
        cruiseList.add(cruise3);
        Mockito.when(mockMySqlCruiseDAO.findAll()).thenReturn(cruiseList);
        Assertions.assertEquals(cruiseList,  cruiseService.findAllCruises());
        Mockito.verify(mockMySqlCruiseDAO, Mockito.times(1)).findAll();
    }

    @Test
    void findAllFail() throws SQLException {
        Mockito.doThrow(new SQLException()).when(mockMySqlCruiseDAO).findAll();
        Assertions.assertThrows(SQLException.class, () ->  cruiseService.findAllCruises());
        Mockito.verify(mockMySqlCruiseDAO, Mockito.times(1)).findAll();
    }

    @Test
    void findCruiseByIdSuccess() throws SQLException, EntityNotFoundException {
        Cruise cruise = new Cruise("Test cruise", new Liner(1), new Route(4), LocalDateTime.MIN, LocalDateTime.MAX, new BigDecimal("123"));
        cruise.setId(11);
        Mockito.when(mockMySqlCruiseDAO.findById(Mockito.anyInt())).thenReturn(cruise);
        Assertions.assertEquals(cruise, cruiseService.findCruiseById(cruise.getId()));
        Mockito.verify(mockMySqlCruiseDAO, Mockito.times(1)).findById(Mockito.anyInt());
    }

    @Test
    void findCruiseByIdFail() throws SQLException, EntityNotFoundException {
        Cruise cruise = new Cruise("Test cruise", new Liner(1), new Route(4), LocalDateTime.MIN, LocalDateTime.MAX, new BigDecimal("123"));
        cruise.setId(11);
        Mockito.doThrow(new SQLException()).when(mockMySqlCruiseDAO).findById(Mockito.anyInt());
        Assertions.assertThrows(SQLException.class, () ->  cruiseService.findCruiseById(cruise.getId()));
        Mockito.verify(mockMySqlCruiseDAO, Mockito.times(1)).findById(Mockito.anyInt());
    }

    @Test
    void modifyCruiseSuccess() throws SQLException, EntityNotUpdatedException {
        Cruise cruise = new Cruise("Test cruise", new Liner(1), new Route(4), LocalDateTime.MIN, LocalDateTime.MAX, new BigDecimal("123"));
        cruise.setId(11);
        cruiseService.modifyCruise(cruise);
        Mockito.verify(mockMySqlCruiseDAO,Mockito.times(1)).update(Mockito.any(Cruise.class));
        Mockito.verify(mockMySqlCruiseDAO,Mockito.times(1)).updateCruiseInfo(Mockito.any(Cruise.class));
    }

    @Test
    void modifyCruiseFail() throws SQLException, EntityNotUpdatedException {
        Cruise cruise = new Cruise("Test cruise", new Liner(1), new Route(4), LocalDateTime.MIN, LocalDateTime.MAX, new BigDecimal("123"));
        cruise.setId(11);
        Mockito.doThrow(new SQLException()).when(mockMySqlCruiseDAO).update(Mockito.any(Cruise.class));
        Assertions.assertThrows(SQLException.class, () -> cruiseService.modifyCruise(cruise));
        Mockito.verify(mockMySqlCruiseDAO,Mockito.times(1)).update(Mockito.any(Cruise.class));
        Mockito.verify(mockMySqlCruiseDAO,Mockito.times(0)).updateCruiseInfo(Mockito.any(Cruise.class));
    }

    @Test
    void deleteCruiseByIdSuccess() throws SQLException, EntityNotDeletedException {
        cruiseService.deleteCruiseById(123321);
        Mockito.verify(mockMySqlCruiseDAO,Mockito.times(1)).deleteById(Mockito.anyInt());
    }

    @Test
    void deleteCruiseByIdFail() throws SQLException {
        Mockito.doThrow(new SQLException()).when(mockMySqlCruiseDAO).deleteById(Mockito.anyInt());
        Assertions.assertThrows(SQLException.class, () -> cruiseService.deleteCruiseById(123321));
        Mockito.verify(mockMySqlCruiseDAO,Mockito.times(1)).deleteById(Mockito.anyInt());
    }

    @Test
    void getStatusDictSuccess() throws SQLException, EntityNotFoundException {
        Map<Integer, String> statusDict = new HashMap<>();
        statusDict.put(1,"qwerty");
        Mockito.when(mockMySqlCruiseDAO.getStatusDict()).thenReturn(statusDict);
        Assertions.assertEquals(statusDict, cruiseService.getStatusDict());
        Mockito.verify(mockMySqlCruiseDAO, Mockito.times(1)).getStatusDict();
    }

    @Test
    void getStatusDictFail() throws SQLException, EntityNotFoundException {
        Mockito.doThrow(new SQLException()).when(mockMySqlCruiseDAO).getStatusDict();
        Assertions.assertThrows(SQLException.class,() -> cruiseService.getStatusDict());
        Mockito.verify(mockMySqlCruiseDAO, Mockito.times(1)).getStatusDict();
    }
}