package com.cruisecompany.service;


import com.cruisecompany.controller.util.EntityNotDeletedException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.dao.DAOFactory;
import com.cruisecompany.dao.impl.MysqlRoutePointDAO;
import com.cruisecompany.entity.RoutePoint;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.sql.SQLException;


@ExtendWith(MockitoExtension.class)
class RoutePointServiceTest {

    @Mock
    DAOFactory mockDaoFactory;

    @Mock
    MysqlRoutePointDAO mockMysqlRoutePointDAO;

    RoutePointService routePointService;

    @BeforeEach
    void setUp(){
        Mockito.when(mockDaoFactory.getRoutePointDAO()).thenReturn(mockMysqlRoutePointDAO);
        routePointService = new RoutePointService(mockDaoFactory);
    }

    @Test
    void saveRoutePointSuccess() throws SQLException, EntityNotInsertedException {
        routePointService.saveRoutePoint(new RoutePoint(12));
        Mockito.verify(mockMysqlRoutePointDAO,Mockito.times(1)).insert(Mockito.any(RoutePoint.class));
    }

    @Test
    void saveRoutePointFail() throws SQLException, EntityNotInsertedException {
        Mockito.doThrow(new SQLException()).when(mockMysqlRoutePointDAO).insert(Mockito.any(RoutePoint.class));
        Assertions.assertThrows(SQLException.class, () ->  routePointService.saveRoutePoint(new RoutePoint(12)));
        Mockito.verify(mockMysqlRoutePointDAO,Mockito.times(1)).insert(Mockito.any(RoutePoint.class));
    }

    @Test
    void modifyRoutePointSuccess() throws SQLException, EntityNotUpdatedException {
        RoutePoint routePoint = new RoutePoint(12, 1, "Port New Zealand (UK)");
        routePointService.modifyRoutePoint(routePoint);
        Mockito.verify(mockMysqlRoutePointDAO, Mockito.times(1)).update(Mockito.any(RoutePoint.class));
    }

    @Test
    void modifyRoutePointFail() throws SQLException, EntityNotUpdatedException {
        RoutePoint routePoint = new RoutePoint(12, 1, "Port New Zealand (UK)");
        Mockito.doThrow(new SQLException()).when(mockMysqlRoutePointDAO).update(Mockito.any(RoutePoint.class));
        Assertions.assertThrows(SQLException.class, () -> routePointService.modifyRoutePoint(routePoint));
        Mockito.verify(mockMysqlRoutePointDAO, Mockito.times(1)).update(Mockito.any(RoutePoint.class));
    }

    @Test
    void deleteRoutePointByIdSuccess() throws SQLException, EntityNotDeletedException {
        routePointService.deleteRoutePointById(12);
        Mockito.verify(mockMysqlRoutePointDAO,Mockito.times(1)).deleteById(Mockito.anyInt());
    }

    @Test
    void deleteRoutePointByIdFail() throws SQLException, EntityNotDeletedException {
        Mockito.doThrow(new SQLException()).when(mockMysqlRoutePointDAO).deleteById(Mockito.anyInt());
        Assertions.assertThrows(SQLException.class, () ->  routePointService.deleteRoutePointById(12));
        Mockito.verify(mockMysqlRoutePointDAO,Mockito.times(1)).deleteById(Mockito.anyInt());
    }
}