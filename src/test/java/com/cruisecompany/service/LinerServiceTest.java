package com.cruisecompany.service;


import com.cruisecompany.controller.util.EntityNotDeletedException;
import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.dao.DAOFactory;
import com.cruisecompany.dao.impl.MysqlLinerDAO;
import com.cruisecompany.entity.Liner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@ExtendWith(MockitoExtension.class)
class LinerServiceTest {

    @Mock
    DAOFactory mockDaoFactory;

    @Mock
    MysqlLinerDAO mockMysqlLinerDAO;

    LinerService linerService;

    @BeforeEach
    void setUp(){
        Mockito.when(mockDaoFactory.getLinerDAO()).thenReturn(mockMysqlLinerDAO);
        linerService = new LinerService(mockDaoFactory);
    }

    @Test
    void findAllSuccess() throws SQLException, EntityNotFoundException {
        List<Liner> linerList = new ArrayList<>();
        Liner liner1 = new Liner(1, "Test liner 1", 5);
        Liner liner2 = new Liner(2, "Test liner 2", 55);
        Liner liner3 = new Liner(3, "Test liner 3", 555);
        linerList.add(liner1);
        linerList.add(liner2);
        linerList.add(liner3);
        Mockito.when(mockMysqlLinerDAO.findAll()).thenReturn(linerList);
        Assertions.assertEquals(linerList,  linerService.findAllLiners());
        Mockito.verify(mockMysqlLinerDAO, Mockito.times(1)).findAll();
    }

    @Test
    void findAllFail() throws SQLException, EntityNotFoundException {
        Mockito.doThrow(new SQLException()).when(mockMysqlLinerDAO).findAll();
        Assertions.assertThrows(SQLException.class, () ->  linerService.findAllLiners());
        Mockito.verify(mockMysqlLinerDAO, Mockito.times(1)).findAll();
    }

    @Test
    void findLinerByIdSuccess() throws SQLException, EntityNotFoundException {
        Liner liner = new Liner(1, "Test liner 1", 5);
        Mockito.when(mockMysqlLinerDAO.findById(Mockito.anyInt())).thenReturn(liner);
        Assertions.assertEquals(liner, linerService.findById(liner.getId()));
        Mockito.verify(mockMysqlLinerDAO, Mockito.times(1)).findById(Mockito.anyInt());
    }

    @Test
    void findLinerByIdFail() throws SQLException, EntityNotFoundException {
        Liner liner = new Liner(1, "Test liner 1", 5);
        Mockito.doThrow(new SQLException()).when(mockMysqlLinerDAO).findById(Mockito.anyInt());
        Assertions.assertThrows(SQLException.class, () ->  linerService.findById(liner.getId()));
        Mockito.verify(mockMysqlLinerDAO, Mockito.times(1)).findById(Mockito.anyInt());
    }

    @Test
    void saveLinerSuccess() throws SQLException, EntityNotInsertedException {
        Liner liner = new Liner(1, "Test liner 1", 5);
        linerService.saveLiner(liner);
        Mockito.verify(mockMysqlLinerDAO,Mockito.times(1)).insert(Mockito.any(Liner.class));
    }

    @Test
    void saveLinerFail() throws SQLException, EntityNotInsertedException {
        Liner liner = new Liner(1, "Test liner 1", 5);
        Mockito.doThrow(new SQLException()).when(mockMysqlLinerDAO).insert(Mockito.any(Liner.class));
        Assertions.assertThrows(SQLException.class, () ->  linerService.saveLiner(liner));
        Mockito.verify(mockMysqlLinerDAO,Mockito.times(1)).insert(Mockito.any(Liner.class));
    }

    @Test
    void modifyLinerSuccess() throws SQLException, EntityNotUpdatedException {
        Liner liner = new Liner(1, "Test liner 1", 5);
        linerService.modifyLiner(liner);
        Mockito.verify(mockMysqlLinerDAO,Mockito.times(1)).update(Mockito.any(Liner.class));
    }

    @Test
    void modifyLinerFail() throws SQLException, EntityNotUpdatedException {
        Liner liner = new Liner(1, "Test liner 1", 5);
        Mockito.doThrow(new SQLException()).when(mockMysqlLinerDAO).update(Mockito.any(Liner.class));
        Assertions.assertThrows(SQLException.class, () -> linerService.modifyLiner(liner));
        Mockito.verify(mockMysqlLinerDAO,Mockito.times(1)).update(Mockito.any(Liner.class));
    }

    @Test
    void deleteLinerByIdSuccess() throws SQLException, EntityNotDeletedException {
        linerService.deleteLinerById(123321);
        Mockito.verify(mockMysqlLinerDAO,Mockito.times(1)).deleteById(Mockito.anyInt());
    }

    @Test
    void deleteLinerByIdFail() throws SQLException, EntityNotDeletedException {
        Mockito.doThrow(new SQLException()).when(mockMysqlLinerDAO).deleteById(Mockito.anyInt());
        Assertions.assertThrows(SQLException.class, () -> linerService.deleteLinerById(123321));
        Mockito.verify(mockMysqlLinerDAO,Mockito.times(1)).deleteById(Mockito.anyInt());
    }
}