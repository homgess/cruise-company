package com.cruisecompany.service;


import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.dao.DAOFactory;
import com.cruisecompany.dao.impl.MysqlUserDAO;
import com.cruisecompany.entity.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    DAOFactory mockDaoFactory;

    @Mock
    MysqlUserDAO mockMysqlUserDAO;

    UserService userService;

    @BeforeEach
    void setUp(){
        Mockito.when(mockDaoFactory.getUserDAO()).thenReturn(mockMysqlUserDAO);
        userService = new UserService(mockDaoFactory);
    }

    @Test
    void findAllSuccess() throws SQLException, EntityNotFoundException {
        List<User> userList = new ArrayList<>();
        User user1 = new User(1, "admin", "admin", 3);
        User user2 = new User(2, "user", "pass123", 2);
        User user3 = new User(3, "qwerty", "qwerty", 2);
        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        Mockito.when(mockMysqlUserDAO.findAll()).thenReturn(userList);
        Assertions.assertEquals(userList,  userService.findAllUsers());
        Mockito.verify(mockMysqlUserDAO, Mockito.times(1)).findAll();
    }

    @Test
    void findAllFail() throws SQLException, EntityNotFoundException {
        Mockito.doThrow(new SQLException()).when(mockMysqlUserDAO).findAll();
        Assertions.assertThrows(SQLException.class, () ->  userService.findAllUsers());
        Mockito.verify(mockMysqlUserDAO, Mockito.times(1)).findAll();
    }

    @Test
    void findUserByIdSuccess() throws SQLException, EntityNotFoundException {
        User user = new User(1, "admin", "admin", 3);
        Mockito.when(mockMysqlUserDAO.findById(Mockito.anyInt())).thenReturn(user);
        Assertions.assertEquals(user, userService.findUserById(user.getId()));
        Mockito.verify(mockMysqlUserDAO, Mockito.times(1)).findById(Mockito.anyInt());
    }

    @Test
    void findUserByIdFail() throws SQLException, EntityNotFoundException {
        User user = new User(1, "admin", "admin", 3);
        Mockito.doThrow(new SQLException()).when(mockMysqlUserDAO).findById(Mockito.anyInt());
        Assertions.assertThrows(SQLException.class, () ->  userService.findUserById(user.getId()));
        Mockito.verify(mockMysqlUserDAO, Mockito.times(1)).findById(Mockito.anyInt());
    }

    @Test
    void findUserByLoginSuccess() throws SQLException, EntityNotFoundException {
        User user = new User(1, "admin", "admin", 3);
        Mockito.when(mockMysqlUserDAO.findByLogin(Mockito.anyString())).thenReturn(user);
        Assertions.assertEquals(user, userService.findUserByLogin(user.getLogin()));
        Mockito.verify(mockMysqlUserDAO, Mockito.times(1)).findByLogin(Mockito.anyString());
    }

    @Test
    void findUserByLoginFail() throws SQLException, EntityNotFoundException {
        User user = new User(1, "admin", "admin", 3);
        Mockito.doThrow(new SQLException()).when(mockMysqlUserDAO).findByLogin(Mockito.anyString());
        Assertions.assertThrows(SQLException.class,() -> userService.findUserByLogin(user.getLogin()));
        Mockito.verify(mockMysqlUserDAO, Mockito.times(1)).findByLogin(Mockito.anyString());
    }

    @Test
    void saveUserSuccess() throws SQLException, EntityNotInsertedException {
        User user = new User(1, "admin", "admin", 3);
        userService.saveUser(user);
        Mockito.verify(mockMysqlUserDAO,Mockito.times(1)).insert(Mockito.any(User.class));
    }

    @Test
    void saveUserFail() throws SQLException, EntityNotInsertedException {
        User user = new User(1, "admin", "admin", 3);
        Mockito.doThrow(new SQLException()).when(mockMysqlUserDAO).insert(Mockito.any(User.class));
        Assertions.assertThrows(SQLException.class, () ->  userService.saveUser(user));
        Mockito.verify(mockMysqlUserDAO,Mockito.times(1)).insert(Mockito.any(User.class));
    }

    @Test
    void modifyUserSuccess() throws SQLException, EntityNotUpdatedException {
        User user = new User(1, "admin", "admin", 3);
        userService.modifyUser(user);
        Mockito.verify(mockMysqlUserDAO,Mockito.times(1)).update(Mockito.any(User.class));
    }

    @Test
    void modifyUserFail() throws SQLException, EntityNotUpdatedException {
        User user = new User(1, "admin", "admin", 3);
        Mockito.doThrow(new SQLException()).when(mockMysqlUserDAO).update(Mockito.any(User.class));
        Assertions.assertThrows(SQLException.class, () ->  userService.modifyUser(user));
        Mockito.verify(mockMysqlUserDAO,Mockito.times(1)).update(Mockito.any(User.class));
    }

    @Test
    void updateLastOnlineDateSuccess() throws SQLException, EntityNotUpdatedException {
        userService.updateLastOnlineDate(1);
        Mockito.verify(mockMysqlUserDAO, Mockito.times(1)).updateLastOnlineDateById(Mockito.anyInt());
    }

    @Test
    void updateLastOnlineDateFail() throws SQLException, EntityNotUpdatedException {
        Mockito.doThrow(new SQLException()).when(mockMysqlUserDAO).updateLastOnlineDateById(Mockito.anyInt());
        Assertions.assertThrows(SQLException.class, () -> userService.updateLastOnlineDate(1));
        Mockito.verify(mockMysqlUserDAO, Mockito.times(1)).updateLastOnlineDateById(Mockito.anyInt());
    }

    @Test
    void getRoleDictSuccess() throws SQLException, EntityNotFoundException {
        Map<Integer, String> roleDict = new HashMap<>();
        roleDict.put(1,"user");
        Mockito.when(mockMysqlUserDAO.getRoleDict()).thenReturn(roleDict);
        Assertions.assertEquals(roleDict, userService.findRoleDict());
        Mockito.verify(mockMysqlUserDAO, Mockito.times(1)).getRoleDict();
    }

    @Test
    void getStatusDictFail() throws SQLException, EntityNotFoundException {
        Mockito.doThrow(new SQLException()).when(mockMysqlUserDAO).getRoleDict();
        Assertions.assertThrows(SQLException.class,() -> userService.findRoleDict());
        Mockito.verify(mockMysqlUserDAO, Mockito.times(1)).getRoleDict();
    }
}