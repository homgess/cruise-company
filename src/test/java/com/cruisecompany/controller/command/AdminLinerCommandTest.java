package com.cruisecompany.controller.command;


import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.entity.Liner;
import com.cruisecompany.service.LinerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@ExtendWith(MockitoExtension.class)
class AdminLinerCommandTest {

    @Mock
    LinerService mockLinerService;

    @Mock
    HttpServletRequest mockRequest;
    @Mock
    HttpServletResponse mockResponse;

    AdminLinerCommand adminLinerCommand;

    @BeforeEach
    void setUp(){
        adminLinerCommand = new AdminLinerCommand(mockLinerService);
    }

    @Test
    void findAllSuccess() throws SQLException, EntityNotFoundException {
        Mockito.when(mockRequest.getParameter("action")).thenReturn("find-all");
        List<Liner> linerList = new ArrayList<>();
        Mockito.when(mockLinerService.findAllLiners()).thenReturn(linerList);
        Assertions.assertEquals("/linerReadAllAdmin.jsp", adminLinerCommand.execute(mockRequest,mockResponse));
        Mockito.verify(mockLinerService,Mockito.times(1)).findAllLiners();
        Mockito.verify(mockRequest,Mockito.times(1)).setAttribute(Mockito.anyString(), Mockito.any());
    }

    @Test
    void findAllFail() throws SQLException, EntityNotFoundException {
        Mockito.when(mockRequest.getParameter("action")).thenReturn("find-all");
        Mockito.doThrow(new SQLException()).when(mockLinerService).findAllLiners();
        Assertions.assertEquals("re:/pages/error", adminLinerCommand.execute(mockRequest,mockResponse));
        Mockito.verify(mockLinerService,Mockito.times(1)).findAllLiners();
        Mockito.verify(mockRequest,Mockito.times(1)).setAttribute(Mockito.anyString(), Mockito.any());
    }

    @Test
    void insertLinerSuccess() throws SQLException, EntityNotInsertedException {
        Mockito.when(mockRequest.getParameter("action")).thenReturn("insert");
        Mockito.when(mockRequest.getParameter("passenger_capacity")).thenReturn("110");
        Mockito.when(mockRequest.getParameter("name")).thenReturn("New test liner 1");
        Assertions.assertEquals("re:/pages/admin-liner?action=find-all", adminLinerCommand.execute(mockRequest,mockResponse));
        Mockito.verify(mockLinerService,Mockito.times(1)).saveLiner(Mockito.any(Liner.class));
        Mockito.verify(mockRequest,Mockito.times(3)).getParameter(Mockito.anyString());
    }

    @Test
    void insertLinerFail() throws SQLException, EntityNotInsertedException {
        Mockito.when(mockRequest.getParameter("action")).thenReturn("insert");
        Mockito.when(mockRequest.getParameter("passenger_capacity")).thenReturn("110");
        Mockito.when(mockRequest.getParameter("name")).thenReturn("New test liner 1");
        Mockito.doThrow(new SQLException()).when(mockLinerService).saveLiner(Mockito.any(Liner.class));
        Assertions.assertEquals("re:/pages/error", adminLinerCommand.execute(mockRequest,mockResponse));
        Mockito.verify(mockLinerService,Mockito.times(1)).saveLiner(Mockito.any(Liner.class));
        Mockito.verify(mockRequest,Mockito.times(3)).getParameter(Mockito.anyString());
    }
}