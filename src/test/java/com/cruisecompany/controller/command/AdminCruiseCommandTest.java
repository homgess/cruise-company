package com.cruisecompany.controller.command;


import com.cruisecompany.controller.util.EntityNotDeletedException;
import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.entity.Cruise;
import com.cruisecompany.entity.Liner;
import com.cruisecompany.entity.Route;
import com.cruisecompany.service.CruiseService;
import com.cruisecompany.service.LinerService;
import com.cruisecompany.service.RouteService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@ExtendWith(MockitoExtension.class)
class AdminCruiseCommandTest {

    @Mock
    CruiseService mockCruiseService;
    @Mock
    LinerService mockLinerService;
    @Mock
    RouteService mockRouteService;

    @Mock
    HttpServletRequest mockRequest;
    @Mock
    HttpServletResponse mockResponse;

    AdminCruiseCommand adminCruiseCommand;

    @BeforeEach
    void setUp(){
        adminCruiseCommand = new AdminCruiseCommand(mockCruiseService, mockLinerService, mockRouteService);
    }

    @Test
    void findAllCruisesSuccess() throws SQLException, EntityNotFoundException {
        Mockito.when(mockRequest.getParameter("action")).thenReturn("find-all");
        List<Cruise> cruiseList = new ArrayList<>();
        Map<Integer, String> statusDict = new HashMap<>();
        Mockito.when(mockCruiseService.findAllCruises()).thenReturn(cruiseList);
        Mockito.when(mockCruiseService.getStatusDict()).thenReturn(statusDict);
        Assertions.assertEquals("/cruiseFindAllAdmin.jsp",  adminCruiseCommand.execute(mockRequest, mockResponse));
        Mockito.verify(mockCruiseService, Mockito.times(1)).findAllCruises();
        Mockito.verify(mockCruiseService, Mockito.times(1)).getStatusDict();
    }

    @Test
    void findAllCruisesFail() throws SQLException, EntityNotFoundException {
        Mockito.when(mockRequest.getParameter("action")).thenReturn("find-all");
        List<Cruise> cruiseList = new ArrayList<>();
        Map<Integer, String> statusDict = new HashMap<>();
        Mockito.doThrow(new SQLException()).when(mockCruiseService).findAllCruises();
        Assertions.assertEquals("re:/pages/error",  adminCruiseCommand.execute(mockRequest, mockResponse));
        Mockito.verify(mockCruiseService, Mockito.times(1)).findAllCruises();
        Mockito.verify(mockCruiseService, Mockito.times(0)).getStatusDict();
    }

    @Test
    void formInsertSuccess() throws SQLException, EntityNotFoundException {
        Mockito.when(mockRequest.getParameter("action")).thenReturn("form-insert");
        List<Liner> linerList = new ArrayList<>();
        List<Route> routeList = new ArrayList<>();
        Mockito.when(mockLinerService.findAllLiners()).thenReturn(linerList);
        Mockito.when(mockRouteService.findAllRoutes()).thenReturn(routeList);
        Assertions.assertEquals("/cruiseInsertForm.jsp",  adminCruiseCommand.execute(mockRequest, mockResponse));
        Mockito.verify(mockLinerService, Mockito.times(1)).findAllLiners();
        Mockito.verify(mockRouteService, Mockito.times(1)).findAllRoutes();
    }

    @Test
    void formInsertFail() throws SQLException, EntityNotFoundException {
        Mockito.when(mockRequest.getParameter("action")).thenReturn("form-insert");
        List<Liner> linerList = new ArrayList<>();
        List<Route> routeList = new ArrayList<>();
        Mockito.doThrow(new SQLException()).when(mockLinerService).findAllLiners();
        Assertions.assertEquals("re:/pages/error",  adminCruiseCommand.execute(mockRequest, mockResponse));
        Mockito.verify(mockLinerService, Mockito.times(1)).findAllLiners();
        Mockito.verify(mockRouteService, Mockito.times(0)).findAllRoutes();
    }

    @Test
    void insertCruiseSuccess() {
        Mockito.when(mockRequest.getParameter("action")).thenReturn("insert");
        Mockito.when(mockRequest.getParameter("liner_id")).thenReturn("1");
        Mockito.when(mockRequest.getParameter("route_id")).thenReturn("1");
        Mockito.when(mockRequest.getParameter("start_datetime")).thenReturn("2022-08-04T10:11:30");
        Mockito.when(mockRequest.getParameter("end_datetime")).thenReturn("2022-09-05T10:11:30");
        Mockito.when(mockRequest.getParameter("price")).thenReturn("135.50");
        Mockito.when(mockRequest.getParameter("name")).thenReturn("Test cruise 1");
        Mockito.when(mockRequest.getParameter("description")).thenReturn("My cruise description");
        Mockito.when(mockRequest.getParameter("image_name")).thenReturn("image.jpg");
        Assertions.assertEquals("re:/pages/admin-cruise?action=find-all", adminCruiseCommand.execute(mockRequest, mockResponse));
        Mockito.verify(mockRequest, Mockito.times(9)).getParameter(Mockito.anyString());
    }

    @Test
    void insertCruiseFail() throws SQLException, EntityNotInsertedException, EntityNotFoundException {
        Mockito.when(mockRequest.getParameter("action")).thenReturn("insert");
        Mockito.when(mockRequest.getParameter("liner_id")).thenReturn("1");
        Mockito.when(mockRequest.getParameter("route_id")).thenReturn("1");
        Mockito.when(mockRequest.getParameter("start_datetime")).thenReturn("2022-08-04T10:11:30");
        Mockito.when(mockRequest.getParameter("end_datetime")).thenReturn("2022-09-05T10:11:30");
        Mockito.when(mockRequest.getParameter("price")).thenReturn("135.50");
        Mockito.when(mockRequest.getParameter("name")).thenReturn("Test cruise 1");
        Mockito.when(mockRequest.getParameter("description")).thenReturn("My cruise description");
        Mockito.when(mockRequest.getParameter("image_name")).thenReturn("image.jpg");
        Mockito.doThrow(new SQLException()).when(mockCruiseService).saveCruise(Mockito.any(Cruise.class));
        Assertions.assertEquals("re:/pages/error", adminCruiseCommand.execute(mockRequest, mockResponse));
        Mockito.verify(mockRequest, Mockito.times(9)).getParameter(Mockito.anyString());
        Mockito.verify(mockRequest, Mockito.times(1)).setAttribute(Mockito.anyString(), Mockito.any());
    }

    @Test
    void formUpdateSuccess() throws SQLException, EntityNotFoundException {
        Mockito.when(mockRequest.getParameter("action")).thenReturn("form-update");
        Mockito.when(mockRequest.getParameter("id")).thenReturn("132");
        Cruise cruise = new Cruise();
        List<Liner> linerList = new ArrayList<>();
        List<Route> routeList = new ArrayList<>();
        Map<Integer, String> statusDict = new HashMap<>();
        Mockito.when(mockCruiseService.findCruiseById(Mockito.anyInt())).thenReturn(cruise);
        Mockito.when(mockLinerService.findAllLiners()).thenReturn(linerList);
        Mockito.when(mockRouteService.findAllRoutes()).thenReturn(routeList);
        Mockito.when(mockCruiseService.getStatusDict()).thenReturn(statusDict);
        Assertions.assertEquals("/cruiseUpdateForm.jsp", adminCruiseCommand.execute(mockRequest, mockResponse));
        Mockito.verify(mockCruiseService, Mockito.times(1)).findCruiseById(Mockito.anyInt());
        Mockito.verify(mockLinerService, Mockito.times(1)).findAllLiners();
        Mockito.verify(mockRouteService, Mockito.times(1)).findAllRoutes();
        Mockito.verify(mockCruiseService, Mockito.times(1)).getStatusDict();
        Mockito.verify(mockRequest, Mockito.times(4)).setAttribute(Mockito.anyString(), Mockito.any());
    }

    @Test
    void formUpdateFail() throws SQLException, EntityNotFoundException {
        Mockito.when(mockRequest.getParameter("action")).thenReturn("form-update");
        Mockito.when(mockRequest.getParameter("id")).thenReturn("132");
        Mockito.doThrow(new SQLException()).when(mockCruiseService).findCruiseById(Mockito.anyInt());
        Assertions.assertEquals("/error.jsp", adminCruiseCommand.execute(mockRequest, mockResponse));
        Mockito.verify(mockCruiseService, Mockito.times(1)).findCruiseById(Mockito.anyInt());
        Mockito.verify(mockLinerService, Mockito.times(0)).findAllLiners();
        Mockito.verify(mockRouteService, Mockito.times(0)).findAllRoutes();
        Mockito.verify(mockCruiseService, Mockito.times(0)).getStatusDict();
        Mockito.verify(mockRequest, Mockito.times(1)).setAttribute(Mockito.anyString(), Mockito.any());
    }

    @Test
    void updateSuccess() throws SQLException, EntityNotUpdatedException {
        Mockito.when(mockRequest.getParameter("action")).thenReturn("update");
        Mockito.when(mockRequest.getParameter("id")).thenReturn("1");
        Mockito.when(mockRequest.getParameter("liner_id")).thenReturn("2");
        Mockito.when(mockRequest.getParameter("route_id")).thenReturn("3");
        Mockito.when(mockRequest.getParameter("status_id")).thenReturn("4");
        Mockito.when(mockRequest.getParameter("start_datetime")).thenReturn("2022-08-04T10:11:30");
        Mockito.when(mockRequest.getParameter("end_datetime")).thenReturn("2022-09-05T10:11:30");
        Mockito.when(mockRequest.getParameter("name")).thenReturn("Test cruise 1");
        Mockito.when(mockRequest.getParameter("price")).thenReturn("135.50");
        Mockito.when(mockRequest.getParameter("description")).thenReturn("My cruise description");
        Mockito.when(mockRequest.getParameter("image_name")).thenReturn("image.jpg");
        Assertions.assertEquals("re:/pages/admin-cruise?action=find-all", adminCruiseCommand.execute(mockRequest, mockResponse));
        Mockito.verify(mockCruiseService, Mockito.times(1)).modifyCruise(Mockito.any(Cruise.class));
        Mockito.verify(mockRequest, Mockito.times(11)).getParameter(Mockito.anyString());
    }

    @Test
    void updateFail() throws SQLException, EntityNotUpdatedException {
        Mockito.when(mockRequest.getParameter("action")).thenReturn("update");
        Mockito.when(mockRequest.getParameter("id")).thenReturn("1");
        Mockito.when(mockRequest.getParameter("liner_id")).thenReturn("2");
        Mockito.when(mockRequest.getParameter("route_id")).thenReturn("3");
        Mockito.when(mockRequest.getParameter("status_id")).thenReturn("4");
        Mockito.when(mockRequest.getParameter("start_datetime")).thenReturn("2022-08-04T10:11:30");
        Mockito.when(mockRequest.getParameter("end_datetime")).thenReturn("2022-09-05T10:11:30");
        Mockito.when(mockRequest.getParameter("name")).thenReturn("Test cruise 1");
        Mockito.when(mockRequest.getParameter("price")).thenReturn("135.50");
        Mockito.when(mockRequest.getParameter("description")).thenReturn("My cruise description");
        Mockito.when(mockRequest.getParameter("image_name")).thenReturn("image.jpg");
        Mockito.doThrow(new SQLException()).when(mockCruiseService).modifyCruise(Mockito.any(Cruise.class));
        Assertions.assertEquals("re:/pages/error", adminCruiseCommand.execute(mockRequest, mockResponse));
        Mockito.verify(mockCruiseService, Mockito.times(1)).modifyCruise(Mockito.any(Cruise.class));
        Mockito.verify(mockRequest, Mockito.times(11)).getParameter(Mockito.anyString());
        Mockito.verify(mockRequest, Mockito.times(1)).setAttribute(Mockito.anyString(), Mockito.any());
    }

    @Test
    void deleteCruiseByIdSuccess() throws SQLException, EntityNotDeletedException {
        Mockito.when(mockRequest.getParameter("action")).thenReturn("delete-by-id");
        Mockito.when(mockRequest.getParameter("id")).thenReturn("1");
        Assertions.assertEquals("re:/pages/admin-cruise?action=find-all", adminCruiseCommand.execute(mockRequest, mockResponse));
        Mockito.verify(mockCruiseService, Mockito.times(1)).deleteCruiseById(Mockito.anyInt());
        Mockito.verify(mockRequest, Mockito.times(2)).getParameter(Mockito.anyString());
    }

    @Test
    void deleteCruiseByIdFail() throws SQLException, EntityNotDeletedException {
        Mockito.when(mockRequest.getParameter("action")).thenReturn("delete-by-id");
        Mockito.when(mockRequest.getParameter("id")).thenReturn("1");
        Mockito.doThrow(new SQLException()).when(mockCruiseService).deleteCruiseById(Mockito.anyInt());
        Assertions.assertEquals("/error.jsp", adminCruiseCommand.execute(mockRequest, mockResponse));
        Mockito.verify(mockCruiseService,Mockito.times(1)).deleteCruiseById(Mockito.anyInt());
        Mockito.verify(mockRequest, Mockito.times(2)).getParameter(Mockito.anyString());
        Mockito.verify(mockRequest, Mockito.times(1)).setAttribute(Mockito.anyString(), Mockito.any());
    }

}