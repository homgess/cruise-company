package com.cruisecompany.dao.impl;


import com.cruisecompany.controller.util.EntityNotDeletedException;
import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.entity.Route;
import com.cruisecompany.entity.RoutePoint;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


@ExtendWith(MockitoExtension.class)
class MysqlRouteDAOTest {

    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPrepStmt;

    @Mock
    Statement mockStmt;

    @Mock
    ResultSet mockResultSet;

    @Test
    void insertSuccess() throws SQLException, EntityNotInsertedException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        MysqlRouteDAO mysqlRouteDAO = new MysqlRouteDAO(mockConn);
        Route route = new Route(10, "New test route 1");
        mysqlRouteDAO.insert(route);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void insertFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        MysqlRouteDAO mysqlRouteDAO = new MysqlRouteDAO(mockConn);
        Route route = new Route(10, "New test route 1");
        Assertions.assertThrows(EntityNotInsertedException.class, () -> mysqlRouteDAO.insert(route));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void updateSuccess() throws SQLException, EntityNotUpdatedException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        MysqlRouteDAO mysqlRouteDAO = new MysqlRouteDAO(mockConn);
        Route route = new Route(10, "New test route 1");
        mysqlRouteDAO.update(route);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void updateFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        MysqlRouteDAO mysqlRouteDAO = new MysqlRouteDAO(mockConn);
        Route route = new Route(10, "New test route 1");
        Assertions.assertThrows(EntityNotUpdatedException.class, () -> mysqlRouteDAO.update(route));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void findByIdSuccess() throws SQLException, EntityNotFoundException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        int routeId = 13;
        String routeName = "My test route 1";
        int[] pointId = {1,2,3};
        String[] pointName = {"Test port 1", "Test port 2", "Test port 3"};
        Route route = new Route(routeId, routeName);
        for (int i = 0; i < pointId.length; i++) {
            RoutePoint routePoint = new RoutePoint(routeId, pointId[i], pointName[i]);
            route.addRoutePointtoList(routePoint);
        }
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(true);
        Mockito.when(mockResultSet.getInt("route_id")).thenReturn(routeId);
        Mockito.when(mockResultSet.getString("route_name")).thenReturn(routeName);
        Mockito.when(mockResultSet.getInt("point_id")).thenReturn(pointId[0]).thenReturn(pointId[1]).thenReturn(pointId[2]);
        Mockito.when(mockResultSet.getString("point_name")).thenReturn(pointName[0]).thenReturn(pointName[1]).thenReturn(pointName[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        MysqlRouteDAO mysqlRouteDAO = new MysqlRouteDAO(mockConn);
        Assertions.assertEquals(route, mysqlRouteDAO.findById(routeId));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findByIdFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        int routeId = 13;
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(false);
        MysqlRouteDAO mysqlRouteDAO = new MysqlRouteDAO(mockConn);
        Assertions.assertThrows(EntityNotFoundException.class, () -> mysqlRouteDAO.findById(routeId));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findAllSuccess() throws SQLException, EntityNotFoundException {
        int[] routeId = {13, 20, 1};
        String[] routeName = {"My test route 13", "My test route 20", "My test route 1"};
        int[][] pointId = {{1,2,3},{4,5,6,7},{8,9}};
        String[][] pointName = {{"Test port 1 route 1", "Test port 2 route 1", "Test port 3 route 1"},{"Test port 4 route 2", "Test port 5 route 2", "Test port 6 route 2","Test port 7 route 2"},{"Test port 8 route 3","Test port 9 route 3"}};
        List<Route> testRouteList = new ArrayList<>();
        for (int i = 0; i < routeId.length; i++) {
            Route route = new Route(routeId[i], routeName[i]);
            for (int j = 0; j < pointId[i].length; j++) {
                RoutePoint routePoint = new RoutePoint(routeId[i], pointId[i][j], pointName[i][j]);
                route.addRoutePointtoList(routePoint);
            }
            testRouteList.add(route);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(true);
        Mockito.when(mockResultSet.getInt("route_id"))
                .thenReturn(routeId[0]).thenReturn(routeId[0]).thenReturn(routeId[0]) .thenReturn(routeId[0]).thenReturn(routeId[0]).thenReturn(routeId[0])
                .thenReturn(routeId[1]).thenReturn(routeId[1]).thenReturn(routeId[1]).thenReturn(routeId[1]).thenReturn(routeId[1]).thenReturn(routeId[1]).thenReturn(routeId[1]).thenReturn(routeId[1])
                .thenReturn(routeId[2]).thenReturn(routeId[2]).thenReturn(routeId[2]).thenReturn(routeId[2]);
        Mockito.when(mockResultSet.getString("route_name"))
                .thenReturn(routeName[0]).thenReturn(routeName[0]).thenReturn(routeName[0])
                .thenReturn(routeName[1]).thenReturn(routeName[1]).thenReturn(routeName[1]).thenReturn(routeName[1])
                .thenReturn(routeName[2]).thenReturn(routeName[2]);
        Mockito.when(mockResultSet.getInt("point_id"))
                .thenReturn(pointId[0][0]).thenReturn(pointId[0][1]).thenReturn(pointId[0][2])
                .thenReturn(pointId[1][0]).thenReturn(pointId[1][1]).thenReturn(pointId[1][2]).thenReturn(pointId[1][3])
                .thenReturn(pointId[2][0]).thenReturn(pointId[2][1]);
        Mockito.when(mockResultSet.getString("point_name"))
                .thenReturn(pointName[0][0]).thenReturn(pointName[0][1]).thenReturn(pointName[0][2])
                .thenReturn(pointName[1][0]).thenReturn(pointName[1][1]).thenReturn(pointName[1][2]).thenReturn(pointName[1][3])
                .thenReturn(pointName[2][0]).thenReturn(pointName[2][1]);
        Mockito.when(mockResultSet.next())
                .thenReturn(true).thenReturn(true).thenReturn(true)
                .thenReturn(true).thenReturn(true).thenReturn(true)
                .thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        MysqlRouteDAO mysqlRouteDAO = new MysqlRouteDAO(mockConn);
        Assertions.assertEquals(testRouteList, mysqlRouteDAO.findAll());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void findAllFail() throws SQLException {
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(false);
        MysqlRouteDAO mysqlRouteDAO = new MysqlRouteDAO(mockConn);
        Assertions.assertThrows(EntityNotFoundException.class, mysqlRouteDAO::findAll);
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void deleteByIdSuccess() throws SQLException, EntityNotDeletedException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        int routeId = 25;
        MysqlRouteDAO mysqlRouteDAO = new MysqlRouteDAO(mockConn);
        mysqlRouteDAO.deleteById(routeId);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }

    @Test
    void deleteByIdFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        int routeId = 25;
        MysqlRouteDAO mysqlRouteDAO = new MysqlRouteDAO(mockConn);
        Assertions.assertThrows(EntityNotDeletedException.class, () ->  mysqlRouteDAO.deleteById(routeId));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }

}