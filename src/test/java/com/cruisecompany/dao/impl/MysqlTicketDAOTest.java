package com.cruisecompany.dao.impl;


import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.entity.Ticket;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@ExtendWith(MockitoExtension.class)
class MysqlTicketDAOTest {

    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPrepStmt;

    @Mock
    Statement mockStmt;

    @Mock
    ResultSet mockResultSet;

    @Test
    void insertSuccess() throws SQLException, EntityNotInsertedException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setBigDecimal(Mockito.anyInt(), Mockito.any(BigDecimal.class));
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        MysqlTicketDAO mysqlTicketDAO = new MysqlTicketDAO(mockConn);
        Ticket ticket = new Ticket(11, 3, 62,1,"null",new BigDecimal("1255.50"));
        mysqlTicketDAO.insert(ticket);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(3)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setBigDecimal(Mockito.anyInt(), Mockito.any(BigDecimal.class));
    }

    @Test
    void insertFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        Mockito.doNothing().when(mockPrepStmt).setBigDecimal(Mockito.anyInt(), Mockito.any(BigDecimal.class));
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        MysqlTicketDAO mysqlTicketDAO = new MysqlTicketDAO(mockConn);
        Ticket ticket = new Ticket(11, 3, 62,1,"null",new BigDecimal("1255.50"));
        Assertions.assertThrows(EntityNotInsertedException.class, () ->  mysqlTicketDAO.insert(ticket));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(3)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setBigDecimal(Mockito.anyInt(), Mockito.any(BigDecimal.class));
    }

    @Test
    void updateSuccess() throws SQLException, EntityNotUpdatedException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.doNothing().when(mockPrepStmt).setBigDecimal(Mockito.anyInt(), Mockito.any(BigDecimal.class));
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        MysqlTicketDAO mysqlTicketDAO = new MysqlTicketDAO(mockConn);
        Ticket ticket = new Ticket(11, 3, 62,1,"null",new BigDecimal("1255.50"));
        mysqlTicketDAO.update(ticket);
        Mockito.verify(mockPrepStmt, Mockito.times(5)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setBigDecimal(Mockito.anyInt(), Mockito.any(BigDecimal.class));
    }

    @Test
    void updateFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.doNothing().when(mockPrepStmt).setBigDecimal(Mockito.anyInt(), Mockito.any(BigDecimal.class));
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        MysqlTicketDAO mysqlTicketDAO = new MysqlTicketDAO(mockConn);
        Ticket ticket = new Ticket(11, 3, 62,1,"null",new BigDecimal("1255.50"));
        Assertions.assertThrows(EntityNotUpdatedException.class, () -> mysqlTicketDAO.update(ticket));
        Mockito.verify(mockPrepStmt, Mockito.times(5)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setBigDecimal(Mockito.anyInt(), Mockito.any(BigDecimal.class));
    }

    @Test
    void findByIdFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        int ticketId = 1024;
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(false);
        MysqlTicketDAO mysqlTicketDAO = new MysqlTicketDAO(mockConn);
        Assertions.assertThrows(EntityNotFoundException.class, () -> mysqlTicketDAO.findById(ticketId));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findByIdSuccess() throws SQLException, EntityNotFoundException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        int ticketId = 1024;
        int cruiseId = 13;
        int userId = 71;
        int position = 3;
        String docsImageName = "img.png";
        BigDecimal price = new BigDecimal("100");
        int statusId = 1;
        Ticket ticket = new Ticket(ticketId, cruiseId, userId, position, docsImageName, price);
        ticket.setStatusId(statusId);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(true);
        Mockito.when(mockResultSet.getInt("ticket_id")).thenReturn(ticketId);
        Mockito.when(mockResultSet.getInt("cruise_id")).thenReturn(cruiseId);
        Mockito.when(mockResultSet.getInt("user_id")).thenReturn(userId);
        Mockito.when(mockResultSet.getInt("position")).thenReturn(position);
        Mockito.when(mockResultSet.getBigDecimal("price")).thenReturn(price);
        Mockito.when(mockResultSet.getInt("status_id")).thenReturn(statusId);
        Mockito.when(mockResultSet.getString("docs_image_name")).thenReturn(docsImageName);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        MysqlTicketDAO mysqlTicketDAO = new MysqlTicketDAO(mockConn);
        Assertions.assertEquals(ticket, mysqlTicketDAO.findById(ticketId));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findAllSuccess() throws SQLException, EntityNotFoundException {
        int[] ticketId = {1024, 1025, 1026};
        int[] cruiseId = {13, 13, 13};
        int[] userId = {71, 10024, 1};
        int[] position = {3,4,5};
        int[] status = {1,2,3};
        String[] docsImageName = {"1.jpg","2.jpg","3.jpg"};
        BigDecimal[] price = {new BigDecimal("100"),new BigDecimal("100"),new BigDecimal("100")};

        List<Ticket> testTicketList = new ArrayList<>();
        for (int i = 0; i < ticketId.length; i++) {
            Ticket ticket = new Ticket(ticketId[i], cruiseId[i], userId[i], position[i], docsImageName[i], price[i]);
            ticket.setStatusId(status[i]);
            testTicketList.add(ticket);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(true);
        Mockito.when(mockResultSet.getInt("ticket_id")).thenReturn(ticketId[0]).thenReturn(ticketId[1]).thenReturn(ticketId[2]);
        Mockito.when(mockResultSet.getInt("cruise_id")).thenReturn(cruiseId[0]).thenReturn(cruiseId[1]).thenReturn(cruiseId[2]);
        Mockito.when(mockResultSet.getInt("user_id")).thenReturn(userId[0]).thenReturn(userId[1]).thenReturn(userId[2]);
        Mockito.when(mockResultSet.getInt("position")).thenReturn(position[0]).thenReturn(position[1]).thenReturn(position[2]);
        Mockito.when(mockResultSet.getString("docs_image_name")).thenReturn(docsImageName[0]).thenReturn(docsImageName[1]).thenReturn(docsImageName[2]);
        Mockito.when(mockResultSet.getBigDecimal("price")).thenReturn(price[0]).thenReturn(price[1]).thenReturn(price[2]);
        Mockito.when(mockResultSet.getInt("status_id")).thenReturn(status[0]).thenReturn(status[1]).thenReturn(status[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        MysqlTicketDAO mysqlTicketDAO = new MysqlTicketDAO(mockConn);
        Assertions.assertEquals(testTicketList, mysqlTicketDAO.findAll());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void findAllFail() throws SQLException {
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(false);
        MysqlTicketDAO mysqlTicketDAO = new MysqlTicketDAO(mockConn);

        Assertions.assertThrows(EntityNotFoundException.class, mysqlTicketDAO::findAll);
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void updateTicketStatusSuccess() throws SQLException, EntityNotUpdatedException {
        int ticketId = 13;
        int statusId = 2;
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        MysqlTicketDAO mysqlTicketDAO = new MysqlTicketDAO(mockConn);
        mysqlTicketDAO.updateTicketStatus(ticketId,statusId);
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void updateTicketStatusFail() throws SQLException {
        int ticketId = 13;
        int statusId = 2;
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        MysqlTicketDAO mysqlTicketDAO = new MysqlTicketDAO(mockConn);
        Assertions.assertThrows(EntityNotUpdatedException.class, () -> mysqlTicketDAO.updateTicketStatus(ticketId,statusId));
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void findAllTicketsByUserIdSuccess() throws SQLException, EntityNotFoundException {
        int[] ticketId = {1024, 1025, 1026};
        int[] cruiseId = {13, 13, 13};
        int[] userId = {71, 10024, 1};
        int[] position = {7,9,10};
        String[] docsImageName = {"1.jpg","2.jpg","3.jpg"};
        BigDecimal[] price = {new BigDecimal("100"),new BigDecimal("100"),new BigDecimal("100")};
        int[] statusId = {3,2,1};
        List<Ticket> testTicketList = new ArrayList<>();
        for (int i = 0; i < ticketId.length; i++) {
            Ticket ticket = new Ticket(ticketId[i], cruiseId[i], userId[i], position[i], docsImageName[i], price[i]);
            ticket.setStatusId(statusId[i]);
            testTicketList.add(ticket);
        }
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(true);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        Mockito.when(mockResultSet.getInt("ticket_id")).thenReturn(ticketId[0]).thenReturn(ticketId[1]).thenReturn(ticketId[2]);
        Mockito.when(mockResultSet.getInt("cruise_id")).thenReturn(cruiseId[0]).thenReturn(cruiseId[1]).thenReturn(cruiseId[2]);
        Mockito.when(mockResultSet.getInt("user_id")).thenReturn(userId[0]).thenReturn(userId[1]).thenReturn(userId[2]);
        Mockito.when(mockResultSet.getInt("position")).thenReturn(position[0]).thenReturn(position[1]).thenReturn(position[2]);
        Mockito.when(mockResultSet.getString("docs_image_name")).thenReturn(docsImageName[0]).thenReturn(docsImageName[1]).thenReturn(docsImageName[2]);
        Mockito.when(mockResultSet.getBigDecimal("price")).thenReturn(price[0]).thenReturn(price[1]).thenReturn(price[2]);
        Mockito.when(mockResultSet.getInt("status_id")).thenReturn(statusId[0]).thenReturn(statusId[1]).thenReturn(statusId[2]);
        MysqlTicketDAO mysqlTicketDAO = new MysqlTicketDAO(mockConn);
        Assertions.assertEquals(testTicketList, mysqlTicketDAO.findTicketByUserId(515));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findAllTicketsByUserIdFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(false);
        MysqlTicketDAO mysqlTicketDAO = new MysqlTicketDAO(mockConn);
        Assertions.assertThrows(EntityNotFoundException.class, () -> mysqlTicketDAO.findTicketByUserId(515));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void getStatusDictSuccess() throws SQLException, EntityNotFoundException {
        int[] statusId = {1,2,3};
        String[] statusName = {"Status1", "Status2", "Status3"};
        Map<Integer, String> testDictMap = new HashMap<>();
        for (int i = 0; i < statusId.length; i++) {
            testDictMap.put(statusId[i],statusName[i]);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(true);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        Mockito.when(mockResultSet.getInt("status_id")).thenReturn(statusId[0]).thenReturn(statusId[1]).thenReturn(statusId[2]);
        Mockito.when(mockResultSet.getString("status_name")).thenReturn(statusName[0]).thenReturn(statusName[1]).thenReturn(statusName[2]);
        MysqlTicketDAO mysqlTicketDAO = new MysqlTicketDAO(mockConn);
        Assertions.assertEquals(testDictMap, mysqlTicketDAO.getStatusDict());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void getStatusDictFail() throws SQLException {
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(false);
        MysqlTicketDAO mysqlTicketDAO = new MysqlTicketDAO(mockConn);
        Assertions.assertThrows(EntityNotFoundException.class, mysqlTicketDAO::getStatusDict);
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

}