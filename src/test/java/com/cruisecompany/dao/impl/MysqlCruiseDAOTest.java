package com.cruisecompany.dao.impl;


import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.entity.Cruise;
import com.cruisecompany.entity.Liner;
import com.cruisecompany.entity.Route;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@ExtendWith(MockitoExtension.class)
class MysqlCruiseDAOTest {

    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPrepStmt;

    @Mock
    Statement mockStmt;

    @Mock
    ResultSet mockResultSet;


    @Test
    void insertSuccess() throws SQLException, EntityNotInsertedException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setBigDecimal(Mockito.anyInt(), Mockito.any(BigDecimal.class));
        MysqlCruiseDAO mysqlCruiseDAO = new MysqlCruiseDAO(mockConn);
        Cruise cruise = new Cruise("My test cruise", new Liner(3), new Route(2), LocalDateTime.MIN, LocalDateTime.MAX, new BigDecimal("1350.99"));
        mysqlCruiseDAO.insert(cruise);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(3)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setBigDecimal(Mockito.anyInt(), Mockito.any(BigDecimal.class));
    }

    @Test
    void insertFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setBigDecimal(Mockito.anyInt(), Mockito.any(BigDecimal.class));
        MysqlCruiseDAO mysqlCruiseDAO = new MysqlCruiseDAO(mockConn);
        Cruise cruise = new Cruise("My test cruise", new Liner(3), new Route(2), LocalDateTime.MIN, LocalDateTime.MAX, new BigDecimal("1350.99"));
        Assertions.assertThrows(EntityNotInsertedException.class, () -> mysqlCruiseDAO.insert(cruise));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(3)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setBigDecimal(Mockito.anyInt(), Mockito.any(BigDecimal.class));
    }

    @Test
    void insertCruiseInfoSuccess() throws SQLException, EntityNotInsertedException {
        int cruiseId = 1;
        String description = "MY CRUISE DESCRIPTION";
        String imageName = "img.jpg";
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        MysqlCruiseDAO mysqlCruiseDAO = new MysqlCruiseDAO(mockConn);
        Cruise cruise = new Cruise(cruiseId);
        cruise.setDescription(description);
        cruise.setImageName(imageName);
        mysqlCruiseDAO.insertCruiseInfo(cruise);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void insertCruiseInfoFail() throws SQLException {
        int cruiseId = 1;
        String description = "MY CRUISE DESCRIPTION";
        String imageName = "img.jpg";
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        MysqlCruiseDAO mysqlCruiseDAO = new MysqlCruiseDAO(mockConn);
        Cruise cruise = new Cruise(cruiseId);
        cruise.setDescription(description);
        cruise.setImageName(imageName);
        Assertions.assertThrows(EntityNotInsertedException.class, () -> mysqlCruiseDAO.insertCruiseInfo(cruise));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void updateSuccess() throws SQLException, EntityNotUpdatedException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setBigDecimal(Mockito.anyInt(), Mockito.any(BigDecimal.class));
        MysqlCruiseDAO mysqlCruiseDAO = new MysqlCruiseDAO(mockConn);
        Cruise cruise = new Cruise("My new test cruise", new Liner(2), new Route(3), LocalDateTime.MIN, LocalDateTime.MAX, new BigDecimal("1355.99"));
        cruise.setId(1);
        mysqlCruiseDAO.update(cruise);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(3)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(3)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setBigDecimal(Mockito.anyInt(), Mockito.any(BigDecimal.class));
    }

    @Test
    void updateFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setBigDecimal(Mockito.anyInt(), Mockito.any(BigDecimal.class));
        MysqlCruiseDAO mysqlCruiseDAO = new MysqlCruiseDAO(mockConn);
        Cruise cruise = new Cruise("My new test cruise", new Liner(2), new Route(3), LocalDateTime.MIN, LocalDateTime.MAX, new BigDecimal("1355.99"));
        cruise.setId(1);
        Assertions.assertThrows(EntityNotUpdatedException.class, () -> mysqlCruiseDAO.update(cruise));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(3)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(3)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setBigDecimal(Mockito.anyInt(), Mockito.any(BigDecimal.class));
    }

    @Test
    void updateCruiseInfoSuccess() throws SQLException, EntityNotUpdatedException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        MysqlCruiseDAO mysqlCruiseDAO = new MysqlCruiseDAO(mockConn);
        Cruise cruise = new Cruise("My new test cruise", new Liner(2), new Route(3), LocalDateTime.MIN, LocalDateTime.MAX, new BigDecimal("1355.99"));
        cruise.setId(1);
        cruise.setDescription("MY COOL CRUISE DESCRIPTION!!!");
        cruise.setStatusId(5);
        cruise.setImageName("image.jpg");
        mysqlCruiseDAO.updateCruiseInfo(cruise);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void updateCruiseInfoFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        MysqlCruiseDAO mysqlCruiseDAO = new MysqlCruiseDAO(mockConn);
        Cruise cruise = new Cruise("My new test cruise", new Liner(2), new Route(3), LocalDateTime.MIN, LocalDateTime.MAX, new BigDecimal("1355.99"));
        cruise.setId(1);
        cruise.setDescription("MY COOL CRUISE DESCRIPTION!!!");
        cruise.setStatusId(5);
        cruise.setImageName("image.jpg");
        Assertions.assertThrows(EntityNotUpdatedException.class, () -> mysqlCruiseDAO.updateCruiseInfo(cruise));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void findByIdSuccess() throws SQLException, EntityNotFoundException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        int cruiseId = 115693;
        String cruiseName = "My test cruise 1";
        int linerId = 115;
        String linerName = "My test liner 1";
        int passengerCapacity = 125;
        int routeId = 41;
        String routeName = "My test route 1";
        LocalDateTime startTime = LocalDateTime.of(2020, 1, 1, 10, 10, 0);
        LocalDateTime endTime = LocalDateTime.of(2020, 1, 13, 23, 59, 0);
        BigDecimal price = new BigDecimal("10035.35");
        Cruise testCruise = new Cruise(cruiseName, new Liner(linerId, linerName, passengerCapacity), new Route(routeId, routeName), startTime, endTime, price);
        testCruise.setId(cruiseId);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(true);
        Mockito.when(mockResultSet.getInt("cruise_id")).thenReturn(cruiseId);
        Mockito.when(mockResultSet.getString("cruise_name")).thenReturn(cruiseName);
        Mockito.when(mockResultSet.getInt("liner_id")).thenReturn(linerId);
        Mockito.when(mockResultSet.getString("liner_name")).thenReturn(linerName);
        Mockito.when(mockResultSet.getInt("passenger_capacity")).thenReturn(passengerCapacity);
        Mockito.when(mockResultSet.getInt("route_id")).thenReturn(routeId);
        Mockito.when(mockResultSet.getString("route_name")).thenReturn(routeName);
        Mockito.when(mockResultSet.getObject("start_datetime")).thenReturn(startTime);
        Mockito.when(mockResultSet.getObject("end_datetime")).thenReturn(endTime);
        Mockito.when(mockResultSet.getBigDecimal("price")).thenReturn(price);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        MysqlCruiseDAO mysqlCruiseDAO = new MysqlCruiseDAO(mockConn);
        Assertions.assertEquals(testCruise, mysqlCruiseDAO.findById(cruiseId));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findByIdFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        int cruiseId = 12315;
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(false);
        MysqlCruiseDAO mysqlCruiseDAO = new MysqlCruiseDAO(mockConn);
        Assertions.assertThrows(EntityNotFoundException.class, () -> mysqlCruiseDAO.findById(cruiseId));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findByNameSuccess() throws SQLException, EntityNotFoundException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        int cruiseId = 115693;
        String cruiseName = "My test cruise 1";
        int linerId = 115;
        String linerName = "My test liner 1";
        int passengerCapacity = 125;
        int routeId = 41;
        String routeName = "My test route 1";
        LocalDateTime startTime = LocalDateTime.of(2020, 1, 1, 10, 10, 0);
        LocalDateTime endTime = LocalDateTime.of(2020, 1, 13, 23, 59, 0);
        BigDecimal price = new BigDecimal("10035.35");
        Cruise testCruise = new Cruise(cruiseName, new Liner(linerId, linerName, passengerCapacity), new Route(routeId, routeName), startTime, endTime, price);
        testCruise.setId(cruiseId);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(true);
        Mockito.when(mockResultSet.getInt("cruise_id")).thenReturn(cruiseId);
        Mockito.when(mockResultSet.getString("cruise_name")).thenReturn(cruiseName);
        Mockito.when(mockResultSet.getInt("liner_id")).thenReturn(linerId);
        Mockito.when(mockResultSet.getString("liner_name")).thenReturn(linerName);
        Mockito.when(mockResultSet.getInt("passenger_capacity")).thenReturn(passengerCapacity);
        Mockito.when(mockResultSet.getInt("route_id")).thenReturn(routeId);
        Mockito.when(mockResultSet.getString("route_name")).thenReturn(routeName);
        Mockito.when(mockResultSet.getObject("start_datetime")).thenReturn(startTime);
        Mockito.when(mockResultSet.getObject("end_datetime")).thenReturn(endTime);
        Mockito.when(mockResultSet.getBigDecimal("price")).thenReturn(price);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        MysqlCruiseDAO mysqlCruiseDAO = new MysqlCruiseDAO(mockConn);
        Assertions.assertEquals(testCruise, mysqlCruiseDAO.findByName(cruiseName));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findByNameFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        String cruiseName = "New test cruise 1";
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(false);
        MysqlCruiseDAO mysqlCruiseDAO = new MysqlCruiseDAO(mockConn);
        Assertions.assertThrows(EntityNotFoundException.class, () -> mysqlCruiseDAO.findByName(cruiseName));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void getStatusDictSuccess() throws SQLException, EntityNotFoundException {
        int[] statusId = {1,2,3};
        String[] statusName = {"Status1", "Status2", "Status3"};
        Map<Integer, String> testDictMap = new HashMap<>();
        for (int i = 0; i < statusId.length; i++) {
            testDictMap.put(statusId[i],statusName[i]);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(true);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        Mockito.when(mockResultSet.getInt("status_id")).thenReturn(statusId[0]).thenReturn(statusId[1]).thenReturn(statusId[2]);
        Mockito.when(mockResultSet.getString("status_name")).thenReturn(statusName[0]).thenReturn(statusName[1]).thenReturn(statusName[2]);
        MysqlCruiseDAO mysqlCruiseDAO = new MysqlCruiseDAO(mockConn);
        Assertions.assertEquals(testDictMap, mysqlCruiseDAO.getStatusDict());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void getStatusDictFail() throws SQLException {
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(false);
        MysqlCruiseDAO mysqlCruiseDAO = new MysqlCruiseDAO(mockConn);

        Assertions.assertThrows(EntityNotFoundException.class, mysqlCruiseDAO::getStatusDict);
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void findAllSuccess() throws SQLException {
        int[] cruiseId = {115693, 1238, 124783};
        String[] cruiseName = {"My test cruise 1", "My test cruise 2", "My test cruise 3"};
        String[] linerName = {"My test liner 1", "My test liner 2", "My test liner 3"};
        int[] passengerCapacity = {125, 125, 130};
        int[] pointCount = {3,4,5};
        int[] statusId = {2,2,5};
        String[] imageName = {"1.jpg","2.jpg","3.jpg"};
        String[] routeName = {"My test route 1", "My test route 2", "My test route 3"};
        LocalDateTime[] startTime = {LocalDateTime.of(2020, 1, 1, 10, 10, 0), LocalDateTime.of(2021, 2, 3, 23, 55, 0), LocalDateTime.of(2023, 2, 1, 15, 0, 0)};
        LocalDateTime[] endTime = {LocalDateTime.of(2020, 1, 13, 23, 59, 0), LocalDateTime.of(2021, 5, 30, 1, 13, 0), LocalDateTime.of(2023, 2, 2, 14, 5, 0)};
        BigDecimal[] price = {new BigDecimal("10035.35"), new BigDecimal("19533"), new BigDecimal("1003.3")};
        List<Cruise> testCruiseList = new ArrayList<>();
        for (int i = 0; i < cruiseId.length; i++) {
            Liner liner = new Liner(0, linerName[i], passengerCapacity[i]);
            Route route = new Route(0, routeName[i]);
            Cruise cruise = new Cruise(cruiseName[i], liner, route, startTime[i], endTime[i], price[i]);
            cruise.setId(cruiseId[i]);
            testCruiseList.add(cruise);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(true);
        Mockito.when(mockResultSet.getInt("cruise_id")).thenReturn(cruiseId[0]).thenReturn(cruiseId[1]).thenReturn(cruiseId[2]);
        Mockito.when(mockResultSet.getString("cruise_name")).thenReturn(cruiseName[0]).thenReturn(cruiseName[1]).thenReturn(cruiseName[2]);
        Mockito.when(mockResultSet.getString("liner_name")).thenReturn(linerName[0]).thenReturn(linerName[1]).thenReturn(linerName[2]);
        Mockito.when(mockResultSet.getInt("passenger_capacity")).thenReturn(passengerCapacity[0]).thenReturn(passengerCapacity[1]).thenReturn(passengerCapacity[2]);
        Mockito.when(mockResultSet.getString("route_name")).thenReturn(routeName[0]).thenReturn(routeName[1]).thenReturn(routeName[2]);
        Mockito.when(mockResultSet.getInt("point_count")).thenReturn(pointCount[0]).thenReturn(pointCount[1]).thenReturn(pointCount[2]);
        Mockito.when(mockResultSet.getObject("start_datetime")).thenReturn(startTime[0]).thenReturn(startTime[1]).thenReturn(startTime[2]);
        Mockito.when(mockResultSet.getObject("end_datetime")).thenReturn(endTime[0]).thenReturn(endTime[1]).thenReturn(endTime[2]);
        Mockito.when(mockResultSet.getBigDecimal("price")).thenReturn(price[0]).thenReturn(price[1]).thenReturn(price[2]);
        Mockito.when(mockResultSet.getInt("status_id")).thenReturn(statusId[0]).thenReturn(statusId[1]).thenReturn(statusId[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        Mockito.when(mockResultSet.getString("image_name")).thenReturn(imageName[0]).thenReturn(imageName[1]).thenReturn(imageName[2]);
        MysqlCruiseDAO mysqlCruiseDAO = new MysqlCruiseDAO(mockConn);
        Assertions.assertEquals(testCruiseList, mysqlCruiseDAO.findAll());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void findAllFail() throws SQLException {
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(false);
        MysqlCruiseDAO mysqlCruiseDAO = new MysqlCruiseDAO(mockConn);
        Assertions.assertThrows(SQLException.class, mysqlCruiseDAO::findAll);
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void deleteByIdSuccess() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        int cruiseId = 115693;
        MysqlCruiseDAO mysqlCruiseDAO = new MysqlCruiseDAO(mockConn);
        mysqlCruiseDAO.deleteById(cruiseId);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }

    @Test
    void deleteByIdFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        int cruiseId = 115693;
        MysqlCruiseDAO mysqlCruiseDAO = new MysqlCruiseDAO(mockConn);
        Assertions.assertThrows(SQLException.class, () ->  mysqlCruiseDAO.deleteById(cruiseId));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }

}