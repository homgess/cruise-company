package com.cruisecompany.dao.impl;


import com.cruisecompany.controller.util.EntityNotDeletedException;
import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.entity.RoutePoint;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


@ExtendWith(MockitoExtension.class)
class MysqlRoutePointDAOTest {

    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPrepStmt;

    @Mock
    ResultSet mockResultSet;

    @Test
    void insertSuccess() throws SQLException, EntityNotInsertedException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        MysqlRoutePointDAO mysqlRoutePointDAO = new MysqlRoutePointDAO(mockConn);
        RoutePoint routePoint = new RoutePoint(11, 1, "Test port 1 to route 11");
        mysqlRoutePointDAO.insert(routePoint);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void insertFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        MysqlRoutePointDAO mysqlRoutePointDAO = new MysqlRoutePointDAO(mockConn);
        RoutePoint routePoint = new RoutePoint(11, 2, "Test port 2 to route 11");
        Assertions.assertThrows(EntityNotInsertedException.class, () -> mysqlRoutePointDAO.insert(routePoint));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void updateSuccess() throws SQLException, EntityNotUpdatedException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        MysqlRoutePointDAO mysqlRoutePointDAO = new MysqlRoutePointDAO(mockConn);
        RoutePoint routePoint;
        routePoint = new RoutePoint(11, 3, "Test port 3 to route 11");
        mysqlRoutePointDAO.update(routePoint);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void updateFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        MysqlRoutePointDAO mysqlRoutePointDAO = new MysqlRoutePointDAO(mockConn);
        RoutePoint routePoint = new RoutePoint(11, 4, "Test port 4 to route 11");
        Assertions.assertThrows(EntityNotUpdatedException.class, () -> mysqlRoutePointDAO.update(routePoint));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void findByRouteIdSuccess() throws SQLException, EntityNotFoundException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        int routeId = 10;
        int[] pointId = {1, 2, 3};
        String[] pointName = {"Test port 1 to route 10", "Test port 2 to route 10", "Test port 3 to route 10"};
        List<RoutePoint> testRoutePointList = new ArrayList<>();
        for (int i = 0; i < pointId.length; i++)
            testRoutePointList.add(new RoutePoint(pointId[i], routeId, pointName[i]));
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(true);
        Mockito.when(mockResultSet.getInt("route_id")).thenReturn(routeId);
        Mockito.when(mockResultSet.getInt("point_id")).thenReturn(pointId[0]).thenReturn(pointId[1]).thenReturn(pointId[2]);
        Mockito.when(mockResultSet.getString("point_name")).thenReturn(pointName[0]).thenReturn(pointName[1]).thenReturn(pointName[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        MysqlRoutePointDAO mysqlRoutePointDAO = new MysqlRoutePointDAO(mockConn);
        Assertions.assertEquals(testRoutePointList, mysqlRoutePointDAO.findByRouteId(routeId));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findByRouteIdFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        int routeId = 10;
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(false);
        MysqlRoutePointDAO mysqlRoutePointDAO = new MysqlRoutePointDAO(mockConn);
        Assertions.assertThrows(EntityNotFoundException.class, () -> mysqlRoutePointDAO.findByRouteId(routeId));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void deleteByIdSuccess() throws SQLException, EntityNotDeletedException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        int pointId = 25;
        MysqlRoutePointDAO mysqlRoutePointDAO = new MysqlRoutePointDAO(mockConn);
        mysqlRoutePointDAO.deleteById(pointId);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }

    @Test
    void deleteByIdFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        int pointId = 25;
        MysqlRoutePointDAO mysqlRoutePointDAO = new MysqlRoutePointDAO(mockConn);
        Assertions.assertThrows(EntityNotDeletedException.class, () -> mysqlRoutePointDAO.deleteById(pointId));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }
}