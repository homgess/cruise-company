package com.cruisecompany.dao.impl;


import com.cruisecompany.controller.util.EntityNotDeletedException;
import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.entity.Liner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


@ExtendWith(MockitoExtension.class)
class MysqlLinerDAOTest {

    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPrepStmt;

    @Mock
    Statement mockStmt;

    @Mock
    ResultSet mockResultSet;

    @Test
    void insertSuccess() throws SQLException, EntityNotInsertedException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        MysqlLinerDAO mysqlLinerDAO = new MysqlLinerDAO(mockConn);
        Liner liner = new Liner(121, "New test liner 1", 255);
        mysqlLinerDAO.insert(liner);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void insertFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        MysqlLinerDAO mysqlLinerDAO = new MysqlLinerDAO(mockConn);
        Liner liner = new Liner(121, "New test liner 1", 255);
        Assertions.assertThrows(EntityNotInsertedException.class, () -> mysqlLinerDAO.insert(liner));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void updateSuccess() throws SQLException, EntityNotUpdatedException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        MysqlLinerDAO mysqlLinerDAO = new MysqlLinerDAO(mockConn);
        Liner liner = new Liner(121, "New test liner 1", 255);
        mysqlLinerDAO.update(liner);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void updateFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        MysqlLinerDAO mysqlLinerDAO = new MysqlLinerDAO(mockConn);
        Liner liner = new Liner(121, "New test liner 1", 255);
        Assertions.assertThrows(EntityNotUpdatedException.class, () -> mysqlLinerDAO.update(liner));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void findByIdSuccess() throws SQLException, EntityNotFoundException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        int linerId = 115;
        String linerName = "My test liner 1";
        int passengerCapacity = 125;
        Liner testLiner = new Liner(linerId, linerName, passengerCapacity);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(true);
        Mockito.when(mockResultSet.getInt("liner_id")).thenReturn(linerId);
        Mockito.when(mockResultSet.getString("liner_name")).thenReturn(linerName);
        Mockito.when(mockResultSet.getInt("passenger_capacity")).thenReturn(passengerCapacity);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        MysqlLinerDAO mysqlLinerDAO = new MysqlLinerDAO(mockConn);
        Assertions.assertEquals(testLiner, mysqlLinerDAO.findById(linerId));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findByIdFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        int linerId = 115;
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(false);
        MysqlLinerDAO mysqlLinerDAO = new MysqlLinerDAO(mockConn);
        Assertions.assertThrows(EntityNotFoundException.class, () -> mysqlLinerDAO.findById(linerId));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findAllSuccess() throws SQLException, EntityNotFoundException {
        int[] linerId = {11, 22, 33};
        String[] linerName = {"My test liner 1", "My test liner 2", "My test liner 3"};
        int[] passengerCapacity = {225, 250 , 200};
        List<Liner> testLinerList = new ArrayList<>();
        for (int i = 0; i < linerId.length; i++) {
            Liner liner = new Liner(linerId[i], linerName[i], passengerCapacity[i]);
            testLinerList.add(liner);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(true);
        Mockito.when(mockResultSet.getInt("liner_id")).thenReturn(linerId[0]).thenReturn(linerId[1]).thenReturn(linerId[2]);
        Mockito.when(mockResultSet.getString("liner_name")).thenReturn(linerName[0]).thenReturn(linerName[1]).thenReturn(linerName[2]);
        Mockito.when(mockResultSet.getInt("passenger_capacity")).thenReturn(passengerCapacity[0]).thenReturn(passengerCapacity[1]).thenReturn(passengerCapacity[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);

        MysqlLinerDAO mysqlLinerDAO = new MysqlLinerDAO(mockConn);
        Assertions.assertEquals(testLinerList, mysqlLinerDAO.findAll());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void findAllFail() throws SQLException {
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(false);

        MysqlLinerDAO mysqlLinerDAO = new MysqlLinerDAO(mockConn);
        Assertions.assertThrows(EntityNotFoundException.class, mysqlLinerDAO::findAll);
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void deleteByIdSuccess() throws SQLException, EntityNotDeletedException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        int linerId = 115693;
        MysqlLinerDAO mysqlLinerDAO = new MysqlLinerDAO(mockConn);
        mysqlLinerDAO.deleteById(linerId);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }

    @Test
    void deleteByIdFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        int linerId = 115693;
        MysqlLinerDAO mysqlLinerDAO = new MysqlLinerDAO(mockConn);
        Assertions.assertThrows(EntityNotDeletedException.class, () -> mysqlLinerDAO.deleteById(linerId));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }
}