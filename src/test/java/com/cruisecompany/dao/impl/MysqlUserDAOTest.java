package com.cruisecompany.dao.impl;


import com.cruisecompany.controller.util.EntityNotDeletedException;
import com.cruisecompany.controller.util.EntityNotFoundException;
import com.cruisecompany.controller.util.EntityNotInsertedException;
import com.cruisecompany.controller.util.EntityNotUpdatedException;
import com.cruisecompany.entity.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@ExtendWith(MockitoExtension.class)
class MysqlUserDAOTest {

    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPrepStmt;

    @Mock
    Statement mockStmt;

    @Mock
    ResultSet mockResultSet;

    @Test
    void insertSuccess() throws SQLException, EntityNotInsertedException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        MysqlUserDAO mysqlUserDAO = new MysqlUserDAO(mockConn);
        User user = new User(0, "login1", "password2", 0);
        mysqlUserDAO.insert(user);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void insertFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        MysqlUserDAO mysqlUserDAO = new MysqlUserDAO(mockConn);
        User user = new User(0, "login1", "password2", 0);
        Assertions.assertThrows(EntityNotInsertedException.class, () -> mysqlUserDAO.insert(user));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setString(Mockito.anyInt(), Mockito.anyString());
    }

    @Test
    void updateSuccess() throws SQLException, EntityNotUpdatedException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        MysqlUserDAO mysqlUserDAO = new MysqlUserDAO(mockConn);
        User user = new User(235, "login1", "password2", 2);
        mysqlUserDAO.update(user);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void updateFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        MysqlUserDAO mysqlUserDAO = new MysqlUserDAO(mockConn);
        User user = new User(235, "login1", "password2", 2);
        Assertions.assertThrows(EntityNotUpdatedException.class, () ->  mysqlUserDAO.update(user));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void findByIdSuccess() throws SQLException, EntityNotFoundException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        int userId = 103;
        String login = "User103";
        String password = "Pass103";
        int roleId = 2;
        User user = new User(userId, login, password, roleId);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(true);
        Mockito.when(mockResultSet.getInt("user_id")).thenReturn(userId);
        Mockito.when(mockResultSet.getString("user_login")).thenReturn(login);
        Mockito.when(mockResultSet.getString("user_password")).thenReturn(password);
        Mockito.when(mockResultSet.getInt("role_id")).thenReturn(roleId);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        MysqlUserDAO mysqlUserDAO = new MysqlUserDAO(mockConn);
        Assertions.assertEquals(user, mysqlUserDAO.findById(userId));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findByIdFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        int userId = 103;
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(false);
        MysqlUserDAO mysqlUserDAO = new MysqlUserDAO(mockConn);
        Assertions.assertThrows(EntityNotFoundException.class, () -> mysqlUserDAO.findById(userId));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findByLoginSuccess() throws SQLException, EntityNotFoundException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        int userId = 103;
        String login = "User103";
        String password = "Pass103";
        int roleId = 2;
        User user = new User(userId, login, password, roleId);
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(true);
        Mockito.when(mockResultSet.getInt("user_id")).thenReturn(userId);
        Mockito.when(mockResultSet.getString("user_login")).thenReturn(login);
        Mockito.when(mockResultSet.getString("user_password")).thenReturn(password);
        Mockito.when(mockResultSet.getInt("role_id")).thenReturn(roleId);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(false);
        MysqlUserDAO mysqlUserDAO = new MysqlUserDAO(mockConn);
        Assertions.assertEquals(user, mysqlUserDAO.findByLogin(login));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findByLoginFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        String login = "qwertyZ";
        Mockito.when(mockPrepStmt.executeQuery()).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(false);
        MysqlUserDAO mysqlUserDAO = new MysqlUserDAO(mockConn);
        Assertions.assertThrows(EntityNotFoundException.class, () -> mysqlUserDAO.findByLogin(login));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeQuery();
    }

    @Test
    void findAllSuccess() throws SQLException, EntityNotFoundException {
        int[] userId = {103, 2423, 35864};
        String[] login = {"User103", "NeWOLLA", "Affirmat22ua"};
        String[] password = {"Pass103", "qwerty123", "Affirmat22ua"};
        int[] roleId = {2,3,2};
        List<User> testUserList = new ArrayList<>();
        for (int i = 0; i < userId.length; i++) {
            User user = new User(userId[i], login[i], password[i], roleId[i]);
            testUserList.add(user);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(true);
        Mockito.when(mockResultSet.getInt("user_id")).thenReturn(userId[0]).thenReturn(userId[1]).thenReturn(userId[2]);
        Mockito.when(mockResultSet.getString("user_login")).thenReturn(login[0]).thenReturn(login[1]).thenReturn(login[2]);
        Mockito.when(mockResultSet.getString("user_password")).thenReturn(password[0]).thenReturn(password[1]).thenReturn(password[2]);
        Mockito.when(mockResultSet.getInt("role_id")).thenReturn(roleId[0]).thenReturn(roleId[1]).thenReturn(roleId[2]);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);

        MysqlUserDAO mysqlUserDAO = new MysqlUserDAO(mockConn);
        Assertions.assertEquals(testUserList, mysqlUserDAO.findAll());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void findAllFail() throws SQLException {
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(false);

        MysqlUserDAO mysqlUserDAO = new MysqlUserDAO(mockConn);
        Assertions.assertThrows(EntityNotFoundException.class, mysqlUserDAO::findAll);
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void deleteByIdSuccess() throws SQLException, EntityNotDeletedException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        int userId = 1;
        MysqlUserDAO mysqlUserDAO = new MysqlUserDAO(mockConn);
        mysqlUserDAO.deleteById(userId);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }

    @Test
    void deleteByIdFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        int userId = 2;
        MysqlUserDAO mysqlUserDAO = new MysqlUserDAO(mockConn);
        Assertions.assertThrows(EntityNotDeletedException.class, () -> mysqlUserDAO.deleteById(userId));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }

    @Test
    void getRoleDictSuccess() throws SQLException, EntityNotFoundException {
        int[] roleId = {1,2,3};
        String[] roleName = {"Role1", "Role2", "Role3"};
        Map<Integer, String> testRoleMap = new HashMap<>();
        for (int i = 0; i < roleId.length; i++) {
            testRoleMap.put(roleId[i],roleName[i]);
        }
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(true);
        Mockito.when(mockResultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        Mockito.when(mockResultSet.getInt("role_id")).thenReturn(roleId[0]).thenReturn(roleId[1]).thenReturn(roleId[2]);
        Mockito.when(mockResultSet.getString("role_name")).thenReturn(roleName[0]).thenReturn(roleName[1]).thenReturn(roleName[2]);
        MysqlUserDAO mysqlUserDAO = new MysqlUserDAO(mockConn);
        Assertions.assertEquals(testRoleMap, mysqlUserDAO.getRoleDict());
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void getRoleDictFail() throws SQLException {
        Mockito.when(mockConn.createStatement()).thenReturn(mockStmt);
        Mockito.when(mockStmt.executeQuery(Mockito.anyString())).thenReturn(mockResultSet);
        Mockito.when(mockResultSet.isBeforeFirst()).thenReturn(false);
        MysqlUserDAO mysqlUserDAO = new MysqlUserDAO(mockConn);

        Assertions.assertThrows(EntityNotFoundException.class, mysqlUserDAO::getRoleDict);
        Mockito.verify(mockStmt, Mockito.times(1)).executeQuery(Mockito.anyString());
    }

    @Test
    void updateLastOnlineDateByIdSuccess() throws SQLException, EntityNotUpdatedException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(1);
        MysqlUserDAO mysqlUserDAO = new MysqlUserDAO(mockConn);
        mysqlUserDAO.updateLastOnlineDateById(21);
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }

    @Test
    void updateLastOnlineDateByIdFail() throws SQLException {
        Mockito.when(mockConn.prepareStatement(Mockito.anyString())).thenReturn(mockPrepStmt);
        Mockito.doNothing().when(mockPrepStmt).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.doNothing().when(mockPrepStmt).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.when(mockPrepStmt.executeUpdate()).thenReturn(0);
        MysqlUserDAO mysqlUserDAO = new MysqlUserDAO(mockConn);
        Assertions.assertThrows(EntityNotUpdatedException.class, () -> mysqlUserDAO.updateLastOnlineDateById(21));
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockPrepStmt, Mockito.times(1)).executeUpdate();
    }

}